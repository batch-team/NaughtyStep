#!/bin/python

import json
import argparse

from batchfactory.pdbclient import PDBClient
from batchfactory.roger_client import RogerClient
from naughtystep.utils.config import Config
from naughtystep.utils import hostLocator
from naughtystep.utils import misc
from naughtystep.utils import errors


class ConsistencyChecker(object):
    def __init__(self):
        self.roger = RogerClient({'roger': Config().get('services', 'roger')})
        self.pdb = PDBClient()
        self.hosts = []

    def loadServices(self, serviceFile):
        with open(serviceFile) as serviceConfig:
            services = json.load(serviceConfig)

        for service, value in services.iteritems():
            if value['type'] == 'hostgroup':
                self.hosts += [host['certname'].split('.')[0] for host in self.pdb.get_hostgroup_nodes(service)[1]]
            else:
                self.hosts += hostLocator.getAllHostsTenant(service)


    def run(self, dryrun=False):
        for host in self.hosts:
            err, pdbResponse = self.pdb.get_host_facts(host, 'batch_shutdowntime')
            success, appstate = self.roger.get_appstate(host)
            if err or not success:
                continue

            if not pdbResponse:
                print host, 'has no batch_shutdowntime fact'
                continue

            shutdowntime = pdbResponse[0]['value']

            if shutdowntime != 0 and appstate not in ['drained', 'draining', 'hard_reboot', 'destroy', 'intervention']:
                print 'Host {0} is draining, although roger state is {1} - removing from draining'.format(host, appstate)

                if not dryrun:
                    try:
                        misc.doSsh(host, 'rm -f /etc/shutdowntime')
                    except errors.SshError:
                        print "Failed for ", host


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dryrun", action="store_true")

    args = parser.parse_args()

    checker = ConsistencyChecker()
    checker.loadServices('/etc/naughtystep/services.json')
    checker.run(args.dryrun)
