import json
import time
import unittest

from naughtystep.ledger import ledger


currentTime = int(time.time())

status1 = {'alarms': [{'name': 'some alarm 1'},
                      {'name': 'some alarm 2'}],
           'action': 'reboot', 'timestamp': currentTime}
status2 = {'alarms': [{'name': 'some alarm 3'},
                      {'name': 'some alarm 4'}],
           'action': 'nova', 'timestamp': currentTime}
status3 = {'alarms': [{'name': 'some alarm 5'},
                      {'name': 'some alarm 6'}],
           'action': 'kill', 'timestamp': currentTime}


class LedgerTest(unittest.TestCase):
    def setUp(self):
        self.application = ledger.application.test_client()

    def tearDown(self):
        ledger.ledger.clear()

    def test_listAllNoHost(self):
        rv = self.application.get('/v1/step/ledger')
        assert rv.status_code == 200
        assert rv.data == '[]'

    def test_listAllNodesWithHost(self):
        ledger.ledger = {'jigglypuff.cern.ch': [status1],
                         'jigglypuff2.cern.ch': [status2, status3]}

        rv = self.application.get('/v1/step/ledger')

        assert rv.status_code == 200

        assert rv.data == '["jigglypuff.cern.ch", "jigglypuff2.cern.ch"]' or \
            rv.data == '["jigglypuff2.cern.ch", "jigglypuff.cern.ch"]'

    def test_getHostDetails(self):
        ledger.ledger = {'jigglypuff.cern.ch': [status1],
                         'jigglypuff2.cern.ch': [status2, status3]}

        rv = self.application.get('/v1/step/jigglypuff3.cern.ch/ledger')
        assert rv.status_code == 404

        rv = self.application.get('/v1/step/jigglypuff.cern.ch/ledger')
        assert rv.status_code == 200
        assert json.loads(rv.data) == [status1]

    def test_addHost(self):
        ledger.ledger = {'jigglypuff.cern.ch': [status1],
                         'jigglypuff2.cern.ch': [status2, status3]}

        rv = self.application.post('/v1/step/jigglypuff3.cern.ch/ledger',
                                   data=json.dumps(status1),
                                   content_type='application/json')

        assert rv.status_code == 201

        rv = self.application.post('/v1/step/jigglypuff3.cern.ch/ledger',
                                   data=json.dumps(status3),
                                   content_type='application/json')

        assert rv.status_code == 201
        assert ledger.ledger['jigglypuff3.cern.ch'] == [status1, status3]


if __name__ == '__main__':
    unittest.main()
