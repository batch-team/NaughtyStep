package blackboard

import (
	"encoding/json"
	"github.com/jarcoal/httpmock"
	"github.com/smartystreets/goconvey/convey"
	"naughtyUtils/config"
	"strconv"
	"testing"
	"time"
)

func Test_GetAllHosts(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	blackboardUrl := "http://" + config.Get("blackboard", "host") + "/v1/step"

	convey.Convey("Blackboard error codes", t, func() {
		convey.Convey("404 Not Found", func() {
			httpmock.RegisterResponder("GET", blackboardUrl,
				httpmock.NewStringResponder(404, "Not Found"))

			_, err := GetAllHosts()

			convey.So(err, convey.ShouldNotBeNil)
		})

		convey.Convey("500 Server Error", func() {
			httpmock.RegisterResponder("GET", blackboardUrl,
				httpmock.NewStringResponder(500, "Server Error"))

			_, err := GetAllHosts()

			convey.So(err, convey.ShouldNotBeNil)
		})

	})

	convey.Convey("Blackboard empty", t, func() {
		httpmock.RegisterResponder("GET", blackboardUrl,
			httpmock.NewStringResponder(200, "[]"))

		response, err := GetAllHosts()

		convey.So(err, convey.ShouldBeNil)
		convey.So(response, convey.ShouldBeEmpty)
	})

	convey.Convey("Blackboard with multiple hosts", t, func() {
		httpmock.RegisterResponder("GET", blackboardUrl,
			httpmock.NewStringResponder(200, `["host1", "host2", "hostn"]`))

		response, err := GetAllHosts()

		convey.So(err, convey.ShouldBeNil)
		convey.So(response, convey.ShouldHaveLength, 3)
		convey.So(response, convey.ShouldContain, "host1")
		convey.So(response, convey.ShouldContain, "host2")
		convey.So(response, convey.ShouldContain, "hostn")
	})
}

func Test_GetHostData(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	blackboardUrl := "http://" + config.Get("blackboard", "host") + "/v1/step"

	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456789}],
                 "tryNo": 1, "notUntil": 123456789, "tests": {}}`

	convey.Convey("Host not present", t, func() {
		httpmock.RegisterResponder("GET", blackboardUrl+"/jigglypuff",
			httpmock.NewStringResponder(404, "Not Found"))

		_, err := GetHostData("jigglypuff")

		convey.So(err, convey.ShouldNotBeNil)
	})

	convey.Convey("Host present", t, func() {
		httpmock.RegisterResponder("GET", blackboardUrl+"/jigglypuff",
			httpmock.NewStringResponder(200, str))

		host, err := GetHostData("jigglypuff")

		convey.So(err, convey.ShouldBeNil)

		var hostData Host
		json.Unmarshal([]byte(str), &hostData)
		hostData.Name = "jigglypuff"

		convey.So(host, convey.ShouldHaveSameTypeAs, hostData)
		convey.So(host, convey.ShouldResemble, hostData)
	})
}

func Test_Retry(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	blackboardUrl := "http://" + config.Get("blackboard", "host") + "/v1/step"

	convey.Convey("Host not present", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/retry",
			httpmock.NewStringResponder(404, "Not Found"))

		err := Retry("jigglypuff", "1234")

		convey.So(err, convey.ShouldNotBeNil)
	})

	convey.Convey("Host present", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/retry",
			httpmock.NewStringResponder(200, "OK"))

		err := Retry("jigglypuff", "1234")

		convey.So(err, convey.ShouldBeNil)
	})
}

func Test_DeleteHost(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	blackboardUrl := "http://" + config.Get("blackboard", "host") + "/v1/step"

	convey.Convey("Host not present", t, func() {
		httpmock.RegisterResponder("DELETE", blackboardUrl+"/jigglypuff",
			httpmock.NewStringResponder(404, "Not Found"))

		err := DeleteHost("jigglypuff")

		convey.So(err, convey.ShouldNotBeNil)
	})

	convey.Convey("Host present", t, func() {
		httpmock.RegisterResponder("DELETE", blackboardUrl+"/jigglypuff",
			httpmock.NewStringResponder(200, "OK"))

		err := DeleteHost("jigglypuff")

		convey.So(err, convey.ShouldBeNil)
	})
}

func Test_enableTest(t *testing.T) {

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	blackboardUrl := "http://" + config.Get("blackboard", "host") + "/v1/step"

	convey.Convey("Enable ping for host not present", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/ping",
			httpmock.NewStringResponder(404, "Not Found"))

		err := enableTest("jigglypuff", "ping")

		convey.So(err, convey.ShouldNotBeNil)
	})

	convey.Convey("Enable ping for host present", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/ping",
			httpmock.NewStringResponder(200, "OK"))

		err := enableTest("jigglypuff", "ping")

		convey.So(err, convey.ShouldBeNil)
	})

	convey.Convey("Enable ssh for host present", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/ssh",
			httpmock.NewStringResponder(200, "OK"))

		err := enableTest("jigglypuff", "ssh")

		convey.So(err, convey.ShouldBeNil)
	})

}

func Test_HasAlarm(t *testing.T) {

	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456},
			    {"exception": "afsd_wrong", "uuid": "2", "timestamp": 123499},
			    {"exception": "CVMFSProbe", "uuid": "3", "timestamp": 999999}],
		"tryNo": 1, "notUntil": 123456789, "tests": {"ping":["", ""]}}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("Check alarm not present", t, func() {
		ret := HasAlarm(hostData, "some_alarm")
		convey.So(ret, convey.ShouldEqual, false)
	})

	convey.Convey("Check for all alarms if they are present", t, func() {
		convey.So(HasAlarm(hostData, "no_contact"), convey.ShouldEqual, true)
		convey.So(HasAlarm(hostData, "CVMFSProbe"), convey.ShouldEqual, true)
		convey.So(HasAlarm(hostData, "afsd_wrong"), convey.ShouldEqual, true)
	})
}

func Test_testEnabled(t *testing.T) {
	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456},
			    {"exception": "afsd_wrong", "uuid": "2", "timestamp": 123499},
			    {"exception": "CVMFSProbe", "uuid": "3", "timestamp": 999999}],
		"tryNo": 0, "notUntil": 123456789, "tests": {"ping":[""], "nova":[""]}}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("Check test not present", t, func() {
		ret := testEnabled(hostData, "ssh")
		convey.So(ret, convey.ShouldEqual, false)
	})

	convey.Convey("Check for all tests if they are present", t, func() {
		convey.So(testEnabled(hostData, "ping"), convey.ShouldEqual, true)
		convey.So(testEnabled(hostData, "nova"), convey.ShouldEqual, true)
	})
}

func Test_TimeSinceLastAlarm(t *testing.T) {

	currentTime := time.Now().Unix()

	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": ` + strconv.Itoa(int(currentTime)-10) + `},
	                    {"exception": "no_contact", "uuid": "2", "timestamp": ` + strconv.Itoa(int(currentTime)-20) + `},
	                    {"exception": "no_contact", "uuid": "3", "timestamp": ` + strconv.Itoa(int(currentTime)-30) + `}],
                 "runningTests": ["ping"],
                 "tryNo": 1, "notUntil": 123456789, "tests": [{}]}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("", t, func() {
		timeSinceAlarm, err := TimeSinceLastAlarm(hostData)
		convey.So(int(timeSinceAlarm.Seconds()), convey.ShouldEqual, 10)
		convey.So(err, convey.ShouldBeNil)
	})
}

func Test_TimeSinceFirstAlarm(t *testing.T) {

	currentTime := time.Now().Unix()

	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": ` + strconv.Itoa(int(currentTime)-10) + `},
	                    {"exception": "no_contact", "uuid": "2", "timestamp": ` + strconv.Itoa(int(currentTime)-20) + `},
	                    {"exception": "no_contact", "uuid": "3", "timestamp": ` + strconv.Itoa(int(currentTime)-30) + `}],
                 "runningTests": ["ping"],
                 "tryNo": 1, "notUntil": 123456789, "tests": [{}]}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("", t, func() {
		timeSinceAlarm, err := TimeSinceFirstAlarm(hostData)
		convey.So(int(timeSinceAlarm.Seconds()), convey.ShouldEqual, 30)
		convey.So(err, convey.ShouldBeNil)
	})
}

func Test_GetUniqueAlarms(t *testing.T) {

	var hostData Host

	convey.Convey("No alarms", t, func() {
		convey.So(GetUniqueAlarms(hostData), convey.ShouldBeEmpty)
	})

	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456},
			    {"exception": "afsd_wrong", "uuid": "2", "timestamp": 123499},
			    {"exception": "no_contact", "uuid": "2", "timestamp": 123499},
			    {"exception": "afsd_wrong", "uuid": "3", "timestamp": 123499},
			    {"exception": "afsd_wrong", "uuid": "4", "timestamp": 123499},
			    {"exception": "CVMFSProbe", "uuid": "5", "timestamp": 999999}],
                 "tryNo": 1, "notUntil": 123456789, "tests": {}}`

	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("Multiple different alarms", t, func() {
		convey.So(GetUniqueAlarms(hostData), convey.ShouldHaveLength, 3)
		convey.So(GetUniqueAlarms(hostData), convey.ShouldContain, "no_contact")
		convey.So(GetUniqueAlarms(hostData), convey.ShouldContain, "afsd_wrong")
		convey.So(GetUniqueAlarms(hostData), convey.ShouldContain, "CVMFSProbe")
	})
}

func Test_EnableTests(t *testing.T) {

	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456},
			    {"exception": "afsd_wrong", "uuid": "2", "timestamp": 123499},
			    {"exception": "CVMFSProbe", "uuid": "3", "timestamp": 999999}],
		 "tryNo": 0, "notUntil": 123456789, "tests": {"ping":[""], "nova":[""]}}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	blackboardUrl := "http://" + config.Get("blackboard", "host") + "/v1/step"

	convey.Convey("Test enabling with 404 from the blackboard", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/some_test",
			httpmock.NewStringResponder(404, "Not Found"))

		tests := []string{"some_test"}
		err := EnableTests(hostData, tests)

		convey.So(err, convey.ShouldNotBeNil)
	})

	convey.Convey("Test enabling test present (blackboard replies with 404 if query is made)", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/ping",
			httpmock.NewStringResponder(404, "Not Found"))

		tests := []string{"ping"}
		err := EnableTests(hostData, tests)

		convey.So(err, convey.ShouldBeNil)
	})

	convey.Convey("Test enabling test present and test not present", t, func() {
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/ping",
			httpmock.NewStringResponder(404, "Not Found"))
		httpmock.RegisterResponder("POST", blackboardUrl+"/jigglypuff/enable/ssh",
			httpmock.NewStringResponder(200, "OK"))

		tests := []string{"ping", "ssh"}
		err := EnableTests(hostData, tests)

		convey.So(err, convey.ShouldBeNil)
	})
}

func Test_TestsResultsPresent(t *testing.T) {
	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456},
			    {"exception": "afsd_wrong", "uuid": "2", "timestamp": 123499},
			    {"exception": "CVMFSProbe", "uuid": "3", "timestamp": 999999}],
		 "tryNo": 1, "notUntil": 123456789, "tests": {"ping":["true", ""], "nova":["false", "true"]}}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("Test TestsResultsPresent", t, func() {
		convey.Convey("Tests not available yet", func() {
			convey.So(TestsResultsPresent(hostData, []string{"ping", "nova"}), convey.ShouldBeFalse)
		})
		convey.Convey("Tests not enabled", func() {
			convey.So(TestsResultsPresent(hostData, []string{"console"}), convey.ShouldBeFalse)
		})
		convey.Convey("Tests present", func() {
			convey.So(TestsResultsPresent(hostData, []string{"nova"}), convey.ShouldBeTrue)
			hostData.Tests["ping"][1] = "true"
			convey.So(TestsResultsPresent(hostData, []string{"nova", "ping"}), convey.ShouldBeTrue)
		})
	})

}

func Test_TestsPassed(t *testing.T) {
	str := `{"alarms": [{"exception": "no_contact", "uuid": "1", "timestamp": 123456},
			    {"exception": "afsd_wrong", "uuid": "2", "timestamp": 123499},
			    {"exception": "CVMFSProbe", "uuid": "3", "timestamp": 999999}],
		 "tryNo": 1, "notUntil": 123456789, "tests": {"ping":["true", ""], "nova":["false", "true"]}}`

	var hostData Host
	json.Unmarshal([]byte(str), &hostData)
	hostData.Name = "jigglypuff"

	convey.Convey("Test TestsResultsPresent", t, func() {
		convey.Convey("Tests not available yet", func() {
			convey.So(TestsPassed(hostData, map[string]string{"ping": "true", "nova": "true"}), convey.ShouldBeFalse)
		})
		convey.Convey("Tests not enabled", func() {
			convey.So(TestsPassed(hostData, map[string]string{"console": "true"}), convey.ShouldBeFalse)
		})
		convey.Convey("Tests present", func() {
			convey.So(TestsPassed(hostData, map[string]string{"nova": "true"}), convey.ShouldBeTrue)
			hostData.Tests["ping"][1] = "true"
			convey.So(TestsPassed(hostData, map[string]string{"nova": "true", "ping": "true"}), convey.ShouldBeTrue)
		})
	})
}
