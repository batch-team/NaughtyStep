""" Test module for blackboard """

import unittest
import json
import time
import requests_mock
import mock

from naughtystep.blackboard import blackboard
from naughtystep.utils import config

config = config.Config()

currentTime = int(time.time()) - 5
incrementedTime = currentTime + 10

# Test alarms in string and json versions
alarm1str = '{"exception": "no_contact", "uuid": "1", "timestamp": "' + str(currentTime) + '"}'
alarm2str = '{"exception": "no_contact", "uuid": "2", "timestamp": "' + str(currentTime) + '"}'
alarm3str = '{"exception": "some_alarm", "uuid": "2", "timestamp": "' + str(currentTime) + '"}'

alarm1json = json.loads(alarm1str)
alarm2json = json.loads(alarm2str)
alarm3json = json.loads(alarm3str)


class BlackboardTest(unittest.TestCase):
    """ Tests for the blackboard module """

    def setUp(self):
        """ Get a test client to run requests at setup """
        self.application = blackboard.application.test_client()

    def tearDown(self):
        """ Clear the content of the blackboard at teardown """

        blackboard.blackboard.clear()

    def populate(self):
        """ Add some hosts to the blackboard """

        blackboard.blackboard['jigglypuff'] = \
            {'alarms': [alarm1json, alarm3json],
             'tryNo': 2, 'notUntil': currentTime,
             'tests': {'ping': ['true', '', '']}}
        blackboard.blackboard['jigglypuff2'] = \
            {'alarms': [alarm1json],
             'tryNo': 1, 'notUntil': currentTime,
             'tests': {'ping':['', '']}}
        blackboard.blackboard['jigglypuff3'] = \
            {'alarms': [alarm3json],
             'tryNo': 2, 'notUntil': currentTime,
             'tests': {'ping':['true', '', 'true'], 'ssh':['', '', '']}}


    def testListAllNoHost(self):
        """ Fetch the host list when the blackboard is empty """

        rv = self.application.get('/v1/step')

        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.data, '[]')

    def testListAllWithHosts(self):
        """ Fetch the host list when the blackboard is not empty """

        self.populate()

        rv = self.application.get('/v1/step')
        responseJson = json.loads(rv.data)

        self.assertEqual(rv.status_code, 200)
        self.assertEqual(len(responseJson), 3)

        self.assertIn("jigglypuff", responseJson)
        self.assertIn("jigglypuff2", responseJson)
        self.assertIn("jigglypuff3", responseJson)

    def testListAllWithAlarm(self):
        """ List all hosts that have a given alarm """

        self.populate()

        alarms = {'no_contact': set(['jigglypuff', 'jigglypuff2']),
                  'some_alarm': set(['jigglypuff', 'jigglypuff3']),
                  'not_present': set([])}

        for alarm, expectedResponse in alarms.items():
            rv = self.application.get('/v1/step/alarm/' + alarm)
            responseJson = json.loads(rv.data)

            self.assertEqual(rv.status_code, 200)
            self.assertEqual(set(responseJson), expectedResponse)


    def testGetDetailsHostNotFound(self):
        """ Return 404 if a host is not found while fetching details """

        rv = self.application.get('/v1/step/jigglypuff')
        self.assertEqual(rv.status_code, 404)

    def testDeleteHostNotFound(self):
        """ Return 404 if a host is not found when deleting a host """

        rv = self.application.delete('/v1/step/jigglypuff')
        self.assertEqual(rv.status_code, 404)

    def testAddTestResultsHostNotFound(self):
        """ Return 404 if a host is not found when adding test results """

        rv = self.application.post('/v1/step/jigglypuff/test/1/ping',
                                   data='{"ping":true}',
                                   content_type='application/json')
        self.assertEqual(rv.status_code, 404)

    def testRetryHostNotFound(self):
        """ Return 404 if a host is not found when retrying """

        rv = self.application.post('/v1/step/jigglypuff/retryNo',
                                   data='1234656',
                                   content_type='application/json')
        self.assertEqual(rv.status_code, 404)

    def testTestersNoHost(self):
        """ Return 404 if a host is not found when retrying """

        rv = self.application.get('/v1/step/testers/ping')
        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.data, '[]')

    @requests_mock.Mocker()
    def testPendingAction(self, mocker):
        """ Test for blackboard.pendingAction (utility for newException) """

        loggerHost = config.get('naughtyLogger', 'host')
        logger = "http://{0}/v1/step/log/error/blackboard".format(loggerHost)
        mocker.post(logger, status_code=200)

        actions = ["reboot", "manual", "ssh"]

        actionServiceDown = "manual"

        # Check result if the host is not present in any of the actions
        for action in actions:
            actionHost = config.get(action, 'host')
            actionUrl = "http://{0}/v1/step/jigglypuff/actions/{1}".format(actionHost, action)
            if action is actionServiceDown:
                mocker.get(actionUrl, status_code=500)
            else:
                mocker.get(actionUrl, status_code=404)

        ret = blackboard._pendingAction("jigglypuff")
        self.assertEqual(ret, False)

        # Check result if the host is present in an action
        actionHost = config.get(actions[0], 'host')
        actionUrl = "http://{0}/v1/step/jigglypuff/actions/{1}".format(actionHost, actions[0])
        mocker.get(actionUrl, status_code=200, json=["jigglypuff"])

        ret = blackboard._pendingAction("jigglypuff")
        self.assertEqual(ret, True)


    @requests_mock.Mocker()
    @mock.patch('naughtystep.blackboard.blackboard._isVirtual')
    @mock.patch('naughtystep.blackboard.blackboard._pendingAction')
    def testAddException(self, mocker, _pendingAction, _isVirtual):
        """ Add 3 exceptions for the same host """

        _isVirtual.return_value = True
        _pendingAction.return_value = False

        loggerHost = config.get('naughtyLogger', 'host')
        logger = "http://{0}/v1/step/log/info/blackboard".format(loggerHost)
        mocker.post(logger, status_code=200)

        rv = self.application.post('/v1/step/jigglypuff/alarm/no_contact',
                                   data=alarm1str,
                                   content_type='application/json')

        self.assertEqual(rv.status_code, 201)
        self.assertEqual(len(blackboard.blackboard['jigglypuff']['alarms']), 1)
        self.assertIn('jigglypuff', blackboard.blackboard)

        rv = self.application.post('/v1/step/jigglypuff/alarm/afs',
                                   data=alarm2str,
                                   content_type='application/json')

        self.assertEqual(rv.status_code, 201)
        self.assertEqual(len(blackboard.blackboard['jigglypuff']['alarms']), 2)
        self.assertIn('jigglypuff', blackboard.blackboard)

        self.assertEqual(len(blackboard.blackboard), 1)

        _pendingAction.return_value = True

        rv = self.application.post('/v1/step/jigglypuff/alarm/afs',
                                   data=alarm2str,
                                   content_type='application/json')
        self.assertEqual(rv.status_code, 409)

        self.assertEqual(mocker.call_count, 3)


    def testGetFullState(self):
        """ Get all the details for a host """

        blackboard.blackboard['jigglypuff'] = \
            {'alarms': [alarm1json, alarm2json],
             'tryNo': 0, 'notUntil': currentTime, 'tests': [{}]}

        rv = self.application.get('/v1/step/jigglypuff')
        self.assertEqual(rv.status_code, 200)

        js = json.loads(rv.data)
        self.assertEqual(js, blackboard.blackboard['jigglypuff'])


    @requests_mock.Mocker()
    def testDeleteHost(self, mocker):
        """ Delete a host """

        loggerHost = config.get('naughtyLogger', 'host')
        logger = "http://{0}/v1/step/log/info/blackboard".format(loggerHost)
        mocker.post(logger, status_code=200)

        self.populate()

        rv = self.application.delete('/v1/step/jigglypuff')
        self.assertEquals(rv.status_code, 200)
        self.assertNotIn('jigglypuff', blackboard.blackboard)

        self.assertEqual(mocker.call_count, 1)


    @requests_mock.Mocker()
    def testRetry(self, mocker):
        """ Rerun tests on a host """

        loggerHost = config.get('naughtyLogger', 'host')
        logger = "http://{0}/v1/step/log/info/blackboard".format(loggerHost)
        mocker.post(logger, status_code=200)

        self.populate()

        rv = self.application.post('/v1/step/jigglypuff/retry',
                                   data='10', content_type='application/json')
        self.assertEquals(rv.status_code, 200)
        self.assertEquals(blackboard.blackboard['jigglypuff']['notUntil'], incrementedTime)
        self.assertEquals(blackboard.blackboard['jigglypuff']['tryNo'], 3)


    def testMarkForTesting(self):
        """ Test marking for testing of a host """

        self.populate()

        rv = self.application.post('/v1/step/jigglypuff5/enable/ssh')
        self.assertEquals(rv.status_code, 404)

        rv = self.application.post('/v1/step/jigglypuff/enable/ssh')
        self.assertEquals(rv.status_code, 200)

        rv = self.application.post('/v1/step/jigglypuff/enable/nova')
        self.assertEquals(rv.status_code, 200)

        self.assertIn('ssh', blackboard.blackboard['jigglypuff']['tests'])
        self.assertIn('nova', blackboard.blackboard['jigglypuff']['tests'])


    def testTestersWithHost(self):
        """ Test fetching hosts that are waiting for test results """

        self.populate()

        rv = self.application.get('/v1/step/testers/ping')
        self.assertEquals(rv.status_code, 200)

        expected1 = [{u'hostname': u'jigglypuff2', u'tryNo': 1},
                     {u'hostname': u'jigglypuff', u'tryNo': 2}]
        expected2 = [{u'hostname': u'jigglypuff2', u'tryNo': 1},
                     {u'hostname': u'jigglypuff', u'tryNo': 2}]
        assert json.loads(rv.data) == expected1 or json.loads(rv.data) == expected2

        rv = self.application.get('/v1/step/testers/ssh')
        self.assertEquals(rv.status_code, 200)
        self.assertEquals(json.loads(rv.data), [{u'hostname': u'jigglypuff3', u'tryNo': 2}])

        rv = self.application.get('/v1/step/testers/nova')
        self.assertEquals(rv.status_code, 200)
        self.assertEquals(json.loads(rv.data), [])


    @requests_mock.Mocker()
    def testAddTestResults(self, mocker):
        """ Test the addition of test results """

        self.populate()

        loggerHost = config.get('naughtyLogger', 'host')
        logger = "http://{0}/v1/step/log/info/blackboard".format(loggerHost)
        mocker.post(logger, status_code=200)

        rv = self.application.post('/v1/step/jigglypuff/test/0/ping',
                                   data='true',
                                   content_type='application/json')
        self.assertEquals(rv.status_code, 201)
        self.assertEqual(blackboard.blackboard['jigglypuff']['tests']['ping'][0], 'true')

        rv = self.application.post('/v1/step/jigglypuff/test/1/ping',
                                   data='false',
                                   content_type='application/json')
        self.assertEqual(rv.status_code, 201)
        self.assertEqual(blackboard.blackboard['jigglypuff']['tests']['ping'][1], 'false')


if __name__ == '__main__':
    unittest.main()
