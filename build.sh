#!/bin/bash

# USAGE: ./script.sh version release
# NOTE: The package is created in batch7

if [ -z $1 ]; then
    echo "Please specify the package version"
    exit 1
fi

if [ -z $2 ]; then
    echo "Please specify the release version"
    exit 1
fi

build_sh_abspath=`realpath $0`
sources=`dirname $build_sh_abspath`

echo "Working on source folder: $sources based on $build_sh_abspath"

spec="naughtystep-server.spec"
spec_path="$sources/$spec"
version=$1
release=$2

if [ ! -f $spec_path ]; then
    echo "Specfile $spec_path does not exist"
    exit 1
fi

if [ ! -d $sources ]; then
    echo "Source directory $sources does not exist"
    exit 1
fi

if [ -d $HOME/rpmbuild ]; then
    rm -rf $HOME/rpmbuild
fi

mkdir -p $HOME/rpmbuild/{RPMS,SRPMS,BUILD,SOURCES,SPECS,tmp}

cat <<EOF >$HOME/rpmbuild/.rpmmacros
%_topdir   %(echo $HOME)/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

pushd $sources
export GOPATH=$sources
export GOBIN=$sources/bin

go get ./...
go build -o $sources/bin/naughtystep-skynet skynet
go build -o $sources/bin/naughtystep-test-nova tests/nova
go build -o $sources/bin/naughtystep-test-ping tests/ping
go build -o $sources/bin/naughtystep-test-ssh tests/ssh
go build -o $sources/bin/naughtystep-test-pdb tests/pdb
popd

pushd $HOME/rpmbuild
ls

cp -r $sources naughtystep-server-$1
tar --exclude=naughtystep-server-$1/src/cloud* --exclude=naughtystep-server-$1/src/git* --exclude=naughtystep-server-$1/src/go* -czf SOURCES/naughtystep-server-$1.tar.gz naughtystep-server-$1
rm -rf naughtystep-server-$1

cp $spec_path SPECS
sed -i "s/%define version.*/%define version $1/g" SPECS/$spec
sed -i "s/%define release.*/%define release $2/g" SPECS/$spec

rpmbuild -ba SPECS/$spec

koji build --scratch batch7 SRPMS/naughtystep-server-$1-$2.src.rpm

echo "Check the scratch build and promote it if successfull..."

popd

