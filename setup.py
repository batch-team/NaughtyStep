#!/usr/bin/env python

from distutils.core import setup

setup(
    name='naughtystep-server',
    version='1',
    description='Naughtystep',
    author='batch-operations@cern.ch',
    author_email='batch-operations@cern.ch',
    url='http://cern.ch/batch',
    package_dir={'': 'src'},
    packages=['naughtystep', 'naughtystep.actions', 'naughtystep.utils',
              'naughtystep.consumer', 'naughtystep.logger', 'naughtystep.ledger',
              'naughtystep.blackboard', 'naughtystep.approver',
              'naughtystep.drainingengine', 'naughtystep.hostlocator'],

    scripts=['bin/naughtystep-blackboard', 'bin/naughtystep-cli-backend',
             'bin/naughtystep-twisted-consumer', 'bin/naughtystep-hostlocator',
             'bin/naughtystep-action-kill', 'bin/naughtystep-action-manual',
             'bin/naughtystep-action-drain', 'bin/naughtystep-action-reboot',
             'bin/naughtystep-action-ssh', 'bin/naughtystep-ledger', 'bin/naughtystep-logger',
             'bin/naughtystep-approver', 'bin/naughtystep-drainingengine', 'bin/naughtystep-limiter'],

    requires=['paramiko', 'requests', 'tabulate', 'tqdm']
)
