# The REST API of the various components (draft)

## Blackboard

- Alarms addition:

  `POST /v1/step/<hostname>/alarm/<alarm_name>`

  Body JSON:
  `{"timestamp": int,
    "uuid": string,
    "exception": string,
    "host": string,
    "hostgroup": string}`

  If the host is not present in the naughtystep, a new entry is created and
returns 201.
  If the host is present, the new alarm is noted and returns 201.

  If the host is pending an action, the alarm is discarded and a 409 Conflict
is returned.

- List all hosts in naughtystep:

  `GET /v1/step`

  Returns a list of the hosts present in the naughtystep, along with status
code 200.

  `GET /v1/step/alarm/<alarm_name>`

  Returns a list of all the hosts present in the naughtystep that have the
given alarm present and status code 200.

- Get the details of a host:

  `GET /v1/step/<hostname>`

  Returns a JSON with the entire state of the host from naughtystep and status
code 200 if successful, or status code 404 if the host is not present in the
naughtystep.

  Body JSON:
    `{"alarms": [{"timestamp": int,
                  "uuid": string,
                  "exception": string,
                  "host": string,
                  "hostgroup": string},
                ...],
     "notUntil": string, // RFC 3339 format
     "tryNo": int,
     "tests": [{"ssh": string,
                "ping": string,
                "novaState": string,
                "novaConsoleOutput": string},
                ...]
    }`

- Add results of a test:

  `POST /v1/step/<hostname>/test/<tryNo>/<testName>`

  Body JSON: string containing the test result

  Returns 201 on success, 404 if `hostname` is not present in the naughtystep

- Tester request queue:

  `GET /v1/step/testers/<testName>`

  Returns a JSON list of strings, representing the list of hosts that require
test `testName` for the current test run and status 200.

- Retry:

  `POST /v1/step/<hostname>/retry`

  Body JSON: int representing the number of seconds to schedule the test rerun
after the current scheduled one.

  Returns 200 on success, 404 is the host is not present in the naughtystep.

- Enable

  `POST /v1/step/<hostname>/enable/<test>`

  Enables `test` for `hostname`. Enabling a test means that the blackboard
will report it for the pending hosts for `test` until a result for this
test is added.

  Returns 200.

- Locking

  `POST /v1/step/lock/<exception>`

  Locks an exception - this will make the blackboard refuse hosts with
this exception.

  Returns 200 on success, 409 if the exception is already locked.

  `POST /v1/step/unlock/<exception>`

  Removes the lock on an exception.
  
  Returns 200 on success, 409 if the exception is not locked.

  `GET /v1/step/lock`

  Gives a list of locked exceptions.

  Returns 200.

## Logger

Provides a single method, for adding a log entry:

  `POST /v1/step/log/<level>/<module[.submodule]>`

  Body JSON: a string to be logged.

  Level represents the log level: INFO, DEBUG, ERROR, CRITICAL, etc

  The log is stored in `/var/log/naughtystep.log`

## Host Locator

Utility used to find hosts in the cloud by maintaining some metadata about
them in an in-memory cache. The cache is refreshed periodically or by request.
A cache refresh will make a request last for about 5-10 minutes.

  `GET /v1/step/hostlocator/<hostname>`

  Returns 200 on success, 404 if the host cannot be found.

  Body JSON:
    `{"Name": string,
     "UUID": string,
     "TenantName": string,
     "TenantId": string}`

  `GET /v1/step/hostlocator/<hostname>/neighbours`

  Gives a list of neighbours (VMs on the same hypervisor) of _hostname_.

  Returns 200 on success, 404 if _hostname_ is not found.

  `GET /v1/step/hostlocator/<tenant>/tenant`

  Gives a list of all hosts in _tenant_.

  Returns 200 on success, 404 is _tenant_ is not found.

  `GET /v1/step/hostlocator/refresh`

  Triggers a cache refresh for the host locator.

## Actions

All actions share a common interface. The only difference between them
is the payload required when adding a new host.

- List all pending hosts:

  `GET /v1/step/actions/<action>`

  Returns a list of the hosts pending and status code 200.

- Get details of a pending host:

  `GET /v1/step/<host>/actions/<action>`

  Returns status code 200 on success or 404 if the host is not pending.
  The JSON body is the same as the one sent when adding the host to the action.

- Set a host in pending state:

  `POST /v1/step/<host>/actions/<action>`

  Returns status code 200.
  Use a JSON payload to add details about the specifics of the action.

- Approve an action for a host (must be pending):

  `POST /v1/step/<host>/actions/<action>/approve`

  Returns 200 on success, 404 if the host is not pending or other action
specific codes.

- Decline an action for a host:

  `POST /v1/step/<host>/actions/<action>/decline`

  Returns 200 on success, 404 if the host is not pending.

### Payloads

All hosts added to an action must have the following fields in the payload:

  `hostgroup`

  A string representing the hostgroup, used by the approver to control the
limit of unavailable/draining hosts in a hostgroup.

  `reason`

  A human readable string representing the reason for this action.

  `available`

  `true` if the host runs or can run jobs, `false` otherwise.

Additionally, all hosts can have a `priority` field with an integer value.
If not specifified, a default priority is set. See `priorities.md` for more
details about this.

Besides these common attributes, all actions have some specific attributes:

- Reboot

  `virtual`

  `true` if the host is a VM, `false` otherwise.

- Drain

  `type`

  A string determining the type of draining. Available types are `draining`,
`hard_reboot`, `destroy` and `intervention`.

  Additionally, the drain action might take a `time` attribute, a timestamp
that indicates the end time of the drain process.

- Ssh

  `script`

  The name of the script to be ran on the machine. The script must be available
in the script repository directory, configured in `/etc/naughtystep.conf`.

  `auth`

  The authentication type to the VM. Must be `krb` for Kerberos or `password`
for password (the password is fetched from teigi).


## Ledger

- List all hosts present in the ledger.

  `GET /v1/step/ledger`

- Get details of a host present in the ledger.

  `GET /v1/step/<host>/ledger`

- Add a host to the ledger.

  `POST /v1/step/<host>/ledger`, along with a json payload containg the
alarms of the host, the action taken and the timestamp of the action.

## WSGI cli backend

This module is used to forward the requests from the user CLI to the
naughtystep.

Accepts request of the form:

  `GET/POST/DELETE /<component>/<url...>`

and forwards the request to the `component` (e.g. host locator, blackboard),
with the given url and request body.
