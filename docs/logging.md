# Logging

An important part of the application is logging. There are 2 types of
logging used: central logging and service logging. All log files are produced
in ```/var/log```.

## Service Logging

Each service saves its standard output and error streams in 2 different files,
usually named ```<service_name>.stdout``` and ```<service_name>.stderr```.
These files are usually helpful for debugging.

## Central Logging

The ```logger``` service provides central logging. It simply accepts POST
requests from all services and maintains a central log file, named
```naughtystep.log```. Here, all the operations perfomed on hosts can be seen,
as well as debugging information for the steps taken.
