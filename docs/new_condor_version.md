# Upgrading to a new condor version

In order to upgrade to a new version of condor:

- set the hiera variable `condor_package_version` to the desired version.
  This versionlocks the desired condor version and sets the `condor_version`
  sensor to look for this specific version.
- configure the priority of the new version in `/etc/naughtystep/naughtystep.json`
  (should be lower than of the current existing versions).

