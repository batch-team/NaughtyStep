# Tests

The NaughtyStep workflow is:
- A new exception arrives from the _consumer_
- The host is added to the _blackboard_
- The _skynet_ launches the corresponding handler
- The handler enables tests and checks their results
- When enough data is collected, the handler makes a decision and adds the host
  to an action and removes it from the _blackboard_

The tests are identified by their name (a string). The blackboard holds
for each host a list of enabled tests and their results. Multiple tests can be
added and they can be ran multiple times before the skynet makes a decision.
The whole process is done in different invocations of the same handler,
so the handler must find the state in which the host is (what tests are enabled,
what tests have their results available, what is the test run, if the tests
should run one more time before making a decision, etc).

The tests are golang services: they run under _supervisord_ alongside various
other services of the NaughtyStep.

## Creating a new test

Before creating a new test, find a place for it: in some cases, a new service
might be required, while in other cases it might be added to a current service.
For example, the _ssh_ test service has currently 5 tests:

- `puppetConf`, which checks the presence of the puppet configuration file
  ```/etc/puppet/puppet.conf```
- `puppetExecutable`, which checks the presence of the puppet executable
- `ssh`, which checks if the machine can be accessed via SSH
- `uptime`, which runs the ```uptime``` command on the machine
- `varFull`, which checks if the HDD usage in ```/var``` is 100%

All tests are types which implement the ```tester``` interface, i.e. they
must have the methods ```GetName``` and ```Run```. The main function of the
service must create an object for each test and pass it to ```tester.Run```.

```tester.Run``` runs in an infinite loop, querying periodically the blackboard
for hosts that need this specific test to run (the tests are identified by a
string, their name) and calls the ```Run``` method of the test.

