Exception handlers
========

Whenever the skynet analyses a host, it can decide to fix a problem caused by
an exception by calling the corresponding handler. If multiple exceptions are
present, the handler with the highest priority is used (read more in the
skynet documentation).

Exception handlers are objects that implement the ```Handler``` interface
(```src/skynet/handlers.go```):

- ```Name() string``` returns the exception handled. There cannot be multiple
  handlers that handle the same exception.
- ```GetPriority() int``` returns the priority of this handler. It can be
  any strictly positive integer (negative values and 0 may be used internally
  by skynet).
- ```RequiredTests(blackboard.Host) []string``` returns an array of tests that
  must be ran.
- ```Handle()``` is the core handler - checks the result of the tests and makes
  the decision.


The skynet enables all the tests requested by ```RequiredTests``` then calls
```Handle```. It is the responsability of ```Handle``` to check if the test
result are present, schedule retries and make decisions based on these results.
In some cases, ```RequiredTests``` might also need to check the result of
some tests (see the ```no_contact``` handler for an example of this).

Adding a new handler
====

This guide will show the addition of a handler for the ```yum_stuck```
exception.

1. Create a new file in ```src/skynet/handlers```. By convention, the handler
source files are named ```handle_exception.go```. 
2. Use the following template

```
package handlers

import (
	"blackboard"
)

type MyExceptionHandler struct{}

func (this *MyExceptionHandler) Name() string {
	return "my_exception"
}

func (this *MyExceptionHandler) GetPriority() int {
	return 1
}

func (this *MyExceptionHandler) Handle(host blackboard.Host) {
}

func (this *MyExceptionHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
```

3. Replace ```MyExceptionHandler``` with an appropriate name and change the
values returned by ```Name()``` and ```GetPriority()```.

4. Write the ```RequiredTests``` and the ```Handle``` functions.

In the case of the ```yum_stuck``` exception, no tests are needed, so
```RequiredTests``` is the same. The ```Handle``` method must set the host
for drain and kill. There are utilities in the ```skynet/actions``` package for
performing actions and the ```blackboard``` package provides utilities for
interacting with the blackboard.

All it is needed for this handler is to call ```actions.Drain``` and do some
error checking and logging. Find below the whole handler:

```
package handlers

import (
	"blackboard"
	"skynet/actions"
	"utils/naughtyLogger"
)

type YumStuckHandler struct{}

func (this *YumStuckHandler) Name() string {
	return "yum_stuck"
}

func (this *YumStuckHandler) GetPriority() int {
	return 4
}

func (this *YumStuckHandler) Handle(host blackboard.Host) {
	naughtyLogger.Info("skynet", host.Name+" has rpm stuck")
	err := actions.Drain(host, "Has yum_stuck", "destroy", false, this.GetPriority())
	if err != nil {
		naughtyLogger.Error("skynet",
			host.Name+" could not be marked for drain: "+err.Error())
		return
	}

	blackboard.DeleteHost(host.Name)
}

func (this *YumStuckHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
```

5. Register the handler in the ```GetHandler()``` factory from
```src/skynet/handlers/handlers/go```. This function is used by the skynet
to get a handler from the exception name.

6. Finally, the consumers must be configured to forward the new exception to
the blackboard by adding the exception name in the ```forwarded_exceptions```
list under ```gni_consumer``` in ```/etc/naughtystep/naughtystep.json```.

7. The skynet needs to be recompiled and the consumer restarted.

