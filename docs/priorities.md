# Priorities

Priorities positive integers used to control the race conditions that occur
during the decision making process by setting an order in which the exceptions
should be handled.

All handlers and hosts pending an action must have an associated priority.
When analysing a host, the `skynet` checks the priority of the pending
action (if there is one). If the host has an exception whose handler has
a higher priority, the action is declined and that handler is ran. When
analysing a host, only the hander with the highest priority will run.

The approver approves actions in decreasing order of priority.

It is recommended that the priority of the handler is equal to the priority
of its decisions - this is true for all handlers except for `condor_upgrade`.

The priorities of the currently available handlers are:


- `nscd_wrong`: 1
- `Operating_System`: 1
- `condor_upgrade`: 10-20
- `afsd_wrong`: 21
- `CVMFSProbe`: 21
- `limd_wrong`: 22
- `puppetd_wrong`: 22
- `rpm_stuck`: 23
- `yum_stuck`: 23
- default (if no priority is specified when adding to the action): 50
- `no_contact`: 100
- manual intervention: 101

The `condor_upgrade` handler has the priority 10 (it must override anything
with priority less than 10), but its decisions have priorities between
10 and 20 to be able to prioritize the draining based on the condor version.

The draining priorities associated with each condor version are configured
in `/etc/naughtystep/naughtystep.json` under `drainingengine`.
They should be kept in the interval 10-20, inclusive, and all expected condor
versions should be specified. If a version is not specified, a default priority
is assigned.

