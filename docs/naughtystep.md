# Naughtystep

This guide introduces NaughtyStep, a tool for automating the batch farm
management built using a microservices architecture with REST APIs.

The current microservices are:

- Consumers: They connect to a Stomp broker and read exceptions from the
  monitoring infrastructure. The exceptions are filtered and passed forward
  to the blackboard. Currently, 2 consumers are available: a stompctl/dirq
  based consumer and a twisted based consumer (recommended). Besides listening
  to the exceptions from monitoring, the consumers are connected to the megabus,
  analysing notifications from the Cloud team (twisted consumer only).

- Blackboard: Is a state storage service, in which hosts are kept until all
  the data is collected and a decision can be made. It also coordinates the
  running of tests, by storing which tests are required for any given host
  and the results of the tests, when they become available.

- Skynet: Keeps the core logic of the application. It runs periodically,
  analysing all the hosts in the blackboard and dispatching and appropriate
  handling routine, depending on the exceptions available for a host. Each
  handler has an associated priority which defines the order in which exceptions
  should be treated (handlers with equal priority are chosen randomly).

- Actions: Once skynet makes a decision, the host is in pending state for that
  action until approval or denial. Each action (manual, drain, kill, reboot
  and ssh) is provided as a separate service. When adding a host to an action,
  some data needs to be provided. All actions require the availability
  (if the machine can run jobs), the hostname, the reason for adding and
  the priority of the action. Additionally, actions might require some more
  data. For example, the drain action requires the draining type to be
  specified.

- Ledger: It is a long term database storage of actions performed on hosts.

- Logger: Central logging service. All services perform logging by sending data
  to this service.

- Redis: The blackboard and action storage is managed by a redis instance

- Host locator: A cache of the locations (i.e. tenants) of the VMs in the
  batch farm. Equivalent to PDB/ai-rc, but works for hosts without a PDB
  entry and gives more details (such as the hypervisor and the UUID).

- Tests: Services that query periodially the blackboard for hosts that are
  awaiting some test results and run the given tests. Each test runs in a
  separate thread (either as a separate service or as a goroutine) and it
  identified by a string.

- Draining engine: Manages the draining of VMs. See draining.md.

- Approver: Is a service to automatically approve actions, while keeping some
  constraints. It always approves actions for unavailable hosts, but has a
  configurable limit for actions performed on available hosts. Draining hosts
  are considered unavailable.

- Limiter: Runs on query (i.e. not automatically) and locks an exception if
  more than a configurable percent of hosts from a given hostgroup have that
  exception. By locking, the hosts are present in the blackboard but hidden
  from all queries (such that the tests and the skynet do not perform any
  operations on them) until the action is unlocked.


## Configuration

All the services mentioned above are managed using supervisord, running behind
a firewall and an WSGI proxy. The proxy required Kerberos authentication,
performs authorization on individual endpoints and, if they succeeded, the
request is passed to the service indicated in URL.

The following files are available for configuration:

- /etc/naughtystep/naughtystep.json: The main configuration file
- /etc/naughtystep/access.json: Authorization
- /etc/naughtystep/services.json: Services (tenants or hostgroups) used by
                                  the draining engine and the approver
- /etc/naughtystep/limits.json: Limiter configuration

