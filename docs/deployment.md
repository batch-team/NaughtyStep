# Deployment notes

## Ledger
- Leger uses a PostgreSQL database, now provided by DBOD.
- If deploying a new instance, request a new instance to DBOD service.
- Create `<ledger-user>` account with `<password>` for `<db-name>`:

  ```
  psql -h <dbod-instance>.cern.ch -p <dbod-port> -U admin
  Password for user admin: <dbod-instance-password>
  admin=> CREATE USER <ledger-user> WITH PASSWORD '<password-here>';
  admin=> CREATE DATABASE <db-name-here>;
  CREATE DATABASE
  admin=> \connect <db-name>
  <db-name>=> GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO <ledger-user>;
  GRANT
  ```

- Download `pg_hba.conf` from dbod interface and add at the end of the file:

  ```
  # User related -- PLEASE EDIT HERE
  host	all		admin		0.0.0.0/0		md5
  host	all		<ledger-user>		0.0.0.0/0		md5
  ```

- Create the ledger schema in the database from the ledger instance:

  ```
  [root@naughtystep-ledger]# python
  >>> from naughtystep.ledger import ledger
  >>> ledger.db.create_all()
  >>> ledger.db.session.commit()
  ```
