# NaughtyStep

NaughtyStep is a tool to aid in the maintenance of the batch farm at CERN by
automating the diagnostics and fixing of both physical machines and VMs.

The core features are:
- consumes GNI exceptions
- asynchronously runs various tests, depending on the exception
- can prioritize the order in which exceptions are handled
- can watch if the number of exceptions on a hostgroup exceeds a certain
threshold and emit warnings
- provides tools for automating the draining process

## Usage

The human interaction is done using a python script called `naughtystep`.

- `naughtyStep list` lists all the hosts that are tested currently, along with
their state
- `naughtyStep details <hostname>` gives details about the host `hostname`,
including the alarms and the test results
- `naughtyStep pending <action>` lists all hosts for which an action has been
suggested
- `naughtyStep pending <action> <hostname>` gives details about a host that
is pending action, including the alarms and the results of the tests
- `naughtyStep approve <action> <hostname>` performs action on host `hostname`
- `naughtyStep decline <action> <hostname>` declines the action action for the
host `hostname`, creating some diagnostic log entries

, where action currently is reboot, ssh, drain, kill or manual.

## Where to find information

The documentation in this repo provides information on maintenance and
small guides for adding new features. The batchops docs entry provides
information on usage of the tool from a user's perspective. The `man
naughtystep` command provides information on the available commands.




