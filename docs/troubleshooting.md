# Troubleshooting tips

## Action failed with status 500

Status 500 indicates something went really bad on the server side.

The first step is to check if there is something useful in the logs.
Check the `/var/log/<service>.stdout`, `/var/log/<service>.stderr`
and `/var/log/naughtystep.log` files for anything useful regarding the
issue. If nothing is found, run the service in development/debug mode 
by stopping the production server:

`supervisorctl stop <service>`

and starting the flask development server in debug mode (make sure to set
`debug` to `true` in the `/etc/naughtystep/naughtystep.json`
for the appropriate service):

`python bin/<service>`

Now, recreate the environment in which the error occured. For example, if
the problem occured during an action approval, the same host with the same
settings should be marked for that action.

Doing the same operation again should produce a diagnostic output both in the
client response and in the `/var/log/<service>.stdout` and
`/var/log/<service>.stderr`.

## Draining engine does not end

The draining engine uses thread pools to check the state of the hosts and
to drain them. If one thread dies in a thread pool (e.g. from an exception),
the draining engine request will not end.

A regular draining engine is expected to last anywhere from 30 minutes to
over 2 hours, depending on the number of draining hosts.
