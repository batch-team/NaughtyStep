Draining - How it works
========

Draining is a procedure required to avoid losing jobs when performing
operations such as reboot on a machine by stopping the machine from accepting
jobs and waiting until the existing jobs are finished.

Reasons for draining a machine include the upgrade campaings (condor and
operating system) and machine decomissioning.

NaughtyStep provides 3 tools to help with this process: the drain action, the
draining engine and the approver.

The drain action
====

The NaughtyStep core logic, skynet, might decide that a node needs to be
drained. In this case, the node is added to the drain action with the correct
metadata and awaits approval.

On approval, the action only sets roger appstate and the roger message.

Roger states
====

The draining state of the nodes is managed using roger appstate. Available
states for draining are ```draining```, ```destroy```, ```intervention```.

The message is usually set to describe the reason for draining.

When the message contains a timestamp, that timestamp is used as the deadline
for draining.

Draining engine
====

The draining engine checks the roger state of all hosts in the hostgroups or
tenants provided in ```/etc/naughtystep/services.json``` and manages their
draining process.

The first time when the draining engine notices a node in
a draining state, the draining process is started by creating
```/etc/shutdowntime``` on the node. This file contains a timestamp which
indicates the draining deadline - at that time the machine should not run
any jobs.

On further runs of the draining engine, some nodes may run jobs and be in the
process of draining. These nodes are acknowledged, but no action is taken.

After the draining is started and there are no more jobs running, the action
corresponding to the draining state is taken:

- ```draining``` - run the ```finish_draining``` script, remove the
  ```/etc/shutdowntime``` file and reboot using the ssh and reboot actions
  provided by NaughtyStep. The ```finish_draining``` script runs on the machine
  all the ```/etc/drain.d/fix_*``` files, if they are present.
- ```destroy``` - kill the VM
- ```intervention``` - remove the ```/etc/shutdowntime``` file and set roger
  appstate to ```drained```.

If a host belongs to a tenant specified in
```/etc/naughtystep/services.json```, its draining is stopped when all its
neighbours are drained too - this can be used to coordinate interventions
with the Cloud team.


Approver
====

The approver is a tool for managing draining campaings, by making sure only
a given percent out of a service (tenant of hostgroup) is being drained or
unavailable.

The entries in ```/etc/naughtystep/services.json``` have the following format:


```"service": {"type": "[hostgroup|tenant]", "limit": 10, "approve": ["action1", "action2"]}```

, where ```service``` is a hostgroup or a tenant, ```limit``` is the percent of
hosts that can be unavailable/draining at the same time and ```approve``` is
a list of NaughtyStep actions that can be approved by the approver.

There is a special service, named ```unknown```, associated with the hosts
for which there was no configured service. Currently, it is used to approve
actions on unavailable hosts.

The approver computes the number of actions it can make on available hosts
on a service using the following formula:

```total_hosts - no_of_draining_hosts - no_of_unavailable_hosts```, where
```no_of_unavailable_hosts``` is the number of hosts pending for some
NaughtyStep action maked as unavailable.


Running the approver and the draining engine
====

There are 4 ways of running both the approver and the draining engine:

- by directly running the script
- by running the service and making a POST request
- by using the NaughtyStep client to make a POST request to the service
- by making the request from Rundeck.

Directly running the script is only recommended for debugging purposes.
The best way to run it is using the NaughtyStep client, either from Rundeck
or from CLI.

Both the approver and the draining engine have dryrun options, where the user
can see the actions that would be made if it were to run normally.

The draining engine has 3 dryrun options: ```drystart```, ```dryend```,
```dryrun```. A drystart run ends draining on hosts, but does not start
draining (it reports it as if it was started). Similarly, a dryend run starts
draining on hosts but does not end it. Dryrun is a combination of the 2. These
options are useful during development and debugging.


Consistency issues
====

For NaughtyStep, a host is in draining state if its roger state is
```draining```, ```hard_reboot```, ```destroy```, ```intervention``` or
```drained```. However, the host will actually be draining if the
```/etc/shutdowntime``` file is present. The presence of this file is
normally managed by the draining engine but there might be the case where,
for some nodes, the roger appstate is reverted to production, but the
```/etc/shutdowntime``` file is still present on the host. In this case,
NaughtyStep has no way of knowing that the host is still draining. A utility
script, ```consistency-check.py``` is provided, that removes this file on
hosts what are not in a draining state anymore.

As a future improvement, a roger action could be used to remove the file
automatically when leaving a draining state, which would render this script
obsolete.


Draining times
====

Each VM stays in the draining state for a period of time. During this time,
it can accept jobs, provided that the jobs can finish by the end of the
draining period.

A node is removed from the draining state if there are no jobs running or
the drain period had been exceeded by 7 days.

There are 2 ways of setting the draining time:
- Manually, by specifying a timestamp in the message (the message can
  contain free text around the timestamp - a regex is used to find where is
  the timestamp in the roger message)
- Automatically, by the draining engine.

The draining engine sets the drain time to the default service value
(i.e. value per hostgroup/tenant) or to the default global value.

All values are in seconds.
