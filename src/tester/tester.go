package tester

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"utils/config"
	"utils/naughtyLogger"
)

type Test interface {
	RunTest(string) string
	GetName() string
}

type testTarget struct {
	Hostname string
	TryNo    int
}

var waitGroup sync.WaitGroup

func Run(test Test) {
	blackboard := config.Get("blackboard", "host")

	for {
		url := "http://" + blackboard + "/v1/step/testers/" + test.GetName()
		response, err := http.Get(url)
		if err != nil {
			fmt.Println("Error on " + test.GetName())
			log.Println(err)
			time.Sleep(time.Second * 300)
			continue
		}

		var targetList []testTarget
		json.NewDecoder(response.Body).Decode(&targetList)

		response.Body.Close()

		for _, host := range targetList {
			waitGroup.Add(1)
			go runAndUpdate(host, test)
		}

		waitGroup.Wait()

		time.Sleep(time.Second * 300)
	}
}

// Run the test on a given target and update the blackboard
func runAndUpdate(target testTarget, test Test) {
	defer waitGroup.Done()
	testResult := test.RunTest(target.Hostname)
	updateBlackboard(target, testResult, test.GetName())
}

func updateBlackboard(target testTarget, testResult string, testName string) {
	blackboard := config.Get("blackboard", "host")

	url := fmt.Sprintf("http://%s/v1/step/%s/test/%d/%s",
		blackboard, target.Hostname, target.TryNo, testName)

	response, err := http.Post(url, "application/json", strings.NewReader(testResult))

	if err != nil {
		errorMsg := fmt.Sprintf("%s test update errored for host: %s (try %d): %s",
			testName, target.Hostname, target.TryNo, err.Error())

		fmt.Fprint(os.Stderr, errorMsg)
		naughtyLogger.Error("test."+testName, errorMsg)
		return
	}

	if response.StatusCode != http.StatusCreated {
		fmt.Println(testName)
		fmt.Println(target)
		fmt.Println(target.Hostname)
		fmt.Println(target.TryNo)
		fmt.Println(response.StatusCode)
		fmt.Println(response.Status)
		errorMsg := fmt.Sprintf("%s test update failed for host: %s (try %d) - server replied with %d %s\n",
			testName, target.Hostname, target.TryNo, response.StatusCode, response.Status)

		fmt.Fprint(os.Stderr, errorMsg)
		naughtyLogger.Warning("test."+testName, errorMsg)
		return
	}

	response.Body.Close()

	notice := fmt.Sprintf("test.%s Ran on %s (try %d) : %.30s",
		testName, target.Hostname, target.TryNo, testResult)
	naughtyLogger.Info("test."+testName, notice)
}
