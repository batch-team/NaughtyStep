package handlers

import (
	"blackboard"
	"skynet/actions"
	"utils/naughtyLogger"
)

type AfsdWrongHandler struct{}

func (this *AfsdWrongHandler) Name() string {
	return "afsd_wrong"
}

func (this *AfsdWrongHandler) GetPriority() int {
	return 21
}

func (this *AfsdWrongHandler) Handle(host blackboard.Host) {
	naughtyLogger.Info("skynet",
		host.Name+" has afsd_wrong and operating_system. Deleting and rebooting")

	err := actions.Reboot(host, "adfs_wrong", true, this.GetPriority())
	if err != nil {
		naughtyLogger.Info("skynet",
			host.Name+" could not be rebooted: "+err.Error())
		return
	}

	blackboard.DeleteHost(host.Name)
}

func (this *AfsdWrongHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
