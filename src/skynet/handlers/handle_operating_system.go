package handlers

import (
	"blackboard"

	"skynet/actions"
	"utils/naughtyLogger"
)

type OperatingSystemHandler struct{}

func (this *OperatingSystemHandler) Name() string {
	return "operating_system"
}

func (this *OperatingSystemHandler) GetPriority() int {
	return 1
}

func (this *OperatingSystemHandler) Handle(host blackboard.Host) {
	if blackboard.HasAlarm(host, "CMVFSProbe") || blackboard.HasAlarm(host, "afsd_wrong") {
		return
	}

	err := actions.Drain(host, "Has Operating_System", "draining", true, this.GetPriority())
	if err != nil {
		naughtyLogger.Error("skynet",
			host.Name+" could not be drained: "+err.Error())
		return
	}
	blackboard.DeleteHost(host.Name)
}

func (this *OperatingSystemHandler) RequiredTests(host blackboard.Host) []string {
	return []string{}
}
