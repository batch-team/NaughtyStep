package handlers

import (
	"blackboard"

	"utils/naughtyLogger"
)

type IgnoredHandler struct{}

func (this *IgnoredHandler) Name() string {
	return "recurrent"
}

func (this *IgnoredHandler) GetPriority() int {
	return 100
}

func (this *IgnoredHandler) Handle(host blackboard.Host) {
	naughtyLogger.Debug("skynet", "Deleting "+host.Name+": ignored alarm detected")
	blackboard.DeleteHost(host.Name)
}

func (this *IgnoredHandler) RequiredTests(host blackboard.Host) []string {
	return []string{""}
}
