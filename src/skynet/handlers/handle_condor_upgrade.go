package handlers

import (
	"blackboard"
	"strconv"

	"skynet/actions"
	"utils/config"
	"utils/naughtyLogger"
)

type CondorUpgradeHandler struct{}

func (this *CondorUpgradeHandler) Name() string {
	return "condor_upgrade"
}

func (this *CondorUpgradeHandler) GetPriority() int {
	return 10
}

func (this *CondorUpgradeHandler) Handle(host blackboard.Host) {

	if !blackboard.TestsResultsPresent(host, []string{"condor_version"}) {
		return
	}

	version := host.Tests["condor_version"][host.TryNo]

	if version == "ERROR" {
		if host.TryNo < 4 {
			retryTime := config.Get("skynet", "retry_time")
			blackboard.Retry(host.Name, retryTime)
		} else {
			err := actions.ManualIntervention(host, "condor_version test failed")
			if err != nil {
				return
			}
			blackboard.DeleteHost(host.Name)
		}
		return
	}

	priority, ok := config.GetMap("drain", "priorities")[version]
	if !ok {
		naughtyLogger.Warning("skynet", "No priority configured for condor version "+version)
		priority = config.GetMap("drain", "priorities")["default"]
	}

	p, err := strconv.Atoi(priority)
	if err != nil {
		naughtyLogger.Error("skynet", "Priority for "+version+" incorrect ("+priority+")")
	}

	err = actions.Drain(host, "Has Condor_Upgrade", "draining", true, p)
	if err != nil {
		naughtyLogger.Error("skynet",
			host.Name+" could not be drained: "+err.Error())
		return
	}
	blackboard.DeleteHost(host.Name)
}

func (this *CondorUpgradeHandler) RequiredTests(host blackboard.Host) []string {
	return []string{"condor_version"}
}
