package handlers

import (
	"strconv"
	"time"

	"blackboard"
	"skynet/actions"
	"utils/config"
	"utils/naughtyLogger"
)

type NoContactHandler struct{}

func (this *NoContactHandler) Name() string {
	return "no_contact"
}

func (this *NoContactHandler) GetPriority() int {
	return 100
}

func timeSinceLastNoContact(host blackboard.Host) time.Duration {

	var timeSinceLastAlarm time.Time

	for _, alarm := range host.Alarms {
		if alarm.Exception == "no_contact" && timeSinceLastAlarm.Before(alarm.Timestamp.Time) {
			timeSinceLastAlarm = alarm.Timestamp.Time
		}
	}

	return time.Now().Sub(timeSinceLastAlarm)
}

func isRebooting(host blackboard.Host) bool {
	return blackboard.TestsPassed(host, map[string]string{"novaState": "HARD_REBOOT"}) ||
		blackboard.TestsPassed(host, map[string]string{"novaState": "REBOOT"})
}

func (this *NoContactHandler) Handle(host blackboard.Host) {

	noRetries, _ := config.GetInt("skynet", "retries")
	retryTime := config.Get("skynet", "retry_time")

	testResultsPresent := func() bool {
		baseTestsPresent := blackboard.TestsResultsPresent(host, []string{"dns", "ping", "ssh"})
		varFullPresent := host.TryNo == 0 || blackboard.TestsPassed(host, map[string]string{"ssh": "false"}) || blackboard.TestsResultsPresent(host, []string{"varFull"})
		novaStatePresent := !host.IsVirtual || blackboard.TestsResultsPresent(host, []string{"novaState"})
		novaConslePresent := host.TryNo < noRetries || !host.IsVirtual || blackboard.TestsResultsPresent(host, []string{"novaConsoleOutput"})

		return baseTestsPresent && varFullPresent && novaStatePresent && novaConslePresent
	}

	allTestsPassed := func() bool {
		baseTestsOk := blackboard.TestsPassed(host, map[string]string{"dns": "true", "ping": "true", "ssh": "true", "varFull": "true"})
		novaStateOk := !host.IsVirtual || blackboard.TestsPassed(host, map[string]string{"novaState": "ACTIVE"})
		return baseTestsOk && novaStateOk
	}

	if !testResultsPresent() {
		return
	}

	var err error
	if blackboard.TestsPassed(host, map[string]string{"dns": "false"}) {
		// Ignore hosts missing DNS entries
	} else if blackboard.TestsPassed(host, map[string]string{"ping": "true", "ssh": "true", "varFull": "false"}) {
		err = actions.InjectScript(host, "/var is full", "clear_var", "password", this.GetPriority())
	} else if blackboard.TestsPassed(host, map[string]string{"ssh": "true", "varFull": "ERROR"}) {
		err = actions.ManualIntervention(host, "Could not perform the varfull check")
	} else if blackboard.TestsPassed(host, map[string]string{"novaState": "NOT FOUND"}) {
		// Couldn't find the VM: delete, but the no_contact may be due to monitoring issues
		actions.HostOK(host)
	} else if blackboard.TestsPassed(host, map[string]string{"novaConsoleOutput": "TEST FAILED"}) {
		err = actions.ManualIntervention(host, "novaError: couldn't fetch console")
	} else if blackboard.TestsPassed(host, map[string]string{"ping": "true", "ssh": "false"}) && host.TryNo < 30 || host.TryNo < noRetries {
		naughtyLogger.Debug("skynet.no_contact", "Retrying on "+host.Name)
		blackboard.Retry(host.Name, retryTime)
		return
	} else {
		timeSinceLastNoContact := timeSinceLastNoContact(host).Seconds()
		naughtyLogger.Debug("skynet.no_contact", "Time since last NC for "+host.Name+" "+strconv.Itoa(int(timeSinceLastNoContact)))
		if allTestsPassed() && timeSinceLastNoContact < 3600 {
			err = actions.ManualIntervention(host, "no_contact but host seems fine")
		} else if allTestsPassed() && timeSinceLastNoContact >= 3600 {
			err = actions.ManualIntervention(host, "Host seems fine now, but was spawining no_contacts quite recently")
		} else if host.IsVirtual && isRebooting(host) {
			err = actions.ManualIntervention(host, "novaError: stuck in hard reboot")
		} else {
			err = actions.Reboot(host, "no_contact", false, this.GetPriority())
		}
	}
	if err != nil {
		naughtyLogger.Error("skynet",
			"Could not perform action on "+host.Name+": "+err.Error())
		return
	}
	blackboard.DeleteHost(host.Name)
}

func (this *NoContactHandler) RequiredTests(host blackboard.Host) []string {
	noRetries, _ := config.GetInt("skynet", "retries")
	requiredTests := []string{"dns", "ping", "ssh"}
	if host.IsVirtual {
		requiredTests = append(requiredTests, "novaState")
	}
	if host.TryNo == 1 && blackboard.TestsPassed(host, map[string]string{"ssh": "true"}) {
		requiredTests = append(requiredTests, "varFull")
	}
	if host.TryNo == noRetries && host.IsVirtual {
		requiredTests = append(requiredTests, "novaConsoleOutput")
	}

	return requiredTests
}
