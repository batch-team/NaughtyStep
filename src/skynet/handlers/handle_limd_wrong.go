package handlers

import (
	"blackboard"
	"strconv"
	"strings"

	"skynet/actions"
	"utils/naughtyLogger"
)

type LimdWrongHandler struct{}

func (this *LimdWrongHandler) Name() string {
	return "limd_wrong"
}

func (this *LimdWrongHandler) GetPriority() int {
	return 22
}

func (this *LimdWrongHandler) Handle(host blackboard.Host) {

	if strings.HasSuffix(host.Hostgroup, "spare") {
		blackboard.DeleteHost(host.Name)
		return
	}

	if !blackboard.TestsResultsPresent(host, []string{"uptime"}) {
		return
	}

	uptime, _ := strconv.Atoi(host.Tests["uptime"][0])

	if uptime > 48*3600 {
		reason := "Limd wrong after " + strconv.Itoa(uptime) + " seconds of running"
		if host.IsVirtual {
			err := actions.Kill(host, reason)
			if err != nil {
				naughtyLogger.Error("skynet",
					host.Name+" could not be killed: "+err.Error())
				return
			}
		} else {
			err := actions.ManualIntervention(host, reason)
			if err != nil {
				naughtyLogger.Error("skynet",
					host.Name+" could not be added to manual intervention: "+err.Error())
				return
			}
		}
	}

	blackboard.DeleteHost(host.Name)
}

func (this *LimdWrongHandler) RequiredTests(host blackboard.Host) []string {
	if !strings.HasSuffix(host.Hostgroup, "spare") {
		return []string{"uptime"}
	} else {
		return []string{}
	}
}
