package handlers

import (
	"blackboard"
	"errors"
)

// An interface for an exception handler
type Handler interface {
	// The name of the exception handled, mostly for testing and debugging
	// purposes
	Name() string

	// Return a list of required tests for the given host in order to be
	// able to handle the expceion
	RequiredTests(blackboard.Host) []string

	// Handle the exception
	Handle(host blackboard.Host)

	// Get the handler priority
	// Handlers with higher priority are executed first
	// The priority is also stored in actions;
	// handlers with higher priority override decisions of handlers with
	// lower priority
	GetPriority() int
}

// Factory method that returns the appropriate handler for an exception
func GetHandler(exception string) (Handler, error) {
	switch exception {
	// Special handlers for ignored and recurrent exceptions
	case "ignored":
		return new(IgnoredHandler), nil
	case "recurrent":
		return new(RecurrentHandler), nil
	// -----------------------------------------------------
	case "no_contact":
		return new(NoContactHandler), nil
	case "afsd_wrong":
		return new(AfsdWrongHandler), nil
	case "nscd_wrong":
		return new(NscdWrongHandler), nil
	case "limd_wrong":
		return new(LimdWrongHandler), nil
	case "CVMFSProbe":
		return new(CvmfsprobeHandler), nil
	case "Operating_System":
		return new(OperatingSystemHandler), nil
	case "condor_upgrade":
		return new(CondorUpgradeHandler), nil
	case "puppetd_wrong":
		return new(PuppetdWrongHandler), nil
	case "rpm_stuck":
		return new(RpmStuckHandler), nil
	case "yum_stuck":
		return new(YumStuckHandler), nil
	default:
		return nil, errors.New("No handler present for " + exception)
	}
}

// Type that implements sort.Interface in order to be able to sort handlers by
// their priority
type HandlerArray []Handler

func (this HandlerArray) Len() int {
	return len(this)
}

func (this HandlerArray) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

func (this HandlerArray) Less(i, j int) bool {
	return this[i].GetPriority() > this[j].GetPriority()
}
