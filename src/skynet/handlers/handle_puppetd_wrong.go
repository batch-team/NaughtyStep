package handlers

import (
	"blackboard"

	"skynet/actions"
	"utils/config"
	"utils/naughtyLogger"
)

type PuppetdWrongHandler struct{}

func (this *PuppetdWrongHandler) Name() string {
	return "puppetd_wrong"
}

func (this *PuppetdWrongHandler) GetPriority() int {
	return 22
}

func (this *PuppetdWrongHandler) Handle(host blackboard.Host) {

	var err error

	if !blackboard.TestsResultsPresent(host, []string{"ssh", "puppetExecutable", "puppetConf"}) {
		return
	}

	if blackboard.TestsPassed(host, map[string]string{"ssh": "true", "puppetExecutable": "true", "puppetConf": "true"}) {
		err = actions.ManualIntervention(host, "Has puppetd_wrong but all tests passed")
	} else if blackboard.TestsPassed(host, map[string]string{"ssh": "true"}) {
		err = actions.Drain(host, "Has puppetd_wrong", "destroy", false, this.GetPriority())
	} else if host.TryNo < 4 {
		retryTime := config.Get("skynet", "retry_time")
		blackboard.Retry(host.Name, retryTime)
	} else {
		err = actions.ManualIntervention(host, "Has puppetd_wrong and could not SSH")
	}

	if err != nil {
		naughtyLogger.Error("skynet",
			"Could not perform action on "+host.Name+" "+err.Error())
		return
	}
	blackboard.DeleteHost(host.Name)
}

func (this *PuppetdWrongHandler) RequiredTests(host blackboard.Host) []string {
	requiredTests := []string{"ssh", "puppetExecutable", "puppetConf"}
	return requiredTests
}
