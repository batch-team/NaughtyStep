package handlers

import (
	"blackboard"
	"skynet/actions"
	"utils/naughtyLogger"
)

type NscdWrongHandler struct{}

func (this *NscdWrongHandler) Name() string {
	return "nscd_wrong"
}

func (this *NscdWrongHandler) GetPriority() int {
	return 1
}

func (this *NscdWrongHandler) Handle(host blackboard.Host) {
	err := actions.InjectScript(host, "nscd_wrong", "nscd_fix", "krb", this.GetPriority())
	if err != nil {
		naughtyLogger.Error("skynet",
			"Could not run script on "+host.Name+": "+err.Error())
		return
	}
	blackboard.DeleteHost(host.Name)
}

func (this *NscdWrongHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
