package handlers

import (
	"blackboard"
	"skynet/actions"
	"utils/naughtyLogger"
)

type CvmfsprobeHandler struct{}

func (this *CvmfsprobeHandler) Name() string {
	return "CVMFSProbe"
}

func (this *CvmfsprobeHandler) GetPriority() int {
	return 21
}

func (this *CvmfsprobeHandler) Handle(host blackboard.Host) {
	naughtyLogger.Info("skynet", host.Name+" has cvmfsprobe and operating_system. Deleting and rebooting")
	err := actions.Reboot(host, "CVMSProbe", true, this.GetPriority())
	if err != nil {
		naughtyLogger.Error("skynet",
			host.Name+" could not be marked for reboot: "+err.Error())
		return
	}

	blackboard.DeleteHost(host.Name)
}

func (this *CvmfsprobeHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
