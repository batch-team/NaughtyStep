package handlers

import (
	"blackboard"

	"skynet/actions"
	"utils/naughtyLogger"
)

type RecurrentHandler struct{}

func (this *RecurrentHandler) Name() string {
	return "recurrent"
}

func (this *RecurrentHandler) GetPriority() int {
	return 100
}

func (this *RecurrentHandler) Handle(host blackboard.Host) {

	if !blackboard.TestsResultsPresent(host, []string{"dns"}) {
		return
	}

	if host.Tests["dns"][0] == "false" {
		naughtyLogger.Info("skynet", "Ignoring "+host.Name+": Has a recurrent alarm but no DNS entry")
	} else if blackboard.HasAlarm(host, "no_contact") {
		if host.IsVirtual && !actions.IsPending(host.Name, "kill") {
			actions.Kill(host, "Has a recurrent alarm")
		} else if !actions.IsPending(host.Name, "manual") {
			actions.ManualIntervention(host, "Has a recurrent alarm")
		}
	}

	blackboard.DeleteHost(host.Name)
}

func (this *RecurrentHandler) RequiredTests(host blackboard.Host) []string {
	return []string{"dns"}
}
