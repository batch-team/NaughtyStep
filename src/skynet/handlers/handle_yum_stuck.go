package handlers

import (
	"blackboard"
	"skynet/actions"
	"utils/naughtyLogger"
)

type YumStuckHandler struct{}

func (this *YumStuckHandler) Name() string {
	return "yum_stuck"
}

func (this *YumStuckHandler) GetPriority() int {
	return 23
}

func (this *YumStuckHandler) Handle(host blackboard.Host) {
	naughtyLogger.Info("skynet", host.Name+" has rpm stuck")
	err := actions.Drain(host, "Has yum_stuck", "destroy", false, this.GetPriority())
	if err != nil {
		naughtyLogger.Error("skynet",
			host.Name+" could not be marked for drain: "+err.Error())
		return
	}

	blackboard.DeleteHost(host.Name)
}

func (this *YumStuckHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
