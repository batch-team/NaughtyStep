package handlers

import (
	"github.com/smartystreets/goconvey/convey"
	"testing"

	"blackboard"
)

func Test_GetHandler(t *testing.T) {
	exceptions := []string{"no_contact", "afsd_wrong", "nscd_wrong", "CVMFSProbe"}

	convey.Convey("Check if returned handler is correct based on its Name()", t, func() {
		for _, exception := range exceptions {
			handler, err := GetHandler(exception)
			convey.So(err, convey.ShouldBeNil)
			convey.So(handler.Name(), convey.ShouldEqual, exception)
		}
	})

	convey.Convey("Check exception with no registered handler", t, func() {
		_, err := GetHandler("some_exception")
		convey.So(err, convey.ShouldNotBeNil)
	})
}

func Test_NscdWrongHandler(t *testing.T) {
	handler := new(NscdWrongHandler)

	convey.Convey("Check Name()", t, func() {
		convey.So(handler.Name(), convey.ShouldEqual, "nscd_wrong")
	})

	convey.Convey("Check RequiredTests()", t, func() {
		convey.So(handler.RequiredTests(blackboard.Host{}), convey.ShouldBeEmpty)
	})
}
