package handlers

import (
	"blackboard"
	"skynet/actions"
	"utils/naughtyLogger"
)

type RpmStuckHandler struct{}

func (this *RpmStuckHandler) Name() string {
	return "rpm_stuck"
}

func (this *RpmStuckHandler) GetPriority() int {
	return 23
}

func (this *RpmStuckHandler) Handle(host blackboard.Host) {
	naughtyLogger.Info("skynet", host.Name+" has rpm stuck")
	err := actions.Drain(host, "Has rpm_stuck", "destroy", false, this.GetPriority())
	if err != nil {
		naughtyLogger.Error("skynet",
			host.Name+" could not be marked for drain: "+err.Error())
		return
	}

	blackboard.DeleteHost(host.Name)
}

func (this *RpmStuckHandler) RequiredTests(host blackboard.Host) []string {
	return make([]string, 0)
}
