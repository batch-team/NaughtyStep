package actions

import (
	"utils/naughtyLogger"

	"blackboard"
)

// Mark a host for manual intervention
func ManualIntervention(hostEntry blackboard.Host, reason string) error {
	type diagnosticEntry struct {
		Reason    string   `json:"reason"`
		Hostgroup string   `json:"hostgroup"`
		Available bool     `json:"available"`
		Priority  int      `json:"priority"`
		State     []string `json:"state"`
	}

	naughtyLogger.Info("skynet", "Requesting manual intervention for "+hostEntry.Name+": "+reason)

	dEntry := diagnosticEntry{reason, hostEntry.Hostgroup, false, 100, nil}
	return markForAction(hostEntry.Name, "manual", dEntry)
}

func HostOK(hostEntry blackboard.Host) error {
	type diagnosticEntry struct {
		Action string `json:"action"`
	}

	naughtyLogger.Info("skynet", "Host "+hostEntry.Name+" passed all tests")

	dEntry := diagnosticEntry{"host OK"}
	return addToLedger(hostEntry.Name, dEntry)
}

// Mark a host for kill
func Kill(hostEntry blackboard.Host, reason string) error {
	type diagnosticEntry struct {
		Reason    string   `json:"reason"`
		Hostgroup string   `json:"hostgroup"`
		Available bool     `json:"available"`
		Priority  int      `json:"priority"`
		State     []string `json:"state"`
	}

	naughtyLogger.Info("skynet", "Killing host "+hostEntry.Name)

	dEntry := diagnosticEntry{reason, hostEntry.Hostgroup, false, 100, blackboard.GetUniqueAlarms(hostEntry)}
	return markForAction(hostEntry.Name, "kill", dEntry)
}

func Drain(hostEntry blackboard.Host, reason string, drainType string, available bool, priority int) error {
	type diagnosticEntry struct {
		Type      string   `json:"type"`
		Reason    string   `json:"reason"`
		Hostgroup string   `json:"hostgroup"`
		Available bool     `json:"available"`
		Priority  int      `json:"priority"`
		State     []string `json:"state"`
	}

	naughtyLogger.Info("skynet", "Draining host "+hostEntry.Name)

	dEntry := diagnosticEntry{drainType, reason, hostEntry.Hostgroup, available, priority, blackboard.GetUniqueAlarms(hostEntry)}
	return markForAction(hostEntry.Name, "drain", dEntry)
}

// Mark a host for reboot
func Reboot(hostEntry blackboard.Host, reason string, available bool, priority int) error {
	type diagnosticEntry struct {
		Alarms    []blackboard.Alarm  `json:"alarms"`
		Tests     map[string][]string `json:"tests"`
		Type      bool                `json:"virtual"`
		Reason    string              `json:"reason"`
		Hostgroup string              `json:"hostgroup"`
		Available bool                `json:"available"`
		Priority  int                 `json:"priority"`
		State     []string            `json:"state"`
	}

	naughtyLogger.Info("skynet", "Rebooting host "+hostEntry.Name)

	dEntry := diagnosticEntry{hostEntry.Alarms, hostEntry.Tests, hostEntry.IsVirtual, reason, hostEntry.Hostgroup, available, priority, blackboard.GetUniqueAlarms(hostEntry)}
	return markForAction(hostEntry.Name, "reboot", dEntry)
}

func InjectScript(hostEntry blackboard.Host, reason string, script string, auth string, priority int) error {
	type diagnosticEntry struct {
		Script    string   `json:"script"`
		Auth      string   `json:"auth"`
		Reason    string   `json:"reason"`
		Hostgroup string   `json:"hostgroup"`
		Available bool     `json:"available"`
		Priority  int      `json:"priority"`
		State     []string `json:"state"`
	}

	naughtyLogger.Info("skynet", "Running "+script+" on host "+hostEntry.Name)

	dEntry := diagnosticEntry{script, auth, reason, hostEntry.Hostgroup, false, priority, blackboard.GetUniqueAlarms(hostEntry)}
	return markForAction(hostEntry.Name, "ssh", dEntry)
}
