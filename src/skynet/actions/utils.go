package actions

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"utils/config"
)

func addToLedger(hostname string, diagnostic interface{}) error {
	url := "http://" + config.Get("ledger", "host") + "/v1/step/" + hostname + "/ledger"

	js, err := json.Marshal(diagnostic)
	diagnosticReader := bytes.NewReader(js)

	response, err := http.Post(url, "application/json", diagnosticReader)

	if err != nil {
		return errors.New("Ledger connection issue: " + err.Error())
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusCreated {
		return errors.New("Ledger returned status " + response.Status)
	}

	return nil
}

func markForAction(hostname string, action string, diagnostic interface{}) error {
	actionService := strings.Split(action, "/")[0]
	url := "http://" + config.Get(actionService, "host") + "/v1/step/" + hostname + "/actions/" + action

	js, err := json.Marshal(diagnostic)

	diagnosticReader := bytes.NewReader(js)
	response, err := http.Post(url, "application/json", diagnosticReader)

	if err != nil {
		return errors.New("Action " + action + " connection issue: " + err.Error())
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusCreated {
		body, _ := ioutil.ReadAll(response.Body)
		return errors.New(action + " returned status " + response.Status + " (" + string(body) + ")")
	}

	return nil
}

func GetHighestPendingPriority(hostname string) (int, string) {
	actions := config.GetList("general", "actions")

	highestPendingPriority := -1
	highestPendingAction := ""

	for _, action := range actions {
		url := "http://" + config.Get(action, "host") + "/v1/step/" + hostname + "/actions/" + action

		response, err := http.Get(url)

		// if there are issues with the query,
		// assume the service is down, thus the host can't be pending
		if err != nil {
			continue
		}
		defer response.Body.Close()

		if response.StatusCode != 200 {
			continue
		}
		decoder := json.NewDecoder(response.Body)
		var s struct {
			Priority int `json: priority`
		}
		err = decoder.Decode(&s)
		if err != nil {
			continue // it's ok for priority field to be missing
		}
		if highestPendingPriority < s.Priority {
			highestPendingPriority = s.Priority
			highestPendingAction = action
		}
	}

	return highestPendingPriority, highestPendingAction
}

func DeclineAction(hostname string, action string) error {
	url := "http://" + config.Get(action, "host") + "/v1/step/" + hostname + "/actions/" + action + "/decline"

	response, err := http.Post(url, "application/json", nil)
	if err != nil {
		return errors.New("connection error when declining action: " + err.Error())
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		return errors.New("declining action returned status " + strconv.Itoa(response.StatusCode))
	}

	return nil
}

func IsPending(hostname string, action string) bool {
	url := "http://" + config.Get(action, "host") + "/v1/step/" + hostname + "/actions/" + action

	response, err := http.Get(url)
	if err != nil {
		return false
	}
	defer response.Body.Close()

	return response.StatusCode == 200
}

func GetState(hostname string, action string) ([]string, error) {
	url := "http://" + config.Get(action, "host") + "/v1/step/" + hostname + "/actions/" + action
	response, err := http.Get(url)

	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return nil, errors.New("Couldn't fetch host " + hostname + " details for " + action)
	}
	decoder := json.NewDecoder(response.Body)
	var s struct {
		State []string `json: state`
	}
	err = decoder.Decode(&s)
	if err != nil {
		return nil, errors.New("Host " + hostname + " has no state while pending for " + action)
	}
	return s.State, nil
}
