package main

import (
	"blackboard"
	"fmt"
	"ledger"
	"skynet/actions"
	"skynet/handlers"
	"time"

	"strconv"

	"utils/config"
	"utils/naughtyLogger"
	"utils/roger"
)

// Check if a host has no new alarms for a long time
func hasNoNewAlarms(host blackboard.Host) bool {

	alarmValidityTime, _ := config.GetInt("skynet", "alarm_validity_time")

	timeSinceLastAlarm, _ := blackboard.TimeSinceLastAlarm(host)

	naughtyLogger.Debug("skyent", "host "+host.Name+" valid for "+strconv.Itoa(alarmValidityTime)+" tsla "+strconv.Itoa(int(timeSinceLastAlarm.Seconds())))
	if int(timeSinceLastAlarm.Seconds()) > alarmValidityTime {
		return true
	}

	return false
}

// Check if a host has recurrent alarms
func hasRecurringAlarm(host blackboard.Host) (requestedDelete, requestedKill bool) {

	firstAlarm, _ := blackboard.TimeSinceFirstAlarm(host)
	timeSinceLastLedgerEntry, err := ledger.TimeSinceLastLedgerEntry(host)
	if err != nil {
		return false, false
	}

	ignoreTime, _ := config.GetInt("skynet", "ignore_time")
	recurrentTime, _ := config.GetInt("skynet", "recurrent_time")

	diff := int((timeSinceLastLedgerEntry - firstAlarm).Seconds())
	naughtyLogger.Debug("skynet", fmt.Sprintf("%s diff %d (time since last ledger entry %d, time since first alarm %d)", host.Name, int(diff), int(timeSinceLastLedgerEntry.Seconds()), int(firstAlarm.Seconds())))

	if diff < ignoreTime {
		naughtyLogger.Info("skynet", host.Name+" has an ignored recurrent alarm")
		return true, false
	} else if diff > ignoreTime && diff < recurrentTime {
		naughtyLogger.Info("skynet", host.Name+" has a recurrent alarm")
		return true, blackboard.HasAlarm(host, "no_contact")
	}

	return false, false
}

func main() {
	for {
		hostList, err := blackboard.GetAllHosts()
		if err != nil {
			naughtyLogger.Warning("skynet", err.Error())
			continue
		}

		// Process all hosts
		for _, hostname := range hostList {
			naughtyLogger.Debug("skynet", "Analysing "+hostname)
			hostEntry, err2 := blackboard.GetHostData(hostname)
			if err2 != nil {
				naughtyLogger.Error("skynet", hostEntry.Name+" not in blackboard anymore "+err2.Error())
				continue
			}

			// Delete hosts that have no new exception in 6h and there is no decision yet
			if hasNoNewAlarms(hostEntry) {
				blackboard.DeleteHost(hostEntry.Name)
				naughtyLogger.Info("skynet", "Host "+hostEntry.Name+" has no new exception for 6h. Deleting from naughtystep.")
				continue
			}

			// Ignore hosts that are in the blackboard for less than 1h
			timeSinceFirstAlarm, _ := blackboard.TimeSinceFirstAlarm(hostEntry)
			if timeSinceFirstAlarm.Hours() < 1 {
				naughtyLogger.Debug("skynet", "Ignoring "+hostEntry.Name+": time since first alarm less than 1h")
				continue
			}

			// Check for recurrent alarms
			hasIgnored, hasRecurrent := hasRecurringAlarm(hostEntry)
			if hasIgnored {
				handler, _ := handlers.GetHandler("ignored")
				handler.Handle(hostEntry)
				continue
			}
			if hasRecurrent {
				handler, _ := handlers.GetHandler("recurrent")
				handler.Handle(hostEntry)
				continue
			}

			// If something goes wrong, it is safe to assume that it's draining
			isDraining, err := roger.IsDraining(hostEntry.Name)
			if err != nil {
				naughtyLogger.Error("skynet", "Couldn't check if host "+hostEntry.Name+" is draining: "+err.Error())
			}
			if isDraining || err != nil {
				naughtyLogger.Debug("skynet", "Deleting "+hostEntry.Name+": is draining")
				blackboard.DeleteHost(hostEntry.Name)
				continue
			}

			alarmSet := blackboard.GetUniqueAlarms(hostEntry)

			// Find if the host is pending for some action
			actionList := config.GetList("general", "actions")
			pendingAction := ""
			for _, action := range actionList {
				if actions.IsPending(hostEntry.Name, action) {
					pendingAction = action
					break
				}
			}

			// If the host is pending for some action, check if the states match
			if pendingAction != "" {
				state, err := actions.GetState(hostEntry.Name, pendingAction)
				if err != nil {
					naughtyLogger.Error("skynet", "Couldn't get state for "+hostEntry.Name+" pending for "+pendingAction+": "+err.Error())
					continue
				}
				declineAction := false
				missingAlarm := ""
				for _, alarm := range alarmSet {
					// Check if alarm is in state
					alarmPresent := false
					for _, stateAlarm := range state {
						if stateAlarm == alarm {
							alarmPresent = true
							break
						}
					}
					if !alarmPresent {
						declineAction = true
						missingAlarm = alarm
					}
				}
				if declineAction && len(state) != 0 {
					naughtyLogger.Debug("skynet", hostEntry.Name+" has a new "+missingAlarm+" which was not in the state for "+pendingAction)
					actions.DeclineAction(hostEntry.Name, pendingAction)
				} else {
					naughtyLogger.Debug("skynet", hostEntry.Name+" has no new alarms. Removing")
					blackboard.DeleteHost(hostEntry.Name)
					continue
				}
			}

			naughtyLogger.Debug("skynet", "Fixing "+hostEntry.Name)

			highestPriority := -1
			highestPriorityHandler := handlers.Handler(nil)
			for _, alarm := range alarmSet {
				naughtyLogger.Debug("skynet", "Getting handler for "+alarm+" for "+hostEntry.Name)
				handler, err := handlers.GetHandler(alarm)
				if err != nil {
					naughtyLogger.Error("skynet", "Couldn't get handler for "+alarm+": "+err.Error())
					continue
				}

				if highestPriority < handler.GetPriority() {
					highestPriorityHandler = handler
					highestPriority = handler.GetPriority()
				}
			}

			highestPendingPriority, action := actions.GetHighestPendingPriority(hostEntry.Name)
			naughtyLogger.Debug("skynet", hostEntry.Name+" is pending for an action with priority "+strconv.Itoa(highestPendingPriority)+" and highest priority handler has "+strconv.Itoa(highestPriority))

			// if no handler is available
			if highestPriorityHandler == nil {
				continue
			}

			if highestPendingPriority >= highestPriorityHandler.GetPriority() {
				naughtyLogger.Debug("skynet", "deleting "+hostEntry.Name+" already pending for sth important")
				blackboard.DeleteHost(hostEntry.Name)
				continue
			}

			if highestPendingPriority < highestPriorityHandler.GetPriority() && highestPendingPriority != -1 {
				naughtyLogger.Debug("skynet", "declining "+action+" on "+hostEntry.Name)
				actions.DeclineAction(hostEntry.Name, action)
			}

			requiredTests := highestPriorityHandler.RequiredTests(hostEntry)
			err = blackboard.EnableTests(hostEntry, requiredTests)
			if err != nil {
				naughtyLogger.Error("skynet", "While enabling tests for "+hostEntry.Name+": "+err.Error())
				continue
			}

			highestPriorityHandler.Handle(hostEntry)
			naughtyLogger.Debug("skynet", "Handled "+hostEntry.Name)
		}

		time.Sleep(600 * time.Second)
	}
}
