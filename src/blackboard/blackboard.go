// Tools to implement blackboard queries and operations on blackboard data
package blackboard

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"utils/config"
	"utils/naughtyLogger"
	"utils/timestamp"
)

type Host struct {
	Alarms    []Alarm              `json:"alarms"`
	Hostgroup string               `json:"hostgroup"`
	NotUntil  *timestamp.Timestamp `json:"notUntil"`
	Tests     map[string][]string  `json:"tests"`
	TryNo     int                  `json:"tryNo"`
	Name      string               `json:"name"`
	IsVirtual bool                 `json:"isVirtual"`
}

type Alarm struct {
	Timestamp *timestamp.Timestamp `json:"timestamp"`
	Uuid      string               `json:"uuid"`
	Exception string               `json:"exception"`
	Host      string               `json:"host"`
	Hostgroup string               `json:"hostgroup"`
}

// Get a list of all hosts available on the blackboard
func GetAllHosts() ([]string, error) {

	url := "http://" + config.Get("blackboard", "host") + "/v1/step"

	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, errors.New("Could not fetch the host list")
	}

	var hostList []string
	err = json.NewDecoder(response.Body).Decode(&hostList)

	return hostList, err
}

// TODO: move from here to the action interface
func GetHostsPendingAction(action string) ([]string, error) {
	url := "http://" + config.Get("blackboard", action) + "/v1/step/actions/" + action

	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, errors.New("Could not fetch the host list")
	}

	var hostList []string
	err = json.NewDecoder(response.Body).Decode(&hostList)

	return hostList, err
}

// TODO: move from here to the action interface
func IsAvailable(action string, hostname string) (bool, error) {
	url := "http://" + config.Get("blackboard", action) + "/v1/step/" + hostname + "/actions/" + action

	response, err := http.Get(url)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return false, errors.New("Could not fetch the host list")
	}

	var available struct {
		Available bool `json:"available"`
	}
	err = json.NewDecoder(response.Body).Decode(&available)

	return available.Available, err
}

func GetHostsWithAlarm(alarm string) ([]string, error) {
	url := "http://" + config.Get("blackboard", "host") + "/v1/step" + alarm

	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, errors.New("Could not fetch the host list")
	}

	var hostList []string
	err = json.NewDecoder(response.Body).Decode(&hostList)

	return hostList, err
}

// Get the details of a host from the blackboard
func GetHostData(hostname string) (Host, error) {

	url := "http://" + config.Get("blackboard", "host") + "/v1/step/" + hostname

	response, err := http.Get(url)
	if err != nil {
		return Host{}, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return Host{}, errors.New("Could not fetch the host data")
	}

	var hostData Host
	err = json.NewDecoder(response.Body).Decode(&hostData)

	hostData.Name = hostname

	return hostData, err
}

// Run again the tests on the given host
func Retry(hostname string, retryTime string) error {
	url := "http://" + config.Get("blackboard", "host") + "/v1/step/" + hostname + "/retry"

	response, err := http.Post(url, "application/json", strings.NewReader(retryTime))
	if err != nil {
		naughtyLogger.Error("skynet", err.Error())
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		msg := "Retry request failed with status: " + response.Status + " (" + strconv.Itoa(response.StatusCode) + ")"
		naughtyLogger.Error("skynet", msg)
		return errors.New(msg)
	}

	return nil
}

// Delete a host from the blackboard
func DeleteHost(hostname string) error {

	errorBase := "Could not delete the host " + hostname

	url := "http://" + config.Get("blackboard", "host") + "/v1/step/" + hostname
	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		naughtyLogger.Error("skynet", errorBase+": cannot create request")
		return err
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		naughtyLogger.Error("skynet", errorBase+": request failed")
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		msg := errorBase + ": request has status " + response.Status
		naughtyLogger.Error("skynet", msg)
		return errors.New(msg)
	}

	return nil
}

// Enable a test for a given host
func enableTest(hostname string, test string) error {
	url := "http://" + config.Get("blackboard", "host") + "/v1/step/" + hostname + "/enable/" + test

	response, err := http.Post(url, "application/json", nil)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return errors.New("Could not enable " + test + " on " + hostname)
	}
	return nil
}

// Check if a test is enabled for a given host
func TestEnabled(host Host, target string) bool {
	for test, _ := range host.Tests {
		if test == target {
			return true
		}
	}
	return false
}

// Enable multiple tests for a given host
func EnableTests(host Host, tests []string) error {
	for _, test := range tests {
		if TestEnabled(host, test) != true {
			err := enableTest(host.Name, test)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// Get a set of unique alarms for this host
func GetUniqueAlarms(host Host) []string {

	numberOfUniqueAlarms := 0
	alarmSet := make(map[string]bool)

	for _, alarm := range host.Alarms {
		alarmSet[alarm.Exception] = true
		numberOfUniqueAlarms++
	}

	alarmList := make([]string, 0, numberOfUniqueAlarms)
	for alarm, _ := range alarmSet {
		alarmList = append(alarmList, alarm)
	}
	return alarmList
}

// Check if the given host has an alarm present
func HasAlarm(host Host, alarmName string) bool {
	for _, alarm := range host.Alarms {
		if alarm.Exception == alarmName {
			return true
		}
	}
	return false
}

// Compute the time passed since last alarm, in seconds
func TimeSinceLastAlarm(host Host) (time.Duration, error) {

	if len(host.Alarms) == 0 {
		return time.Since(time.Now()), errors.New("Host has no alarms")
	}

	timeSinceLastAlarm := host.Alarms[0].Timestamp.Time

	for _, alarm := range host.Alarms {
		if timeSinceLastAlarm.Before(alarm.Timestamp.Time) {
			timeSinceLastAlarm = alarm.Timestamp.Time
		}
	}

	return time.Now().Sub(timeSinceLastAlarm), nil
}

// Compute the time passed since first alarm, in seconds
func TimeSinceFirstAlarm(host Host) (time.Duration, error) {

	if len(host.Alarms) == 0 {
		return time.Since(time.Now()), errors.New("Host has no alarms")
	}

	timeSinceFirstAlarm := host.Alarms[0].Timestamp.Time

	for _, alarm := range host.Alarms {
		if timeSinceFirstAlarm.After(alarm.Timestamp.Time) {
			timeSinceFirstAlarm = alarm.Timestamp.Time
		}
	}

	return time.Now().Sub(timeSinceFirstAlarm), nil
}

// Check if the results of a list of tests are present for the current test run
func TestsResultsPresent(host Host, tests []string) bool {
	retVal := true
	for _, test := range tests {
		testResults, testEnabled := host.Tests[test]
		if !testEnabled || testEnabled && len(testResults) > host.TryNo && testResults[host.TryNo] == "" {
			retVal = false
		}
	}
	return retVal
}

func TestsPassed(host Host, expected map[string]string) bool {
	retVal := true
	for testName, expectedValue := range expected {
		testResults, testEnabled := host.Tests[testName]
		if !testEnabled || testEnabled && len(testResults) > host.TryNo && testResults[host.TryNo] != expectedValue {
			retVal = false
		}
	}
	return retVal
}
