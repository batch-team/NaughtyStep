#!/usr/bin/python
import sys
import os
import logging
import json
import requests
import time

from dirqconsumer import DirqConsumerBase
from naughtystep.utils import config


loggerHost = config.Config().get('naughtyLogger', 'host')
loggerInfo = 'http://{0}/v1/step/log/info/gni_consumer'.format(loggerHost)
loggerError = 'http://{0}/v1/step/log/error/gni_consumer'.format(loggerHost)
loggerDebug = 'http://{0}/v1/step/log/debug/gni_consumer'.format(loggerHost)

class SimpleMonitoringConsumer(DirqConsumerBase):

    NUM_RETRIES_ERROR_MSG = 5

    def __init__(self, dq_path, endpoint, step_sec=0.5):
        DirqConsumerBase.__init__(self, dq_path, step_sec)
        self.endpoint = endpoint
        self.ename_msg_with_errors = {}
        self.forwarded_exceptions = config.Config().get("gni_consumer",
                                                        "forwarded_exceptions")

    def _setup(self):
        requests.post(loggerInfo, "Waiting for new messages from monitoring topics")
        return True

    def _process(self, msg):
        header, body = msg.header, json.loads(msg.body)
        metadata, data = body.get("metadata", {}), body.get("payload", {})
        print 'Got a', metadata['description'], 'for', metadata['entity'], 'with timestamp', metadata['timestamp'], 'at', int(time.time())

        if 'm_toplevel_hostgroup' in header.keys() and header['m_toplevel_hostgroup'] == 'bi' and (metadata["hostgroup"].startswith("bi/batch") or metadata["hostgroup"].startswith("bi/condor")):
            if metadata['description'].startswith('exception.'):
                exception = metadata['description'][10:]
            else:
                exception = metadata['description']

            if exception in self.forwarded_exceptions:
                payload = json.dumps({"exception": exception,
                                      "uuid": metadata["uuid"],
                                      "timestamp": metadata["timestamp"],
                                      "host": metadata["entity"],
                                      "hostgroup": metadata["hostgroup"]})
                url = self.endpoint + '/v1/step/{0}.cern.ch/alarm/{1}'.format(metadata["entity"], exception)
            else:
                # Other exceptions that we don't consider
                requests.post(loggerDebug, 'Message not processed (exception not analysed): type=%s' % (metadata["description"]))
                return True

            try:
                re = requests.post(url, data=payload, headers={'Content-type': 'application/json'})
            except requests.exceptions.Timeout as err:
                requests.post(loggerInfo, "Messsage not sent, going to try again later. Reason: %s" % err)
                # save the message to be processed later
                return False
            except requests.exceptions.TooManyRedirects:
                requests.post(loggerInfo, "Bad URL, please try a different one.")
                return False
            except requests.exceptions.RequestException as err:
                requests.post(loggerInfo, "Connection problem, retrying later: %s" % err)
                return False
            return True
        # Consume everything else (otherwise too many files are generated)
        return True

    def _handle_process_error(self, ename, msg, exception):
        self.__handle_error_retries(ename, msg)

    def _handle_msg_error(self, ename, exception):
        self.__handle_error_retries(ename)

    def _handle_error(self, ename, exception):
        self.__handle_error_retries(ename)

    def __handle_error_retries(self, ename, msg=None):
        if self.ename_msg_with_errors.get(ename):
            self.ename_msg_with_errors[ename] += 1
        else:
            self.ename_msg_with_errors[ename] = 1

        if self.ename_msg_with_errors[ename] >= SimpleMonitoringConsumer.NUM_RETRIES_ERROR_MSG:
            requests.post(loggerError, "More than 5 errors for the processing of one message, we discard it")
            if msg:
                requests.post(loggerDebug, "Skipping this message: %s" % msg)
            self.dirq.remove(ename)
        else:
            requests.post(loggerError, "There was an error during the processing of the message, we will try later")
            self.dirq.unlock(ename, permissive=True)

