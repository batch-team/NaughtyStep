""" Module that implements the kill action """

import os
import subprocess
import requests

from naughtystep.actions import action
from naughtystep.utils import errors

class Kill(action.BaseAction):
    """ The kill action """


    @staticmethod
    def name():
        return "kill"


    def setVariables(self, hostname):
        """ Get a nova server fo the given hostname """
        hostLocatorHost = self.config.get('hostLocator', 'host')
        hostLocatorUrl = "http://{0}/v1/step/hostlocator/{1}".format(hostLocatorHost, hostname)

        response = requests.get(hostLocatorUrl)
        if response.status_code != 200:
            raise errors.HostLocatorLookupError("Host Locator returned {0} on {1}".format(response.status_code, hostLocatorUrl))

        hostLocation = response.json()

        if "OS_TENANT_ID" in os.environ:
            del os.environ["OS_TENANT_ID"]
        if "OS_PROJECT_ID" in os.environ:
            del os.environ["OS_PROJECT_ID"]
        if "OS_PROJECT_NAME" in os.environ:
            del os.environ["OS_PROJECT_NAME"]

        env = {
            'OS_PROJECT_DOMAIN_ID':'default',
            'OS_IDENTITY_API_VERSION':'3',
            'OS_AUTH_TYPE':'v3kerberos',
            'OS_AUTH_URL':'https://keystone.cern.ch/krb/v3',
            'OS_PROJECT_NAME':hostLocation['TenantName']}

        return env


    def performAction(self, hostname):
        pipe = subprocess.Popen(["ai-kill-vm", hostname], env=self.setVariables(hostname))
        pipe.communicate()

        if pipe.returncode is not 0:
            raise errors.ShellProcessError("ai-kill-vm returned " + str(pipe.returncode) + " " + str(pipe.stderr) + " " + str(pipe.stdout))


application = Kill().getApp()
