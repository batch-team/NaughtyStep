""" Implements manual intervention action """

import json

from flask import Response
from flask import request

from naughtystep.actions import action

class Manual(action.BaseAction):
    """ Implements manual intervention action """

    def addHost(self, hostname):
        """ Override BaseAction.addHost to allow adding multiple entries for the same host """

        missingField = self.checkRequest(request.json)
        if missingField:
            return Response("Field " + missingField + " is missing or has unexpected value", status=400)

        if self.redis.hexists(self.hashName, hostname):
            entry = json.loads(self.redis.hget(self.hashName, hostname))
            entry['reason'].append(request.json['reason'])
        else:
            entry = request.json
            entry['reason'] = [entry['reason']]

        self.redis.hset(self.hashName, hostname, json.dumps(entry))

        self.logger.log('info', hostname + ' is now pending ' + self.name())
        return Response("OK", status=201)


    @staticmethod
    def name():
        return "manual"


    def performAction(self, hostname):
        """ Approving a manual action does not do anything, just removes the
            host from the list """
        pass

application = Manual().getApp()

