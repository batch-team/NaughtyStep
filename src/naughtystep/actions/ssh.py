""" SSH remote execution module

    Upon approval, a script is executed on the host and some checks are made
                   that the execution was successful.
"""

import socket
import random
import subprocess
import requests
import json

from naughtystep.actions import action
from naughtystep.utils import errors

from requests_kerberos import HTTPKerberosAuth

class Ssh(action.BaseAction):
    """ Ssh action """


    def getRootPassword(self, host):
        """ Do a teigi request to get the root password for a host """

        if not host.endswith('.cern.ch'):
            host += '.cern.ch'

        headers = {'Accept-Encoding': 'deflate', 'Accept': 'application/json'}

        teigiIP = random.choice(socket.gethostbyname_ex('woger.cern.ch')[2])
        teigiHostname = socket.gethostbyaddr(teigiIP)[0]

        url = "https://" + teigiHostname + ":8201/tellme/v1/host/" + host + "/account/root/"

        response = requests.get(url, timeout=10, headers=headers,
                                auth=HTTPKerberosAuth(),
                                verify=self.config.get('security', 'CERN_CA_BUNDLE'),
                                allow_redirects=True)

        return response.json()["password"]


    @staticmethod
    def name():
        return "ssh"


    def checkRequest(self, req):
        baseCheck = super(Ssh, self).checkRequest(req)
        if baseCheck is not None:
            return baseCheck

        for field in ['script', 'auth']:
            if field not in req:
                return field
        return None


    def performAction(self, hostname):

        hostDetails = json.loads(self.redis.hget(self.hashName, hostname))

        if 'auth' not in hostDetails:
            raise errors.NoAuthenticationMethod("No authentication method for " + hostname)

        if 'script' not in hostDetails:
            raise errors.NoScript("No script for " + hostname)

        scriptName = hostDetails['script'] + '.sh'
        scriptPath = self.config.get('ssh', 'scriptRepository') + scriptName

        authMethod = hostDetails['auth']
        if authMethod == 'krb':
            cmd = ['timeout 1660 ssh -T -oStrictHostKeyChecking=no -oPreferredAuthentications=gssapi-with-mic -oConnectTimeout=10 root@' + hostname + ' < ' + scriptPath]
        elif authMethod == 'password':
            password = self.getRootPassword(hostname)
            cmd = ['timeout 1660 sshpass -p' + password + ' ssh -T -oStrictHostKeyChecking=no -oConnectTimeout=10 root@' + hostname + ' < ' + scriptPath]
        else:
            raise errors.UnknownAuthenticationMethod('Unknown authentication method: ' + authMethod)

        pipe = subprocess.Popen(cmd, shell=True)
        out, err = pipe.communicate()

        if pipe.returncode != 0:
            raise errors.ShellProcessError('Shell script {0} exited with status {1}.'.format(scriptName, pipe.returncode))

application = Ssh().getApp()
