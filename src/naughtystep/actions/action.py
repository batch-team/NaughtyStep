""" Base class for actions: provides common methods for all actions """

import json
import requests
import redis

from flask import Response
from flask import Flask
from flask import request

from naughtystep.utils import errors
from naughtystep.utils import config
from naughtystep.utils import logging


class BaseAction(object):
    """ Base class for actions: """

    def __init__(self):
        self.hashName = 'pending_' + self.name()

        self.config = config.Config()
        self.redis = redis.Redis()
        self.logger = logging.Log(self.name())

        self.defaultPriority = 50


    @staticmethod
    def name():
        """ Returns a string with the name of this action for debugging/logging """
        raise NotImplementedError


    def checkRequest(self, req):
        """ Returns None if the request contains all the required fields,
            a string containing the problematic field otherwise """
        for field in ['reason', 'available', 'hostgroup']:
            if field not in req:
                return field

        return None


    def addHost(self, hostname):
        """ Add a host to the pending hosts list for this action """

        if self.redis.hexists(self.hashName, hostname):
            return Response("Host already pending for " + self.name(), status=409)

        missingField = self.checkRequest(request.json)
        if missingField:
            return Response("Field " + missingField + " is missing or has unexpected value", status=400)

        entry = request.json

        if 'priority' not in entry:
            entry['priority'] = self.defaultPriority

        self.redis.hset(self.hashName, hostname, json.dumps(entry))

        self.logger.log('info', hostname + ' is now pending ' + self.name())
        return Response("OK", status=201)


    def hostDetails(self, hostname):
        """ Return the details of a host """

        if self.redis.hexists(self.hashName, hostname):
            return Response(self.redis.hget(self.hashName, hostname),
                            status=200, mimetype='application/json')

        return Response("Not Found", status=404)


    def listAll(self):
        """ List all the hosts pending for action """
        js = json.dumps(self.redis.hkeys(self.hashName))
        return Response(js, status=200, mimetype='application/json')


    def performAction(self, hostname):
        """ Perform the action - should be overwritten by action classes """
        raise NotImplementedError


    def addToLedger(self, hostname):
        """ Add hostname to ledger """

        ledgerHost = self.config.get('ledger', 'host')
        ledgerUrl = "http://{0}/v1/step/{1}/ledger".format(ledgerHost,
                                                           hostname)
        data = {'action': self.name(),
                'reason': json.dumps(json.loads(self.redis.hget(self.hashName, hostname))['reason'])}
        requests.post(ledgerUrl, json=data)


    def approve(self, hostname):
        """ Approve the action for a given host """

        if not self.redis.hexists(self.hashName, hostname):
            return Response("Not Found", status=404)

        self.logger.log('info', '{0} approved for host {1}'.format(self.name(), hostname))

        try:
            self.performAction(hostname)
            self.addToLedger(hostname)
            self.redis.hdel(self.hashName, hostname)
            return Response("OK", status=200)
        except errors.ActionError as error:
            self.logger.log('error', '{0} failed on {1}: {2}'.format(self.name(), hostname, error))

            url = 'http://{0}/v1/step/{1}/actions/manual'.format(self.config.get('manual', 'host'), hostname)
            entry = json.loads(self.redis.hget(self.hashName, hostname))
            data = {'reason': self.name() + ' failed (' + error.__class__.__name__ + ': ' + str(error) + ')',
                    'available': entry['available'],
                    'hostgroup': entry['hostgroup']}
            response = requests.post(url, json=data)

            if response.status_code == 201:
                self.redis.hdel(self.hashName, hostname)
            else:
                logMsg = '{0} failed for {1} but could not be added to manual ({2}: {3})'.format(self.name(), hostname, response.status_code, response.text)
                self.logger.log('error', logMsg)

            return Response(error.__class__.__name__ + ': ' + str(error), status=400)


    def decline(self, hostname):
        """ Decline the action for a given host """

        if not self.redis.hexists(self.hashName, hostname):
            return Response("Not Found", status=404)

        logMsg = '{0} declined for host {1}: {2}'.format(self.name(),
                                                         hostname,
                                                         self.redis.hget(self.hashName, hostname))
        self.logger.log('info', logMsg)

        self.redis.hdel(self.hashName, hostname)
        return Response("OK", status=200)


    def getApp(self):
        """ Return a flask application ready to run """
        app = Flask(__name__)

        app.add_url_rule('/v1/step/<hostname>/actions/' + self.name(),
                         methods=['POST'], view_func=self.addHost)

        app.add_url_rule('/v1/step/<hostname>/actions/' + self.name(),
                         methods=['GET'], view_func=self.hostDetails)

        app.add_url_rule('/v1/step/actions/' + self.name(),
                         methods=['GET'], view_func=self.listAll)

        app.add_url_rule('/v1/step/<hostname>/actions/' + self.name() + '/approve',
                         methods=['POST'], view_func=self.approve)

        app.add_url_rule('/v1/step/<hostname>/actions/' + self.name() + '/decline',
                         methods=['POST'], view_func=self.decline)

        return app

