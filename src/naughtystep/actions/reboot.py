""" Module that implements the reboot action """

import subprocess
import requests
import json

from keystoneclient_kerberos import v3
from keystoneauth1 import session
from novaclient import client
from novaclient.exceptions import Conflict
from novaclient.exceptions import NotFound

from naughtystep.actions import action

from naughtystep.utils import misc
from naughtystep.utils import errors
from naughtystep.utils import novaUtils

class Reboot(action.BaseAction):
    """ The reboot action """

    def _getSession(self, project=None):
        """ Creates an openstack session for the given project.

        :param project: The project to authenticate to.
                        Authenticates to project from env variable
                        OS_TENANT_NAME if not specified.
        """
        credentials = novaUtils.get_keystone_credentials_v3(project)
        print credentials
        auth = v3.Kerberos(**credentials)
        return session.Session(auth=auth)



    def getServer(self, hostname):
        """ Get a nova server fo the given hostname """
        hostLocatorHost = self.config.get('hostLocator', 'host')
        hostLocatorUrl = "http://{0}/v1/step/hostlocator/{1}".format(hostLocatorHost, hostname)

        response = requests.get(hostLocatorUrl)
        if response.status_code != 200:
            raise errors.HostLocatorLookupError("Host Locator returned {0} on {1}".format(response.status_code, hostLocatorUrl))

        hostLocation = response.json()
        novaClient = client.Client(version='2', session=self._getSession(hostLocation['TenantName']))
        try:
            return novaClient.servers.get(hostLocation['UUID'])
        except NotFound:
            raise errors.NovaError(
                    'Could not find client for {0}. Probably the VM was deleted'.format(hostname))


    def rebootPhysical(self, hostname):
        """ Reboot a physical host

            Try running the reboot command over SSH initially, to avoid
            having IPMI issues. If that fails, try a IPMI reboot.
        """

        try:
            misc.doSsh(hostname, 'reboot')
        except errors.SshError:
            cmd = ['ai-remote-power-control', 'cycle', '-e', hostname]
            pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, _ = pipe.communicate()

            if pipe.returncode != 0:
                raise errors.ShellProcessError(out[:-1])


    def checkRequest(self, req):
        baseCheck = super(Reboot, self).checkRequest(req)
        if baseCheck is not None:
            return baseCheck

        for field in ['virtual']:
            if field not in req:
                return field
        return None


    @staticmethod
    def name():
        return "reboot"


    def performAction(self, hostname):
        if json.loads(self.redis.hget('pending_reboot', hostname))['virtual']:
            try:
                server = self.getServer(hostname)
                server.reboot(reboot_type='HARD')
            except Conflict:
                raise errors.NovaError('conflict')
        else:
            self.rebootPhysical(hostname)

application = Reboot().getApp()
