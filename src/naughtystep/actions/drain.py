""" Module that implements the drain action """

import time
import json
from datetime import datetime

from naughtystep.actions import action

from batchfactory.roger_client import RogerClient


class Drain(action.BaseAction):
    """ The drain action """


    def __init__(self):
        super(Drain, self).__init__()
        self.drainingHosts = {}


    @staticmethod
    def name():
        return "drain"


    def checkRequest(self, req):

        baseCheck = super(Drain, self).checkRequest(req)
        if baseCheck is not None:
            return baseCheck

        for field in ['type']:
            if field not in req:
                return field

        if req['type'] not in self.config.get("drain", "types"):
            return 'type'

        return None


    def performAction(self, hostname):
        cfg = {'roger': {'url': 'woger.cern.ch', 'port':8201, 'uri':'/roger/v1/state'}}
        roger = RogerClient(cfg)

        hostDetails = json.loads(self.redis.hget(self.hashName, hostname))

        drainType = hostDetails['type']
        reason = hostDetails['reason']

        roger.set_appstate(drainType, hostname)

        try:
            customDuration = int(hostDetails['time']) + int(time.time())
            humanReadable = datetime.fromtimestamp(customDuration).strftime('%H:%M:%S %d/%m/%Y')
            message = '%s by %s (%s) - %s' % (drainType, customDuration, humanReadable, reason)
        except KeyError:
            message = reason

        roger.update_message(message, hostname)

application = Drain().getApp()
