#!/bin/bash

if [ ! -d /etc/drain.d ]; then
    exit 0
fi

for file in /etc/drain.d/check_*
do
    bash $file
    if [ $? != 0 ]; then
	fix="/etc/drain.d/fix_$(echo $file | cut -d'_' -f2-)"
        if [ ! -f "$fix" ]; then
            (>2& echo "$fix not found")
            exit 1
        fi

        timeout 1600 bash $fix
        if [ $? != 0 ]; then
            (>2& echo "Error while running $fix")
            exit 1
        fi
    fi
done
