#!/bin/bash

killall -9 rpm
pkill -9 -f yum
pkill -9 -f cern-rpmverify
pkill -9 -f "puppet agent"

rm -rf /var/lib/rpm/.rpm.lock
rm -rf /var/lib/rpm/__db*

rpm --rebuilddb
