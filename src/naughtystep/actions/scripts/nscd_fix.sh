#!/bin/bash

service nscd stop
rm -f /var/run/nscd/nscd.pid
rm -f /var/lock/subsys/nscd
service nscd start
