import os
import json
import time
import threading

from flask import Flask
from flask import Response

from keystoneclient_kerberos import v3
from keystoneauth1 import session
from keystoneauth1.exceptions import Unauthorized
from keystoneclient.v3 import client
from novaclient import client as nova_client

from naughtystep.utils import novaUtils
from naughtystep.utils import config
from naughtystep.utils import logging

class HostLocator(object):

    def __init__(self):
        self.hostLocations = {}
        self.neighbours = {}
        self.lastRefresh = 0
        self.lock = threading.Lock()
        self.cacheValidity = config.Config().get('hostLocator', 'validity')
        self.notCaching = config.Config().get('hostLocator', 'notCaching')
        self.logger = logging.Log('hostLocator')


    def _getSession(self, project=None):
        """ Creates an openstack session for the given project.

        :param project: The project to authenticate to.
                        Authenticates to project from env variable
                        OS_TENANT_NAME if not specified.
        """
        credentials = novaUtils.get_keystone_credentials_v3(project)
        auth = v3.Kerberos(**credentials)
        return session.Session(auth=auth)


    def refreshCache(self):
        hypervisors = {}

        keystone = client.Client(session=self._getSession())
        projects = keystone.projects.list(user=novaUtils.get_user())

        for project in projects:
            if project.enabled is False or project.name in self.notCaching:
                continue

            nova = nova_client.Client(version='2', session=self._getSession(project.name))

            try:
                servers = nova.servers.list()
            except Unauthorized:
                self.logger.log('error', 'Unauthorized for ' + project.name)
                continue


            for server in servers:
                try:
                    hypervisor = getattr(server, 'OS-EXT-SRV-ATTR:hypervisor_hostname')
                except AttributeError:
                    errorMsg = '{0} has no hypervisor details (from {1})'.format(server, project.name)
                    self.logger.log('error', errorMsg)
                    print errorMsg
                    continue

                self.hostLocations[server.name + '.cern.ch'] = {'name': server.name + '.cern.ch',
                                                                'UUID': server.id,
                                                                'TenantName': project.name,
                                                                'TenantId': project.id,
                                                                'hypervisor': hypervisor}
                try:
                    hypervisors[hypervisor].append(server.name)
                except KeyError:
                    hypervisors[hypervisor] = [server.name]

            for hypervisor, hosts in hypervisors.iteritems():
                for host in hosts:
                    self.neighbours[host] = hosts

    def isInvalid(self):
        return int(time.time()) - self.lastRefresh > self.cacheValidity


    def refresh(self):
        self.lock.acquire()
        self.logger.log('info', 'Cache refresh started')
        self.refreshCache()
        self.logger.log('info', 'Cache refresh finished')
        self.lastRefresh = int(time.time())
        self.lock.release()


    def refreshViewfunc(self):
        """ Wrapper over self.refresh to be used as a flask viewfunc """
        self.refresh()
        return Response('OK', status=200)


    def locateHost(self, host):
        if not host.endswith('.cern.ch'):
            host += '.cern.ch'

        if self.isInvalid():
            self.refresh()

        try:
            return Response(json.dumps(self.hostLocations[host]),
                            status=200, mimetype='application/json')
        except KeyError:
            return Response('Not found', status=404, mimetype='application/json')


    def getNeighbours(self, host):
        if self.isInvalid():
            self.refresh()

        try:
            return Response(json.dumps(self.neighbours[host]),
                            status=200, mimetype='application/json')
        except KeyError:
            return Response('Not found', status=404, mimetype='application/json')


    def getAllHostsInTenant(self, tenant):
        if self.isInvalid():
            self.refresh()

        hosts = [host for host, location in self.hostLocations.iteritems() if location['TenantName'] == tenant]

        return Response(json.dumps(hosts),
                        status=200, mimetype='application/json')


    def getApp(self):
        """ Return a flask application ready to run """
        app = Flask(__name__)

        app.add_url_rule('/v1/step/hostlocator/<host>',
                         methods=['GET'], view_func=self.locateHost)

        app.add_url_rule('/v1/step/hostlocator/<host>/neighbours',
                         methods=['GET'], view_func=self.getNeighbours)

        app.add_url_rule('/v1/step/hostlocator/<tenant>/tenant',
                         methods=['GET'], view_func=self.getAllHostsInTenant)

        app.add_url_rule('/v1/step/hostlocator/refresh',
                         methods=['GET'], view_func=self.refreshViewfunc)

        return app

application = HostLocator().getApp()

if __name__ == '__main__':
    hostlocator = HostLocator()
    r= hostlocator.locateHost('b6a94243d2.cern.ch')
    print r.get_data()
#    print hostlocator.locateHost('b6a94243d2')

