""" The NaughtyStep approver.

    This component approves (i.e. triggers the execution) of various actions,
    as decided by the skynet, while making sure that not too many machines
    of a hostgroup (here, service) are rendered unavailable in the process.
"""

import json
import argparse
import sys
from pprint import pprint
from tqdm import tqdm

from batchfactory.pdbclient import PDBClient
from batchfactory.roger_client import RogerClient
from naughtystep.utils import config
from naughtystep.utils import misc
from naughtystep.utils import errors
from naughtystep.utils import hostLocator
from naughtystep.utils import logging


class Approver(object):
    """ Encapsulates the approver functionality """

    def __init__(self):
        cfg = config.Config()
        self.roger = RogerClient({'roger': cfg.get('services', 'roger')})
        self.pdb = PDBClient()
        self.logger = logging.Log('approver')
        self.drainingStates = cfg.get('general', 'draining_states')
        self.actions = cfg.get('general', 'actions')
        self.services = self.loadServices()


    def validateConfig(self, services):
        """ Validates the services.json config file.

            If a problem is detected, an error message is printed and the
            program is exited.
        """
        for service, value in services.iteritems():
            if value['type'] not in ['hostgroup', 'tenant']:
                print 'Unknown service type', value['type'], 'for', service
                sys.exit(1)


    def logStatus(self):
        """ Send some debugging info to the logger and to stdout """

        outputStr = 'Approver status:\n'
        for service, value in self.services.iteritems():
            outputStr += 'In {0},\n'.format(service)
            outputStr += '       total: {0}\n'.format(value['current'])
            outputStr += '    draining: {0}\n'.format(value['draining'])
            outputStr += '       limit: {0}\n'.format(value['limit'])
            outputStr += '   exceeding: {0}\n'.format(value['exceeding'])

        self.logger.log('info', outputStr)

        print outputStr


    def loadServices(self):
        """ Build the services data structure

            Reads the services from the config file and computes their
            current status (i.e. number of present hosts, number of available
            hosts, etc)

            :return: A dictionary representing the services and their details:
        """
        with open('/etc/naughtystep/services.json') as serviceConfig:
            services = json.load(serviceConfig)

        self.validateConfig(services)

        self.tenants = [srv for srv in services.keys() if services[srv]['type'] == 'tenant']
        self.hostgroups = [srv for srv in services.keys() if services[srv]['type'] == 'hostgroup']

        for service, value in services.iteritems():
            if value['type'] == 'hostgroup':
                hosts = [host['certname'] for host in self.pdb.get_hostgroup_nodes(service)[1]]
            else:
                hosts = hostLocator.getAllHostsTenant(service)

            value['current'] = len(hosts)
            value['draining'] = len(self.getDrainingHosts(hosts))
            value['exceeding'] = int(value['current'] * value['limit'] / 100.) - value['draining']
            value['pending'] = []

        services['unknown'].update({'current': 0, 'draining': 0, 'exceeding': 0, 'pending': []})

        return services


    def isInProduction(self, hostname):
        """ Check if a host has a non-draining appstate

        :param hostname: The host fqdn of the host to check
        :return: True if the host's appstate from roger is production or qa,
                 False otherwise

        :raise naughtystep.utils.errors.RogerQueryFailed: If the roger query
                                                          fails
        """
        success, appstate = self.roger.get_appstate(hostname)
        if not success:
            raise errors.RogerQueryFailed

        return appstate not in self.drainingStates


    def getDrainingHosts(self, hosts):
        """ Get a list of all draining hosts in a hostgroup

        Works by querying PDB for a list of hosts in the given hostgroup and
        then checking each one's roger state

        :param hostgroup:
        :return: A list of draining hostnames or hostnames for which the
                 state could not be determinated
        """
        drainingHosts = []
        for host in tqdm(hosts):
            try:
                if not self.isInProduction(host):
                    drainingHosts.append(host)
            except errors.RogerQueryFailed:
                # If roger fails, assume node is draining (conservative scenario)
                drainingHosts.append(host)

        return drainingHosts


    def getServiceHandle(self, host, hostgroup):

        try:
            tenant = hostLocator.getTenant(host)
            if tenant in self.tenants:
                return self.services[tenant]
        except errors.NotFound:
            # The machine is probably physical
            pass

        try:
            return self.services[next(s for s in self.hostgroups if hostgroup.startswith(s))]
        except StopIteration:
            return self.services['unknown']


    def run(self, dryrun=True):
        """ Run the approver

        :param dryrun: If true, no actual action is performed
        """

        for action in self.actions:
            hostList = misc.getHosts(action)

            for host in hostList:
                try:
                    hostDetails = misc.getHostDetails(host, action)
                except errors.ActionQueryFailed:
                    continue
                hostEntry = {'action': action,
                             'available': hostDetails['available'],
                             'priority': hostDetails.get('priority', 100)}

                serviceHandle = self.getServiceHandle(host, hostDetails['hostgroup'])

                # If the host is unavailable and draining, it would be subtracted twice
                # It's fine, since this is a safe strategy
                if hostDetails['available'] is False:
                    serviceHandle['exceeding'] -= 1

                serviceHandle['pending'].append((host, hostEntry))

        approved = {}

        for service, content in self.services.iteritems():
            approved[service] = {}
            content['pending'] = sorted(content['pending'],
                                        key=lambda x: x[1]['priority'], reverse=True)


        for service, content in self.services.iteritems():
            for hostname, details in content['pending']:
                action = details['action']

                if action not in content['approve']:
                    continue

                if details['available'] is False or content['exceeding'] > 0:
                    try:
                        if not dryrun:
                            misc.approveAction(hostname, action)

                        approved[service][hostname] = action

                        if details['available']:
                            content['exceeding'] -= 1

                    except errors.ActionApproveFailed as e:
                        print 'Failed approving {0}: {1}'.format(hostname, e)

        return approved


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--dryrun", action="store_true")
    args = parser.parse_args()

    r = Approver()
    pprint(r.run(args.dryrun))

