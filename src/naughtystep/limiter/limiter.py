""" Module that alerts when too many hosts in a hostgroup have the same
    exception
"""
import json
import requests

from pprint import pprint

from batchfactory import pdbclient
from naughtystep.utils import config
from naughtystep.utils import misc
from naughtystep.utils import logging
from naughtystep.utils import errors


def getNoOfHosts(hostgroup):
    """ Returns the number of hosts in a hostgroup """
    return len(pdbclient.PDBClient().get_hostgroup_nodes(hostgroup)[1])


class Limiter(object):
    """ Encapsulates the limiter functionality """

    def __init__(self):
        self.services = {}
        self.locked = []
        self.logger = logging.Log('limiter')
        self.config = config.Config()
        self.actions = self.config.get('general', 'actions')

        with open('/etc/naughtystep/limits.json') as serviceConfigFile:
            serviceConfig = json.load(serviceConfigFile)
            for serviceName, exceptions in serviceConfig.items():
                noOfHosts = getNoOfHosts(serviceName)
                self.services[serviceName] = {exception: {'max': noOfHosts * limit / 100,
                                                          'currentHosts': []}
                                              for exception, limit in exceptions.items()}


    def getService(self, hostgroup):
        """ Return the service to which a hostgroup belongs to """

        for service in self.services:
            if hostgroup.startswith(service):
                return self.services[service]
        return None


    def readHosts(self):
        """ Get hosts from the blackboard """

        url = 'http://{0}/v1/step'.format(self.config.get('blackboard', 'host'))
        hostList = requests.get(url).json()

        for host in hostList:
            try:
                hostDetails = misc.getHostDetails(host)
            except errors.QueryFailed as error:
                continue

            service = self.getService(hostDetails['hostgroup'])
            if service is None:
                continue

            exceptionSet = set(alarm['exception'] for alarm in hostDetails['alarms'])
            for exception in exceptionSet:
                if exception in service:
                    try:
                        service[exception]['currentHosts'].append((host, None))
                    except KeyError:
                        service[exception]['currentHosts'] = [(host, None)]


    def readActionHosts(self):
        """ Get hosts from actions """

        for action in self.actions:
            try:
                hosts = misc.getHosts(action)
            except errors.QueryFailed as error:
                self.logger.log('error', error)
                continue

            for host in hosts:
                try:
                    hostDetails = misc.getHostDetails(host, action)
                except errors.QueryFailed as error:
                    continue

                service = self.getService(hostDetails['hostgroup'])
                if service is None:
                    continue

                if 'state' not in hostDetails or hostDetails['state'] is None:
                    continue

                for exception in hostDetails['state']:
                    if exception in service:
                        try:
                            service[exception]['currentHosts'].append((host, action))
                        except KeyError:
                            service[exception]['currentHosts'] = [(host, action)]


    def lock(self, exception):
        self.locked.append(exception)

        self.logger.log('info', 'Locking {0}'.format(exception))

        url = 'http://{0}/v1/step/lock/{1}'.format(self.config.get('blackboard', 'host'), exception)
        requests.post(url)

        try:
            hosts = misc.getHosts()
        except errors.QueryFailed as error:
            self.logger.log('error', error)
        else:
            for host in hosts:
                url = 'http://{0}/v1/step/{1}/{2}'.format(self.config.get('blackboard', 'host'), host, exception)
                requests.delete(url)

        for action in self.actions:
            try:
                hosts = misc.getHosts(action)
                for host in hosts:
                    hostDetails = misc.getHostDetails(host, action)
                    if 'state' not in hostDetails:
                        print host, 'has no state'
                    elif hostDetails['state'] is None:
                        print host, 'has none state'


                    if 'state' in hostDetails and hostDetails['state'] is not None and exception in hostDetails['state']:
                        misc.declineAction(host, action)
            except (errors.ActionError, errors.QueryFailed) as error:
                self.logger.log('error', error)



    def checkHosts(self):
        """ Check if the number of hosts that have an exception exceeds the maximum allowed """

        for serviceName, service in self.services.items():
            for exceptionName, exception in service.items():
                if len(exception['currentHosts']) > exception['max'] and exceptionName not in self.locked:
                    print 'Locking', exceptionName
                    for entry in exception['currentHosts']:
                        host, action = entry
                        if action is not None:
                            misc.declineAction(host, action)
                    self.lock(exceptionName)
                    misc.addToAction('none', service, 'manual',
                                     'Too many {0} in {1}'.format(exceptionName, serviceName))


    def run(self):
        self.readHosts()
        self.readActionHosts()
        self.checkHosts()
        return {'services': self.services, 'locked': self.locked}


if __name__ == '__main__':
    pprint(Limiter().run())

