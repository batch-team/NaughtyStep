""" Utilities to interact with the host locator """

import requests
from naughtystep.utils import errors
from naughtystep.utils import config


hostlocator = config.Config().get('hostLocator', 'host')


def callHostLocator(url):
    """ Utility to make a query to the host locator

    :param url: The url to use
    :return: The parsed JSON response
    :raise errors.NotFound: If the query fails
    """
    response = requests.get(url)
    if response.status_code != 200:
        raise errors.NotFound
    return response.json()


def getAllHostsTenant(tenant):
    """ Get a list of hosts from a tenant """
    url = 'http://' + hostlocator  + '/v1/step/hostlocator/' + tenant + '/tenant'
    return callHostLocator(url)


def getNeighbours(host):
    """ Get the neighbours of a host """
    url = 'http://' + hostlocator + '/v1/step/hostlocator/' + host + '/neighbours'
    return callHostLocator(url)


def getTenant(host):
    """ Get the tenant of a host """
    url = 'http://' + hostlocator + '/v1/step/hostlocator/' + host
    return callHostLocator(url)['TenantName']


def getHypervisor(host):
    """ Get the hypervisor of a host """
    url = 'http://' + hostlocator + '/v1/step/hostlocator/' + host
    return callHostLocator(url)['hypervisor']

