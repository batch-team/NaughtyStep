""" Exceptions used by different actions """

class ActionError(Exception):
    """ Base class for exceptions thrown by actions """
    pass

class ShellProcessError(ActionError):
    """ A shell process has failed """
    pass

class HostLocatorLookupError(ActionError):
    """ A host locator query has failed """
    pass

class UnknownAuthenticationMethod(ActionError):
    pass

class NoAuthenticationMethod(ActionError):
    pass

class NoScript(ActionError):
    pass

class NovaError(ActionError):
    pass

class SshError(Exception):
    pass

class CommandFailed(Exception):
    pass

class ActionEndpointError(Exception):
    """ Base class for exceptions thrown while trying to access an endpoint
        of an action """
    pass

class ActionAddFailed(ActionEndpointError):
    pass

class ActionApproveFailed(ActionEndpointError):
    pass

class ActionDeclineFailed(ActionEndpointError):
    pass

class ActionQueryFailed(ActionEndpointError):
    pass

class QueryFailed(ActionEndpointError):
    pass

class UnknownType(Exception):
    pass

class BlackboardQueryFailed(Exception):
    pass

class RogerQueryFailed(Exception):
    pass

class PdbQueryFailed(Exception):
    pass

class NotFound(Exception):
    pass
