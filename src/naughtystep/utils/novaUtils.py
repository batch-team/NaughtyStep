import os

def get_keystone_credentials_v3(project=None):
    credentials = {}
    credentials['auth_url'] = os.environ['OS_AUTH_URL']
    credentials['project_name'] = os.environ['OS_TENANT_NAME'] if project is None else project
    credentials['project_domain_id'] = 'default'
    return credentials

def get_user():
    return os.environ['OS_USERNAME']

def get_keystone_credentials_v2():
    credentials = {}
    credentials['username'] = os.environ['OS_USERNAME']
    credentials['password'] = os.environ['OS_PASSWORD']
    credentials['auth_url'] = os.environ['OS_AUTH_URL']
    credentials['tenant_name'] = os.environ['OS_TENANT_NAME']
    return credentials

def get_nova_credentials_v2(project_name):
    credentials = {}
    credentials['version'] = '2'
    credentials['username'] = os.environ['OS_USERNAME']
    credentials['api_key'] = os.environ['OS_PASSWORD']
    credentials['auth_url'] = os.environ['OS_AUTH_URL']
    credentials['project_id'] = project_name  # For some reason, this parameter is called project_id
                                              # It's the project name
    return credentials

