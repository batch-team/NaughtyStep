import requests

from naughtystep.utils import config

class Log(object):
    """ Utility to help sending messages to the logger module """

    def __init__(self, module):
        """ Initialize naughtystep logger

            :param module: The module to initialize the logger for
        """

        loggerHost = config.Config().get('naughtyLogger', 'host')
        self.logUrls = {'debug': 'http://{0}/v1/step/log/debug/{1}'.format(loggerHost, module),
                        'info': 'http://{0}/v1/step/log/info/{1}'.format(loggerHost, module),
                        'error': 'http://{0}/v1/step/log/error/{1}'.format(loggerHost, module)}

    def log(self, logLevel, message):
        """ Logs message in naughtystep's logger
            If the logging fails, the message is printed to the stdout

        :param logLevel: The requested log level - debug, info or error
        :param message: The logged message

        :raise KeyError: If the requested log level is not available
        """
        resp = requests.post(self.logUrls[logLevel], message)

        if resp.status_code != 201:
            print 'Could not log {0}: {1}'.format(logLevel.upper(), message)
