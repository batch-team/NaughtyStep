import json


class Config(object):
    def __init__(self):
        json_data = open("/etc/naughtystep/naughtystep.json").read()
        self.data = json.loads(json_data)

    def get(self, component, key):
        return self.data[component][key]
