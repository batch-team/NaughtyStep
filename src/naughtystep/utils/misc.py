""" This module provides miscellaneous utility functions used by multiple
    modules, mainly for common interactions/queries
"""

import socket
import paramiko
import gssapi
import requests

from naughtystep.utils import errors
from naughtystep.utils import config

cfg = config.Config()

def doSsh(hostname, command):
    """ Execute a command on a host using kerberos autentication

    :param hostname: hostname
    :param command: The command to execute

    :return: A tuple (exit status, stdout)

    :raise naughtystep.utils.errors.SshError: If the ssh connection or the
                                              command execution failed
    """
    print 'Doing ' + command + ' on ' + hostname

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname, username='root', timeout=60,
                    gss_auth=True, gss_kex=True)
    except (socket.timeout, socket.error, socket.gaierror,
            paramiko.ssh_exception.AuthenticationException,
            paramiko.ssh_exception.SSHException,
            gssapi.GSSException):
        raise errors.SshError("Could not create the ssh connection to " + hostname)
    except EOFError:
        raise errors.SshError("EOFError while creating connection " + hostname)
    else:
        try:
            _, stdoutChannel, _ = ssh.exec_command(command, timeout=10)
            exitStatus = stdoutChannel.channel.recv_exit_status()
            stdout = stdoutChannel.read()
        except paramiko.ssh_exception.SSHException:
            raise errors.SshError("Execution of " + command + " on " + hostname + " failed")
        finally:
            ssh.close()

    print 'Done ' + command + ' on ' + hostname
    return exitStatus, stdout


def isVirtual(hostname):
    """ Check if a host is virtual by doing a query to the hostlocator

    :param hostname: The host to check

    :return: True if the host is virtual, false otherwise
    """
    hostlocator = cfg.get('hostLocator', 'host')
    url = 'http://{0}/v1/step/hostlocator/{1}'.format(hostlocator, hostname)
    return requests.get(url).status_code == 200


def addToAction(hostname, hostgroup, action, reason, payload=None):
    """ Add a host to an action

    :param hostname: The name of the host to perform the action on
    :param hostgroup: The host's hostgroup
    :param action: The action to perform
    :param payload: Dictionary with other parameters of the action

    :return: none

    :raise naughtystep.utils.errors.ActionAddFailed: If the host could not
                                                     be added to the action
    :raise naughtystep.utils.errors.ActionApproveFailed: If the action could
                                                         not be approved
    """
    actionHost = cfg.get(action, 'host')
    addActionUrl = 'http://{0}/v1/step/{1}/actions/{2}'.format(actionHost,
                                                               hostname, action)

    hostMetadata = {'reason': reason,
                    'available': False,
                    'hostgroup': hostgroup}

    if payload is not None:
        hostMetadata.update(payload)

    if action == 'reboot':
        hostMetadata['virtual'] = isVirtual(hostname)

    resp = requests.post(addActionUrl, json=hostMetadata)
    if resp.status_code != 201:
        raise errors.ActionAddFailed('Could not add ' + hostname + ' to ' + action + '(status ' + str(resp.status_code) + ')')


def getHosts(action=None):
    """ Get all hosts from a service

    :param action: If None, the blackboard is queried.
                   Otherwise, the specified action is queried
    :return: A list of hosts pending action

    :raise naughtystep.utils.errors.ActionQueryFailed: If the action could
                                                       not be queried
    """
    if action is None:
        url = 'http://{0}/v1/step'.format(cfg.get('blackboard', 'host'))
    else:
        url = 'http://{0}/v1/step/actions/{1}'.format(cfg.get(action, 'host'), action)

    resp = requests.get(url)

    if resp.status_code != 200:
        msg = 'Could not query '
        msg += action if action is None else 'blackboard'
        raise errors.QueryFailed(msg)

    return resp.json()


def approveAction(hostname, action):
    """ Approve an action for a pending host

    :param action: The action to approve
    :param hostname: The hostname to approve the action

    :return: none
    :raise naughtystep.utils.errors.ActionApproveFailed: If the action could
                                                          not be approved
    """
    url = 'http://' + cfg.get(action, 'host') + '/v1/step/' + hostname + '/actions/' + action + '/approve'
    resp = requests.post(url, timeout=1200) # Big timeout, required for ssh actions
    if resp.status_code != 200:
        raise errors.ActionApproveFailed('Could not approve ' + action + ' on ' + hostname + ' (status ' + str(resp.status_code) + ')')


def declineAction(hostname, action):
    """ Approve an action for a pending host

    :param action: The action to decline
    :param hostname: The hostname to decline the action

    :return: none
    :raise naughtystep.utils.errors.ActionDeclineFailed: If the action could
                                                          not be declined
    """
    url = 'http://' + cfg.get(action, 'host') + '/v1/step/' + hostname + '/actions/' + action + '/decline'
    resp = requests.post(url, timeout=10)
    if resp.status_code != 200:
        raise errors.ActionDeclineFailed('Could not decline ' + action + ' on ' + hostname)


def getHostDetails(host, action=None):
    """ Query a service for the details of a host

    :param hostname: The host to query for
    :param action: If specified, an action is queried
                   If None, the blackboard is queried

    :return: Dictionary containing the details of the host

    :raise naughtystep.utils.errors.QueryFailed: If the query fails
    """
    service = 'blackboard' if action is None else action

    url = 'http://{0}/v1/step/{1}'.format(cfg.get(service, 'host'), host)
    if action is not None:
        url += '/actions/' + action

    resp = requests.get(url)
    if resp.status_code != 200:
        raise errors.QueryFailed('Could not query {0} ({1})'.format(service, resp.status_code))
    return resp.json()


def fqdnify(host):
    # TODO: you can do better
    if not host.endswith('.cern.ch'):
        host += '.cern.ch'
    return host

