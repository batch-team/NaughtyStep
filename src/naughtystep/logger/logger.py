import json
import requests

import logging
from logging.handlers import TimedRotatingFileHandler

from flask import Flask
from flask import request
from flask import Response

from naughtystep.utils import config

application = Flask(__name__)
config = config.Config()

# Disable flask's internal logger
application.logger.disabled = True
logging.getLogger('werkzeug').disabled = True
logging.getLogger('requests').setLevel(logging.WARNING)

logHandler = TimedRotatingFileHandler(config.get("naughtyLogger", "file"),
                                      when="midnight", backupCount=5)
formatter = logging.Formatter(fmt='%(levelname)s %(name)s %(asctime)s: %(message)s',
                              datefmt='%m/%d/%Y %I:%M:%S%p')
logHandler.setFormatter(formatter)
logger = logging.getLogger('naughtystep')
logger.addHandler(logHandler)
logger.setLevel(logging.DEBUG)


@application.route('/v1/step/log/<level>/<module>', methods=['POST'])
def newEntry(level, module):
    """ Add new log entry for module with the requested log level """
    logger = logging.getLogger("naughtystep." + module)
    getattr(logger, level)(request.data)
    return Response("Thanks For That", status=201)


def getNoOfHostsInNaughtystep():
    """ Get the number of hosts currently in the blackboard """

    url = 'http://{0}/v1/step'.format(config.get('blackboard', 'host'))
    try:
        hosts = requests.get(url)

        return len(hosts.json()) if hosts.status_code == 200 else -1
    except requests.ConnectionError:
        return -1

def getPendingActions():
    """ Get a dictionary with the number of hosts pending each action """

    actions = config.get('general', 'actions')
    pendingAction = {}
    for action in actions:
        url = 'http://{0}/v1/step/actions/{1}'.format(config.get(action, 'host'), action)
        try:
            hosts = requests.get(url)
            pendingAction[action] = len(hosts.json()) if hosts.status_code == 200 else -1
        except requests.ConnectionError:
            pendingAction[action] = -1

    return pendingAction


@application.route('/v1/step/log/stats', methods=['GET'])
def getStats():
    """ Return a dictionary with stats for naughtystep """

    presentHosts = getNoOfHostsInNaughtystep()
    pendingHosts = getPendingActions()

    stats = {'addedHosts': 123,
             'presentHosts': presentHosts,
             'pendingHosts': pendingHosts}

    return Response(json.dumps(stats), status=200, mimetype='application/json')

