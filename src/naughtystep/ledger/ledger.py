""" Ledger module

    Long term storage of the actions performed on hosts, for detecting
    recurrent issues.
"""
import logging
import json
from datetime import datetime

from flask import Flask
from flask import Response
from flask import request
from flask_sqlalchemy import SQLAlchemy

from naughtystep.utils import config

application = Flask(__name__)

config = config.Config()


logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


with open(config.get('ledger', 'dbpass')) as f:
    password = f.read()

url = '{}://{}:{}@{}:{}/{}'.format(config.get('ledger', 'dbtype'),
                                   config.get('ledger', 'dbuser'),
                                   password,
                                   config.get('ledger', 'dbhost'),
                                   config.get('ledger', 'dbport'),
                                   config.get('ledger', 'dbname'))

application.config['SQLALCHEMY_DATABASE_URI'] = url
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(application)

class Ledger(db.Model):
    """ The database model for the ledger table """
    id = db.Column(db.Integer, primary_key=True)
    hostname = db.Column(db.String(40))
    action = db.Column(db.String(40))
    reason = db.Column(db.String(300))
    timestamp = db.Column(db.TIMESTAMP(timezone=False))

    def __init__(self, hostname, action, timestamp, reason=''):
        self.hostname = hostname
        self.action = action
        self.reason = reason
        self.timestamp = timestamp


@application.route('/v1/step/<hostname>/ledger', methods=['POST'])
def addHost(hostname):
    """ Add a new host along with details to the ledger """

    entry = Ledger(hostname=hostname, action=request.json['action'],
                   reason=request.json['reason'], timestamp=datetime.now())

    db.session.add(entry)
    db.session.commit()

    return Response("Thanks For That", status=201)


@application.route('/v1/step/<hostname>/ledger', methods=['GET'])
def hostDetails(hostname):
    """ Get the details of a host from the ledger """

    details = [{'action': entry.action, 'reason': entry.reason,
                'timestamp': int(entry.timestamp.strftime('%s'))}
               for entry in Ledger.query.filter_by(hostname=hostname).all()]

    if hostDetails == []:
        return Response('Not Found', status=404)

    return Response(json.dumps(details), status=200, mimetype='application/json')


@application.route('/v1/step/ledger', methods=['GET'])
def listHosts():
    """ List all hosts present in the ledger """

    hostSet = set(entry.hostname
                  for entry in db.session.query(Ledger.hostname).all())
    return Response(json.dumps(list(hostSet)),
                    status=200, mimetype='application/json')

