#!/usr/bin/env python

# mccance
# Needs:
#  pip install stompest.asyc
#  pip install twisted
#  pip install stompest

import json
import time
import traceback
import requests
import dateutil.parser

from pytz import timezone

from twisted.internet import reactor, defer, task

from stompest.async import Stomp
from stompest.async.listener import Listener, SubscriptionListener
from stompest.config import StompConfig
from stompest.protocol import StompSpec
from stompest.error import StompConnectionError

from naughtystep.utils.config import Config
from naughtystep.utils.logging import Log

from requests.exceptions import ConnectionError

# Basic single instance consumer thread
class Consumer(object):

    def __init__(self, hostnport, topics, handler=None, user=None, password=None):
        self.config = StompConfig(uri='failover:(tcp://%s)?startupMaxReconnectAttempts=3' % hostnport, login=user, passcode=password)

        if not isinstance(topics, list):
            topics = [topics]

        self.topics = topics
        self.host = hostnport
        self.handler = handler
        self.client = None
        self.subtokens = []
        self.logger = Log('consumer')

    @defer.inlineCallbacks
    def run(self, sequence):
        self.sequence = sequence

        # Connect here, yielding until it's done
        self.client = Stomp(self.config)
        yield self.client.connect()
        headers = {
            StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL,
            'activemq.prefetchSize': '5000',
        }

        listener = SubscriptionListener(self.handler, onMessageFailed=self.handle_messagehander_error)

        for topic in self.topics:
            subtoken = yield self.client.subscribe(topic, headers, listener=listener)
            self.subtokens.append(subtoken)

        # We're done - pass to next callback in the chain to connect next broker
        defer.returnValue(sequence+1)


    # Called if the onMessage handler fails: print and swallow exception
    def handle_messagehander_error(self, connection, failure, frame, errorDestination):
        exceptionMsg = 'Caught and ignoring exception in onMessage handler: %s ' % failure.args
        self.logger.log('error', exceptionMsg)
        traceback.print_exc()

    # Called by signal handler to unsubscribe and disconnect from brokers
    @defer.inlineCallbacks
    def disconnect(self):
        print 'Disconnecting from host: %s' % self.host
        try:
            if self.client:
                self.client.disconnect()
                yield self.client.disconnected
        except StompConnectionError as e:
            print 'Connection error', e
            pass  # hey, we tried our best

    # Called if we really can't connect to one of the brokers
    # Swallows error - the other conenction will continue
    def handle_connect_error(self, failure):
        print '[%i] Error on connection to host %s: %s' % (self.sequence, self.host, failure.getErrorMessage())
        return self.sequence + 1


class Cache(object):
    ''' Utility class to cache duplicates '''

    def __init__(self):
        self.cache = {}
        self.config = Config()

    def hit(self, key):
        if key not in self.cache:
            return False
        elif time.time() - self.cache[key] > self.config.get('gni_consumer', 'cache_validity'):
            del self.cache[key]
            return False
        return True

    def add(self, key):
        self.cache[key] = time.time()



# Handler for GNI messages
class GNIMessage(object):

    def __init__(self):
        self.config = Config()
        self.endpoint = 'http://' + self.config.get('blackboard', 'host')
        self.forwarded_exceptions = self.config.get('gni_consumer', 'forwarded_exceptions')
        self.forwarded_hostgroups = self.config.get('gni_consumer', 'forwarded_hostgroups')

        self.message_count = 0
        self.logger = Log('consumer')
        self.cache = Cache()

    def _hostgroup_forwarded(self, hostgroup):
        return any(hostgroup.startswith(hg) for hg in self.forwarded_hostgroups)

    # Called for each message received
    def onMessage(self, client, frame):

        metadata = json.loads(frame.body)['metadata']

        if 'hostgroup' in metadata and self._hostgroup_forwarded(metadata['hostgroup']):
            exception = metadata['description']
            if exception.startswith('exception.'):
                exception = exception[10:]
            elif exception.startswith('exceptions.'):
                exception = exception[11:]

            host = metadata['entity'] + '.cern.ch'

            cacheKey = '{0}-{1}'.format(host, exception) # Convert the tuple (host, exception) into a string for faster cashing
            if exception in self.forwarded_exceptions and metadata['state'] in ('open', 'active') and not self.cache.hit(cacheKey):
                self.cache.add(cacheKey)
                payload = json.dumps({'exception': exception,
                                      'uuid': metadata['uuid'],
                                      'timestamp': metadata['timestamp'],
                                      'host': metadata['entity'],
                                      'hostgroup': metadata['hostgroup']})
                url = self.endpoint + '/v1/step/{0}.cern.ch/alarm/{1}'.format(metadata['entity'], exception)

                try:
                    requests.post(url, data=payload, headers={'Content-type': 'application/json'})
                    self.logger.log('info', 'New {0} for {1}'.format(exception, host))
                except ConnectionError:
                    print 'ERROR: Connection error'


# Handler for megabus messages
class CloudMessage(object):

    def __init__(self):
        self.logger = Log('consumer')

    def onMessage(self, client, frame):
        body = json.loads(frame.body)

        print 'CLOUD MESSAGE ', body

        if body['type'] == 'compute_node_died':
            action = 'kill'
            payload = {'reason': 'Cloud reported compute_node_died',
                       'hostgroup': 'N/A',
                       'available': False,
                       'priority': 101}
        elif body['type'] == 'destructive_server_intervention' or body['type'] == 'minor_server_intervention':
            action = 'drain'
            localtz = timezone('Europe/Zurich')
            interventionTime = int(localtz.localize(dateutil.parser.parse(body['intervention_time'])).strftime('%s'))

            payload = {'reason': 'Cloud reported destructive_server_intervention',
                       'hostgroup': 'N/A',
                       'available': False,
                       'priority': 101,
                       'type': 'destroy',
                       'time': interventionTime - int(time.time())}

        else:
            self.logger.log('error', 'Unknown message type ' + str(body['type']))


        for host in body['vms']:
            host += '.cern.ch'
            url = 'http://' + Config().get(action, 'host') + '/v1/step/' + host + '/actions/' + action
            self.logger.log('info', 'Cloud requested deletion of ' + host)
            requests.post(url + '/decline') # Remove if pending
            r = requests.post(url, json=payload)
            if r.status_code != 201:
                payload = {'reason': 'Cloud reported ' + body['type'] + ' and could not add to ' + action,
                           'hostgroup': 'N/A',
                           'available': False,
                           'priority': 101}
                requests.post('http://' + Config().get('manual', 'host') + '/v1/step/' + host + '/actions/manual')
                self.logger.log('error', 'Could not delete ' + host + ' as required by cloud')

