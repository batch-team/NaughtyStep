"""
This module is used to store the hosts that are currently under evaluation
(i.e. no decision has been made for them yet), along with details such
as the alarms, test results etc.
"""

import json
import time
import requests
import redis

from flask import Flask
from flask import request
from flask import Response

from naughtystep.utils import config
from naughtystep.utils import logging
from naughtystep.utils import misc


application = Flask(__name__)


config = config.Config()
logger = logging.Log('blackboard')


class Blackboard(object):
    def __init__(self):
        self.r = redis.Redis()

    def add(self, hostname, data):
        self.r.sadd('hosts', hostname)
        return self.r.set(hostname, data)

    def get(self, hostname):
        return self.r.get(hostname)

    def delete(self, hostname):
        self.r.srem('hosts', hostname)
        return self.r.delete(hostname)

    def getAll(self):
        return list(self.r.smembers('hosts'))


class Locks(object):
    def __init__(self):
        self.r = redis.Redis()

    def lock(self, exception):
        return self.r.sadd('locks', exception) == 1

    def locked(self, exception):
        return self.r.sismember('locks', exception)

    def unlock(self, exception):
        return self.r.srem('locks', exception) == 1

    def getAll(self):
        return list(self.r.smembers('locks'))



blackboard = Blackboard()
locks = Locks()



@application.route('/v1/step', methods=['GET'])
def listAll():
    """ List all hosts present in the blackboard """

    hosts = blackboard.getAll()
    return Response(json.dumps(hosts), status=200, mimetype='application/json')


@application.route('/v1/step/alarm/<alarmName>')
def listAllWithAlarm(alarmName):
    """ List all hosts present on the blackboard that have a given alarm present """

    hosts = []
    for host in blackboard.getAll():
        hostDetails = blackboard.get(host)
        if not hostDetails:
            continue

        alarms = json.loads(hostDetails)['alarms']

        if any(alarm['exception'] == alarmName for alarm in alarms):
            hosts.append(host)

    js = json.dumps(hosts)
    return Response(js, status=200, mimetype='application/json')


@application.route('/v1/step/<hostname>/alarm/<alarmName>', methods=['POST'])
def newException(hostname, alarmName):
    """ Add a new alarm.
        If the host is present, just append it. Otherwise, create a new entry
    """

    if locks.locked(alarmName):
        return Response('Exception locked', status=409)

    request.json['exception'] = alarmName

    hostDetails = blackboard.get(hostname)

    if hostDetails:
        hostDetails = json.loads(hostDetails)

        hostDetails['alarms'].append(request.json)
        hostDetails['hostgroup'] = request.json.get('hostgroup')

    else:
        hostDetails = {'alarms': [request.json],
                       'hostgroup': request.json.get('hostgroup'),
                       'tryNo': 0,
                       'notUntil': int(time.time()),
                       'tests': {},
                       'isVirtual': misc.isVirtual(hostname)}

    blackboard.add(hostname, json.dumps(hostDetails))

    return Response('Thanks For That', status=201)


@application.route('/v1/step/<hostname>', methods=['GET'])
def getHostDetails(hostname):
    """ Get all the details of a host """

    hostDetails = blackboard.get(hostname)
    if not hostDetails:
        return Response('Not Found', status=404)

    return Response(hostDetails, status=200, mimetype='application/json')


@application.route('/v1/step/<hostname>', methods=['DELETE'])
def deleteHost(hostname):
    """ Delete a host from the blackboard """

    resp = blackboard.delete(hostname)
    if not resp:
        return Response('Not Found', status=404)

    logger.log('info', 'Host {0} deleted'.format(hostname))

    return Response('OK', status=200)



@application.route('/v1/step/<hostname>/test/<tryNo>/<test>', methods=['POST'])
def addTestResults(hostname, tryNo, test):
    """ Add the results of a test to a host """

    hostDetails = blackboard.get(hostname)
    if not hostDetails:
        return Response('Not Found', status=404)

    hostDetails = json.loads(hostDetails)

    logData = 'Results for test {0} (try {1}) on {2}: {3}'.format(test, tryNo, hostname, request.data[:20])
    logger.log('info', logData)

    hostDetails['tests'][test][int(tryNo)] = request.data
    blackboard.add(hostname, json.dumps(hostDetails))

    return Response('OK', status=201)



@application.route('/v1/step/<hostname>/retry', methods=['POST'])
def retry(hostname):
    """ Mark the given host for a test rerun """

    hostDetails = blackboard.get(hostname)
    if not hostDetails:
        return Response('Not Found', status=404)

    hostDetails = json.loads(hostDetails)

    logger.log('info', 'Rerun tests on {0}'.format(hostname))

    hostDetails['tryNo'] += 1
    hostDetails['notUntil'] = int(time.time()) + int(request.data)

    for _, test in hostDetails['tests'].items():
        test.append('')

    blackboard.add(hostname, json.dumps(hostDetails))

    return Response('OK', status=200)


@application.route('/v1/step/<hostname>/enable/<test>', methods=['POST'])
def enableTest(hostname, test):
    """ Enable a test for a given hostname """

    hostDetails = blackboard.get(hostname)
    if not hostDetails:
        return Response('Not Found', status=404)

    hostDetails = json.loads(hostDetails)

    tryNo = hostDetails['tryNo']
    hostDetails['tests'][test] = [''] * (tryNo + 1)

    blackboard.add(hostname, json.dumps(hostDetails))

    return Response('OK', status=200)


@application.route('/v1/step/testers/<test>', methods=['GET'])
def getUntested(test):
    """
    Get all the hosts that do not have the results of a given test for the
    current test run
    """

    untestedHosts = []
    for host in blackboard.getAll():
        details = blackboard.get(host)

        if not details:
            continue

        details = json.loads(details)
        if test in details['tests'] and details['tests'][test][details['tryNo']] == '' and int(details['notUntil']) < int(time.time()):
            untestedHosts.append({'hostname': host, 'tryNo': details['tryNo']})

    return Response(json.dumps(untestedHosts),
                    status=200, mimetype='application/json')


@application.route('/v1/step/lock/<exception>', methods=['POST'])
def lock(exception):
    """ Add lock for an exception """

    if not locks.lock(exception):
        return Response('Exception already locked', status=409)

    return Response('OK', status=201)


@application.route('/v1/step/unlock/<exception>', methods=['POST'])
def unlock(exception):
    """ Remove lock for an exception """

    if not locks.unlock(exception):
        return Response('Exception not locked', status=409)

    return Response('OK', status=200)


@application.route('/v1/step/lock', methods=['GET'])
def listLocked():
    """ List all locked exceptions """

    return Response(json.dumps(locks.getAll()),
                    status=200, mimetype='application/json')


if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True,
                    threaded=True)
