""" The naughtystep draining engine.

    This is a standalone script that queries roger for draining hosts,
    creates /etc/shutdowntime file and, when draining is finished,
    removes them from draining and performs an action.
"""
import json
import time
import re
import requests
import argparse
import htcondor
from Queue import Queue
from multiprocessing.pool import ThreadPool

from batchfactory.pdbclient import PDBClient
from batchfactory.roger_client import RogerClient
from naughtystep.utils import config
from naughtystep.utils import misc
from naughtystep.utils import errors
from naughtystep.utils import hostLocator
from naughtystep.utils import logging

from tqdm import tqdm
from pprint import pprint


class Hostgroups(object):
    """ Hostgroup cache used to avoid unnecessary PDB queries

        All added hosts are guaranteed to have a hostgroup associated.
        If the hostgroup is not provided on addition, a PDB query is made.
    """

    def __init__(self):
        self.pdb = PDBClient()
        self.hostgroups = {}
        self.failures = []


    def _getHostgroup(self, host):
        """ Do a PDB query to fetch the hosgroup of a host

        :param: host
        :return: string representing the requested hostgroup
        :raise errors.PdbQueryFailed:
        """
        fail, result = self.pdb.get_host_facts(host, 'hostgroup')
        if fail or len(result) == 0:
            raise errors.PdbQueryFailed()
        return result[0]['value']


    def add(self, host, hostgroup=None):
        """ Add a host to the cache

        :param host: The host to add
        :param hostgroup: The hostgroup of the added host;
                          if None, a PDB query is performed
        :return: None
        :raises errors.PdbQueryFailed: If the hostgroup is None
                                       and the PDB query fails
        """
        if hostgroup is None:
            hostgroup = self._getHostgroup(host)

        self.hostgroups[host] = hostgroup


    def addList(self, hostList, hostgroup=None):
        """ Utility function for adding a list of hosts

        :param hostList: The list of hosts to be added
        :param hostgroup: If known, must be the same for all hosts;
                          otherwise individual PDB queries are perfomed
        :return: A list of hosts that could not be added
                 (most likely due to PDB errors)
        """
        failed = []
        for host in hostList:
            try:
                self.add(host, hostgroup)
            except errors.PdbQueryFailed:
                failed.append(host)
        return failed


    def get(self, host):
        """ Get the hostgroup of a host

        :raise KeyError: If the host is not present in cache
        """
        return self.hostgroups[host]


    def getAllHosts(self, hostgroup):
        """ Get all hostgroups from a hostgroup from pdb

        :param hostgroup: The hostgroup to query for
        :return: A list of hosts
        """

        success, nodes = self.pdb.get_hostgroup_nodes(hostgroup)
        if not success:
            return []
        hosts = [host['certname'] for host in nodes]
        self.addList(hosts, hostgroup)
        return hosts


class LSFCache(object):
    """ Class to cache the job status of the LSF machines, used to avoid the
        need of repeated queries to the LSF masters
    """

    def __init__(self, lsfmasters):
        """ Cache the number of jobs running for all LSF hosts

        :param lsfmasters: List of strings representing the hostnames of LSF masters

        :raise naughtystep.utils.errors.SshError: If the ssh connection or the
                                                  command execution failed
        :raise naughtystep.utils.errors.CommandFailed: If the commands run
                                                       on the lsfmasters fail
        """
        self.cache = {}

        for master in lsfmasters:
            command = r"bhosts | sed -n '1!p' | sed  's/ \+/ /g' | cut -d' ' -f1,6"
            status, stdout = misc.doSsh(master, command)

            if status != 0:
                raise errors.CommandFailed('bhosts failed with status ' + str(status))

            entries = [status.split(' ') for status in stdout.split('\n')[:-1]]
            self.cache = {misc.fqdnify(host): jobs for host, jobs in entries}


    def get(self, host):
        """ Get the number of LSF jobs running on host

        :raise KeyError: If the host does not exist or does not run LSF
        """
        return self.cache[host]



class DrainingEngine(object):
    """ Encapsulates all the procedures necessary to start and monitor draining

        In this class, a service is a hostgroup (e.g. bi/batch)
    """


    def __init__(self):
        self.config = config.Config()
        self.logger = logging.Log("draining_engine")
        self.pdb = PDBClient()
        self.roger = RogerClient({'roger': self.config.get('services', 'roger')})
        self.drainingHosts = {}
        self.noRogerHosts = []
        # TODO: this might fail miserably if there is a problem with the lsfmasters
        self.lsfcache = LSFCache(self.config.get('services', 'lsfmasters'))
        self.hostgroupCache = Hostgroups()
        self.collector = str(self.config.get('drainingengine', 'collector'))

        with open('/etc/naughtystep/services.json') as serviceConfig:
            services = json.load(serviceConfig)

        self.services = {}

        self.services['hostgroups'] = [hg for hg, details in services.iteritems() if details['type'] == 'hostgroup']
        self.services['tenants'] = [hg for hg, details in services.iteritems() if details['type'] == 'tenant']


    def getDrainingHosts(self):
        """ Query roger for the hosts that are draining in all configured services

        :return: none
        """

        failed = []
        hosts = set()
        for hostgroup in self.services['hostgroups']:
            hostList = self.hostgroupCache.getAllHosts(hostgroup)
            hosts |= set(hostList)

        for tenant in self.services['tenants']:
            hostList = hostLocator.getAllHostsTenant(tenant)
            failed += self.hostgroupCache.addList(hostList, None)
            hosts |= set(hostList)

        pprint(failed)

        for host in tqdm(hosts):
            success, appstate = self.roger.get_appstate(host)

            if not success:
                self.noRogerHosts.append(host)
                continue

            if appstate not in self.config.get('drain', 'types'):
                continue

            try:
                hostgroup = self.hostgroupCache.get(host)
            except KeyError:
                print 'No hostgroup for ', host

            self.drainingHosts[host] = {'appstate': appstate,
                                        'hostgroup': hostgroup}

##############################################################################
# This is doing roger indexed queries. It's much faster the one above.
# Use it when roger is ready
##############################################################################
#        failed = []
#        hosts = []
#
#        for appstate in self.config.get('drain', 'types'):
#            success, hostsInAppstate = self.roger.in_appstate(appstate)
#            if success is True:
#                hosts += [host.split('.')[0] for host in hostsInAppstate]
#
#        print hosts
#        self.hostgroupCache.addList(hosts)
#        hosts = set(hosts)
#
#        for host in tqdm(hosts):
#            try:
#                print host
#                hostgroup = self.hostgroupCache.get(host)
#                print hostgroup
#                tenant = hostLocator.getTenant(host)
#            except KeyError:
#                continue
#
#            print host, hostgroup, tenant
#
#
#            if hostgroup in self.services['hostgroups'] or tenant in self.services['tenant']:
#                self.drainingHosts[host] = {'appstate': appstate, 'hostgroup': hostgroup}
#
#        print self.drainingHosts
#
##############################################################################

    def getLongDrainTime(self, service):
        """ Get the preconfigured drain time for a long drain

        :param service: The service to get the drain time for
                        (hostgroup or tenant)
        :return: The configured drain time, if there is one for the given
                 service. Otherwise, the default value
        """
        hostgroupTime = config.Config().get("drain", "serviceTime")

        for hg, drainTime in hostgroupTime.iteritems():
            if service.startswith(hg) or hg.startswith(service) or hg == service:
                return drainTime

        return config.Config().get("drain", "defaultTime")


    def isDraining(self, hostname):
        """ Check if a host is draining based on the presence of /etc/shutdowntime

        :param hostname: The host to check
        :return: True if the host is draining, False otherwise

        :raise naughtystep.utils.errors.SshError: If the ssh connection or the
                                                  command execution failed
        """
        status, _ = misc.doSsh(hostname, 'test -f /etc/shutdowntime')
        return status == 0


    def startDraining(self, hostname, drainingType):
        """ Begin draining of a host

        Creates the /etc/shutdowntime file with the appropriate timestamp

        For draining and destroy, the timestamp is based on the configured value
        For all other types, the timestamp is the current timestamp

        :param hostname: The host to drain
        :param drainingType: The type of draining

        :return: none

        :raise naughtystep.utils.errors.SshError: If the ssh connection or the
                                                  command execution failed
        """

        success, message = self.roger.get_message(hostname)
        if success is not True:
            print "can't get message for " + hostname
            return
        if message is None:
            print "no message for " + hostname
            print self.roger.get_message(hostname)
            return
        regex = re.search(r'\d{10}', message)
        if regex:
            drainingTime = regex.group(0)
        else:
            drainingTime = int(time.time())
            if drainingType in ('draining', 'destroy'):
                hostgroup = self.drainingHosts[hostname]['hostgroup']
                drainingTime += self.getLongDrainTime(hostgroup)

        command = 'echo ' + str(drainingTime) + ' > /etc/shutdowntime'
        misc.doSsh(hostname, command)


    def hasLSFJobs(self, hostname):
        """ Check if a host has LSF jobs by querying the local cache

        :param hostname: The hostname to check

        :return: True if the host has LSF jobs, False otherwise

        :raise: none
        """
        try:
            return self.lsfcache.get(hostname) != '0'
        except KeyError:
            return True


    def hasCondorJobs(self, hostname):
        """ Check if a host has Condor jobs by querying the Condor collector

        :param hostname: The hostname to check

        :return: True if the host has Condor jobs or there has been an error, False otherwise
        """
        hostname = misc.fqdnify(hostname)
        queryString = 'Machine=?="' + str(hostname) + '"'

        collector = htcondor.Collector(self.collector)

        try:
            reply = collector.query(htcondor.AdTypes.Any, queryString, ['ChildState'])
            jobCount = reply[0]['ChildState'].count('Claimed')
        except IOError:
            print 'CRITICAL', hostname, ': failed commication with collector'
            return True
        except (IndexError, KeyError):
            print 'CRITICAL', hostname, ': an index error in the job count:', reply
            return True

        return jobCount != 0


    def hasRunningJobs(self, hostname):
        """ Check if the given hostname is running batch jobs

        Determines the type of the batch system from the hostgroup

        :param hostname: The host to check
        :return: True if there are running batch jobs, False otherwise
        :raise naughtystep.utils.errors.UnknownType: If the batch system cannot
                                                     be determined
        """
        hostgroup = self.drainingHosts[hostname]['hostgroup']
        if hostgroup.startswith('bi/condor'):
            return self.hasCondorJobs(hostname)
        elif hostgroup.startswith('bi/batch'):
            return self.hasLSFJobs(hostname)
        else:
            raise errors.UnknownType('Cannot determine the batch system for ' + hostname)


    def stopDraining(self, host):
        """ Remove a host from draining

        Deletes the /etc/shutdowntime file from the host, changes the roger
        appstate to production and performs the requested action on host

        :param host: The host to remove from draining

        :return: none

        :raise naughtystep.utils.errors.SshError: If /etc/shutdowntime could
                                                  not be removed
        :raise errors.ActionAddFailed: If the host could not be marked for
                                       manual intervention
        """
        appstate = self.drainingHosts[host]['appstate']
        hostgroup = self.drainingHosts[host]['hostgroup']
        sshOptions = {'script': 'finish_draining', 'auth': 'krb'}

        try:
            if appstate == 'draining' or appstate == 'hard_reboot':
                misc.addToAction(host, hostgroup, 'ssh', 'drained', sshOptions)
                misc.approveAction(host, 'ssh')

                # If the file removal fails, it's fine:
                # the host is still draining, will retry at the next run
                misc.doSsh(host, 'rm -f /etc/shutdowntime')

                misc.addToAction(host, hostgroup, 'reboot', 'drained')
                misc.approveAction(host, 'reboot')
            elif appstate == 'destroy':
                misc.addToAction(host, hostgroup, 'kill', 'drained')
                misc.approveAction(host, 'kill')
        except (requests.ConnectionError, requests.Timeout, errors.ActionEndpointError) as e:
            try:
                misc.addToAction(host, hostgroup, 'manual',
                                 'Stopping drain failed with {0}: {1}'.format(type(e).__name__, e))
            except (errors.ActionEndpointError) as e:
                pass

            return

        if appstate != 'intervention':
            self.roger.set_appstate('production', host)
            self.roger.update_message('', host)
        else:
            self.roger.set_appstate('drained', host)


    def isDrainingForTooLong(self, host):
        """ Returns true if the shutdowntime has been exceeded by 1 week and
            the host is still draining
        """

        status, stdout = misc.doSsh(host, 'cat /etc/shutdowntime')

        if status != 0:
            return False

        shutdowntime = int(stdout[:-1])

        return shutdowntime + 604800 < int(time.time())



    def checkNode(self, host):
        """ Get the state of a host

        :param host:
        :return: A string representing the state (start, stop, draining or failed)
        """
        try:
            if not self.isDraining(host):
                return 'start'
            elif not self.hasRunningJobs(host) or self.isDrainingForTooLong(host):
                return 'stop'
            else:
                return 'draining'
        except errors.SshError:
            return 'failed'


    def checker(self, inQueue, outQueues):
        """ Function to run in a thread to check the state of hosts

        :param inQueue: Add strings representing hostnames to this queue
        :param outQueues: Dictionary {'state': queue}
        """
        while True:
            hostname = inQueue.get()
            state = self.checkNode(hostname)

            outQueues[state].put(hostname)

            inQueue.task_done()


    def starter(self, inq, started, failed):
        while True:
            hostname = inq.get()
            try:
                self.logger.log('info', 'Starting draining on ' + hostname)
                self.startDraining(hostname, self.drainingHosts[hostname]['appstate'])
            except errors.SshError:
                self.logger.log('error', 'Could not start draining on ' + hostname)
                failed.put(hostname)

            started.put(hostname)
            inq.task_done()


    def stopper(self, inQueue, outQueue):
        while True:
            hostname = inQueue.get()
            try:
                self.logger.log('info', 'Stopping draining on ' + hostname)
                self.stopDraining(hostname)
            except errors.SshError:
                self.logger.log('error', 'Could not stop draining on ' + hostname)
                outQueue['failed'].put(hostname)
            outQueue['stopped'].put(hostname)
            inQueue.task_done()


    def run(self, drystart=True, dryend=True):
        """ Run the draining engine

        :param drystart: If true, draining is not started on new nodes
        :param dryend: If true, draining is not closed on new nodes

        :return: A dictionary describing the result of the engine run:
                 {'started': [], 'stopped': [], 'failed': []}
        """

        self.getDrainingHosts()

        outQueues = {'start': Queue(),
                     'started': Queue(),
                     'stop': Queue(),
                     'stopped': Queue(),
                     'failed': Queue(),
                     'draining': Queue()}

        inQueue = Queue()
        for host in self.drainingHosts:
            inQueue.put(host)

        ThreadPool(128, self.checker, (inQueue, outQueues))
        inQueue.join()


        if not drystart:
            ThreadPool(128, self.starter, (outQueues['start'], outQueues['started'], outQueues['failed']))
            outQueues['start'].join()
            del outQueues['start']
        else:
            del outQueues['started']


        if not dryend:
            toStop = list(outQueues['stop'].queue)

            stopping = Queue()
            hypervisors = set()

            for host in toStop:
                try:
                    tenant = hostLocator.getTenant(host)
                except errors.NotFound:
                    stopping.put(host)
                    continue

                if tenant in self.services['tenants']:
                    neighbours = hostLocator.getNeighbours(host)

                    neighboursRunningJobs = [neighbour for neighbour in neighbours
                                             if neighbour not in toStop and neighbour in self.drainingHosts.keys()]

                    if neighboursRunningJobs:
                        continue

                    hypervisors.add(hostLocator.getHypervisor(host))

                stopping.put(host)

            ThreadPool(128, self.stopper, (stopping, outQueues))
            stopping.join()

            del outQueues['stop']
        else:
            del outQueues['stopped']


        ret = {}
        for state, queue in outQueues.iteritems():
            ret[state] = list(queue.queue)

        if not dryend:
            ret['hypervisors'] = list(hypervisors)

        return ret


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dryrun", action="store_true")
    parser.add_argument("--drystart", action="store_true")
    parser.add_argument("--dryend", action="store_true")

    args = parser.parse_args()

    drystart = args.drystart or args.dryrun
    dryend = args.dryend or args.dryrun

    d = DrainingEngine()
    runResult = d.run(drystart, dryend)

    print pprint(runResult)

