// Tools to implement blackboard queries and operations on blackboard data
package ledger

import (
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"blackboard"
	"utils/config"
	"utils/timestamp"
)

type LedgerEntry struct {
	Alarms    []blackboard.Alarm   `json:"alarms"`
	Action    string               `json:"action"`
	Timestamp *timestamp.Timestamp `json:"timestamp"`
}

// Get all entries in the ledger for a given host
func GetLedgerEntries(hostname string) ([]LedgerEntry, error) {
	url := "http://" + config.Get("ledger", "host") + "/v1/step/" + hostname + "/ledger"
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return nil, errors.New("Can't find the host " + hostname + " in ledger")
	}

	var ledgerEntries []LedgerEntry
	err = json.NewDecoder(response.Body).Decode(&ledgerEntries)

	return ledgerEntries, err
}

func TimeSinceLastLedgerEntry(host blackboard.Host) (time.Duration, error) {
	ledgerEntries, err := GetLedgerEntries(host.Name)
	if err != nil || len(ledgerEntries) == 0 {
		return time.Since(time.Now()), errors.New("Host has no alarms")
	}

	timeSinceLastLedgerEntry := time.Since(ledgerEntries[0].Timestamp.Time)
	for _, entry := range ledgerEntries {
		if timeSinceLastLedgerEntry > time.Since(entry.Timestamp.Time) {
			timeSinceLastLedgerEntry = time.Since(entry.Timestamp.Time)
		}
	}

	return timeSinceLastLedgerEntry, nil
}
