// Helpers for interaction with the nova API
package novaUtils

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"utils/config"

	"github.com/rackspace/gophercloud"
	"github.com/rackspace/gophercloud/openstack"
)

type HostDetails struct {
	Name       string `json:"name"`
	UUID       string `json:"UUID"`
	TenantName string `json:"TenantName"`
	TenantId   string `json:"TenantId"`
}

// Get a service client for the given tenant
func GetServiceClientForTenant(TenantID string, TenantName string) (*gophercloud.ServiceClient, error) {
	// take basic opts from environent
	opts, _ := openstack.AuthOptionsFromEnv()

	// stomp the new tenant
	newopts := opts
	newopts.TenantID = TenantID
	newopts.TenantName = TenantName

	// fetch the provider for that tenant (give up after 3 attempts)
	var provider *gophercloud.ProviderClient
	var errp error
	for try := 0; try < 3; try++ {
		provider, errp = openstack.AuthenticatedClient(newopts)
		if errp != nil {
			fmt.Printf("TenantName ERROR - try %d: %s - %s\n", try, TenantID, TenantName)
			fmt.Println(errp)
			var tm = rand.Intn(100)
			time.Sleep(time.Duration(tm) * time.Duration(try) * time.Millisecond)
		} else {
			break
		}
	}

	if provider == nil {
		return nil, errors.New("Could not get provider client for " + TenantName)
	}

	// create the service client for the tennant
	compute, err := openstack.NewComputeV2(provider, gophercloud.EndpointOpts{
		Region: "cern",
	})

	return compute, err
}

// Use the host locator service to get the details of a host
func GetHostDetails(hostname string) (HostDetails, error) {
	url := "http://" + config.Get("hostLocator", "host") + "/v1/step/hostlocator/" + hostname

	response, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer response.Body.Close()

	var hostDetails HostDetails
	if response.StatusCode != 200 {
		return hostDetails, errors.New("NOT FOUND")
	}

	json.NewDecoder(response.Body).Decode(&hostDetails)

	return hostDetails, nil
}
