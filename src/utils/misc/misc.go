package misc

import (
	"strings"
)

func Fqdnify(hostname string) string {
	if !strings.HasSuffix(hostname, ".cern.ch") {
		hostname += ".cern.ch"
	}
	return hostname
}
