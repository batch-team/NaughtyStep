package pdb

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"

	"utils/gssapi"
	"utils/misc"
)

type Fact struct {
	Name        string `json:"name"`
	Value       string `json:"value"`
	Environment string `json:"environment"`
	Certname    string `json:"certname"`
}

func DoPdbRequest(query string) (*http.Response, error) {
	pdb := "https://aipdbbal09.cern.ch:9081"

	resp, err := gssapi.DoKerberosRequest(pdb + query)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func GetFact(hostname string, fact string) (string, error) {

	hostname = misc.Fqdnify(hostname)

	query := "/v4/nodes/" + hostname + "/facts/" + fact

	response, err := DoPdbRequest(query)
	if err != nil {
		return "", errors.New("PDB request failed: " + err.Error())
	}

	if response.StatusCode != 200 {
		return "", errors.New("unexpected response " + strconv.Itoa(response.StatusCode))
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", errors.New("request parsing failed: " + err.Error())
	}

	var entries []Fact
	json.Unmarshal(body, &entries)

	if len(entries) == 0 {
		return "", errors.New("fact " + fact + " not present for " + hostname)
	}

	return entries[0].Value, nil
}
