package gssapi

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"

	"utils/naughtyLogger"

	"github.com/apcera/gssapi"
	"github.com/apcera/gssapi/spnego"
)

const (
	micHeader = "X-go-gssapi-test-mic"
)

type Context struct {
	DebugLog       bool
	RunAsService   bool
	ServiceName    string
	ServiceAddress string

	gssapi.Options

	*gssapi.Lib `json:"-"`
	loadonce    sync.Once

	// Service credentials loaded from keytab
	credential *gssapi.CredId
}

var c = &Context{}

func loadlib(debug bool, prefix string) (*gssapi.Lib, error) {
	max := gssapi.Err + 1
	if debug {
		max = gssapi.MaxSeverity
	}
	pp := make([]gssapi.Printer, 0, max)
	for i := gssapi.Severity(0); i < max; i++ {
		p := log.New(os.Stderr,
			fmt.Sprintf("%s: %s\t", prefix, i),
			log.LstdFlags)
		pp = append(pp, p)
	}
	c.Options.Printers = pp

	lib, err := gssapi.Load(&c.Options)
	if err != nil {
		return nil, errors.New("Failed to load the lib: " + err.Error())
	}

	return lib, nil
}

func prepareServiceName() (*gssapi.Name, error) {
	nameBuf, err := c.MakeBufferString(c.ServiceName)
	if err != nil {
		return nil, errors.New("Failed to prepare the service name: " + err.Error())
	}
	defer nameBuf.Release()

	name, err := nameBuf.Name(c.GSS_KRB5_NT_PRINCIPAL_NAME)
	if err != nil {
		return nil, errors.New("Failed to prepare the service name: " + err.Error())
	}
	if name.String() != c.ServiceName {
		str := fmt.Sprintf("Name: got %q, expected %q", name.String(), c.ServiceName)
		naughtyLogger.Warning("utils.gssapi", "While preparing the service name: "+str)
	}

	return name, nil
}

func initClientContext(method, path string, bodyf func(ctx *gssapi.CtxId) string) (ctx *gssapi.CtxId, r *http.Request, err error) {
	serviceName, err := prepareServiceName()
	if err != nil {
		return nil, nil, errors.New("Failed to prepare the service name: " + err.Error())
	}

	ctx, _, token, _, _, err := c.InitSecContext(
		c.GSS_C_NO_CREDENTIAL,
		nil,
		serviceName,
		c.GSS_C_NO_OID,
		0,
		0,
		c.GSS_C_NO_CHANNEL_BINDINGS,
		c.GSS_C_NO_BUFFER)

	defer token.Release()

	if err != nil {
		e, ok := err.(*gssapi.Error)
		if ok && e.Major.ContinueNeeded() {
			return nil, nil, errors.New("Unexpected GSS_S_CONTINUE_NEEDED")
		}
	}

	url := c.ServiceAddress + path
	if !strings.HasPrefix(url, "https://") {
		url = "https://" + url
	}

	body := io.Reader(nil)
	if bodyf != nil {
		body = bytes.NewBufferString(bodyf(ctx))
	}

	r, err = http.NewRequest(method, url, body)
	if err != nil {
		return nil, nil, errors.New("Could not create the request")
	}
	spnego.AddSPNEGONegotiate(r.Header, "Authorization", token)

	return ctx, r, nil
}

func DoKerberosRequest(url string) (*http.Response, error) {

	if c.Lib == nil {
		lib, err := loadlib(false, "go-gssapi-test-service")
		if err != nil {
			log.Fatal(err)
		}
		c.Lib = lib
	}

	endpoint := strings.Split(url, "/")[2]
	hostname := strings.Split(endpoint, ":")[0]

	c.ServiceName = "HTTP/" + hostname

	context, request, err := initClientContext("GET", url, nil)
	if err != nil {
		return nil, errors.New("Could not initialize context: " + err.Error())
	}
	defer context.Release()

	resp, err := http.DefaultClient.Do(request)
	return resp, err
}
