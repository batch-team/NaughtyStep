// Logging utility to make requests to the naughtyLogger service
package naughtyLogger

import (
	"fmt"
	"utils/config"
	"net/http"
	"strings"
)

func log(module string, logData string, level string) {

	url := "http://" + config.Get("naughtyLogger", "host") + "/v1/step/log/" + level + "/" + module

	resp, err := http.Post(url, "text/plain", strings.NewReader(logData))

	if err != nil {
		fmt.Println(err)
		return
	}

	resp.Body.Close()

}

func Debug(module string, logData string) {
	log(module, logData, "debug")
}

func Info(module string, logData string) {
	log(module, logData, "info")
}

func Warning(module string, logData string) {
	log(module, logData, "warning")
}

func Error(module string, logData string) {
	log(module, logData, "error")
}

func Critical(module string, logData string) {
	log(module, logData, "critical")
}
