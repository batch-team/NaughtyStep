package roger

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"sync"

	"utils/config"
	"utils/gssapi"
	"utils/misc"
)

var rogerHosts []string
var rogerPort string
var mutex sync.RWMutex

func dereferenceRoger() error {
	roger := config.GetMap("services", "roger")

	rogerPort = roger["port"]

	ips, err := net.LookupIP(roger["url"])
	if err != nil {
		return err
	}

	for _, ip := range ips {
		hostname, err := net.LookupAddr(ip.String())
		if err != nil {
			return errors.New("could not find the hostname of " + ip.String())
		}
		rogerHosts = append(rogerHosts, hostname...)
	}

	for index := range rogerHosts {
		rogerHosts[index] = rogerHosts[index][:len(rogerHosts[index])-1]
	}

	return nil
}

func getEndpoint() (string, error) {
	if len(rogerHosts) == 0 {
		mutex.Lock()
		if len(rogerHosts) == 0 {
			err := dereferenceRoger()
			if err != nil {
				return "", err
			}
		}
		mutex.Unlock()
	}

	teigiHostname := rogerHosts[rand.Intn(len(rogerHosts))]

	url := "https://" + teigiHostname + ":" + rogerPort

	return url, nil
}

func DoRogerRequest(query string) (*http.Response, error) {
	endpoint, err := getEndpoint()
	if err != nil {
		return nil, errors.New("could not find a suitable endpoint: " + err.Error())
	}

	resp, err := gssapi.DoKerberosRequest(endpoint + query)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func processResponse(response *http.Response) ([]byte, error) {
	if response.StatusCode != 200 {
		return nil, errors.New("unexpected response " + strconv.Itoa(response.StatusCode))
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, errors.New("request parsing failed: " + err.Error())
	}

	return body, nil
}

func getRogerEntry(hostname string) (map[string]string, error) {

	hostname = misc.Fqdnify(hostname)

	query := "/roger/v1/state/" + hostname + "/"

	resp, err := DoRogerRequest(query)
	if err != nil {
		return nil, errors.New("roger request failed: " + err.Error())
	}

	defer resp.Body.Close()

	body, err := processResponse(resp)
	if err != nil {
		return nil, err
	}

	var entries map[string]string
	json.Unmarshal(body, &entries)

	return entries, nil
}

func GetAppstate(hostname string) (string, error) {
	entry, err := getRogerEntry(hostname)
	if err != nil {
		return "could not read appstate for " + hostname + ": ", err
	}
	return entry["appstate"], nil
}

func GetRootPassword(hostname string) (string, error) {

	hostname = misc.Fqdnify(hostname)

	url := "/tellme/v1/host/" + hostname + "/account/root/"
	resp, err := DoRogerRequest(url)
	if err != nil {
		return "", errors.New("roger request failed: " + err.Error())
	}

	defer resp.Body.Close()

	body, err := processResponse(resp)
	if err != nil {
		return "", err
	}

	type reply struct {
		Account         string `json: account`
		Encryption_type string `json: encryption_type`
		Sha512          string `json: sha512`
		Hostname        string `json: hostname`
		Password        string `json: password`
		Password_crypt  string `json: password_crypt`
		Update_time     int    `json: update_time`
		Updated_by      string `json: updated_by`
	}

	var rpl reply
	json.Unmarshal(body, &rpl)

	return rpl.Password, nil
}

func IsDraining(hostname string) (bool, error) {
	appstate, err := GetAppstate(hostname)
	if err != nil {
		return true, err // be conservative
	}

	drainingStates := config.GetList("general", "draining_states")

	for _, drainingState := range drainingStates {
		if appstate == drainingState {
			return true, nil
		}
	}

	return false, nil
}
