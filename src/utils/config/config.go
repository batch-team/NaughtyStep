// Config helper; currently just a wrapper around viper
package config

import (
	"github.com/spf13/viper"
	"log"
	"strconv"
)

// Flag to indicate if the config has been loaded
var loaded bool

func Init() {
	viper.SetConfigType("json")
	viper.SetConfigName("naughtystep")
	viper.AddConfigPath("/etc/naughtystep")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalln(err)
	}
	loaded = true
}

// Get the value of the key from the config json
// Automatically sets up viper at the first call
// Returns empty string if either component of key parameters do not exist
// TODO: check if component and key exist
func Get(component string, key string) string {
	if !loaded {
		Init()
	}
	return viper.GetString(component + "." + key)
}

func GetInt(component string, key string) (int, error) {
	value, err := strconv.Atoi(Get(component, key))
	return value, err
}

func GetList(component string, key string) []string {
	if !loaded {
		Init()
	}
	return viper.GetStringSlice(component + "." + key)
}

func GetMap(component string, key string) map[string]string {
	if !loaded {
		Init()
	}
	return viper.GetStringMapString(component + "." + key)
}
