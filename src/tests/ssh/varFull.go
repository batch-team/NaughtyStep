package main

import (
	"strconv"

	"utils/naughtyLogger"
)

type VarFullTest struct{}

func (ping VarFullTest) GetName() string {
	return "varFull"
}

func (varFull VarFullTest) RunTest(target string) string {

	naughtyLogger.Debug("tests.varFull", "Running on "+target)
	out, err := sshPass(target, "df -h | grep '/var$' | awk '{print $5}'")
	if err != nil {
		naughtyLogger.Error("tests.varFull", "Error while doing ssh: "+err.Error())
		return "ERROR"
	}

	if len(out) == 0 {
		// Output is empty when there is no var
		return "true"
	}

	percent, err := strconv.Atoi(string(out)[:len(out)-2])
	if err != nil {
		naughtyLogger.Error("tests.varFull", "Error at atoi: "+err.Error())
		return "ERROR"
	}

	naughtyLogger.Debug("tests.varFull", "/var percent on "+target+" is "+string(out[:len(out)-1]))

	if percent < 100 {
		return "true"
	} else {
		return "false"
	}
}
