package main

import (
	"strings"
)

// Test for checking the uptime of a machine
type UptimeTest struct{}

func (uptime UptimeTest) GetName() string {
	return "uptime"
}

func (uptime UptimeTest) RunTest(target string) string {
	out, err := sshKrb(target, "cat /proc/uptime | awk '{print $1}'")

	if err != nil || strings.HasPrefix(string(out), "exit status") {
		return "ERROR"
	}

	return strings.Split(string(out), ".")[0]
}
