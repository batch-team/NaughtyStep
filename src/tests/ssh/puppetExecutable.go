package main

// Test for checking the case when /usr/bin/puppet exists and
// /opt/puppetlabs/bin/puppet does not
// Might also fail due to ssh issues
type PuppetExecutableTest struct{}

func (ssh PuppetExecutableTest) GetName() string {
	return "puppetExecutable"
}

func (ssh PuppetExecutableTest) RunTest(target string) string {
	_, err := sshKrb(target, " if [ -f /usr/bin/puppet ] && [ ! -f /opt/puppetlabs/bin/puppet ]; then false; fi")

	if err == nil {
		return "true"
	} else {
		return "false"
	}
}
