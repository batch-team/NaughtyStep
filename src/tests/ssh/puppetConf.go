package main

// Test for checking if the puppet configuration includes the server entry
// Might also fail due to ssh issues
type PuppetServerConfTest struct{}

func (ssh PuppetServerConfTest) GetName() string {
	return "puppetConf"
}

func (ssh PuppetServerConfTest) RunTest(target string) string {
	_, err := sshKrb(target, "< /etc/puppet/puppet.conf grep -e ^serverd")

	if err == nil {
		return "true"
	} else {
		return "false"
	}
}
