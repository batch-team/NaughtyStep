package main

import (
	"errors"
	"os/exec"

	"utils/naughtyLogger"
	"utils/roger"
)

func sshKrb(host string, command string) (string, error) {
	cmd := exec.Command("timeout", "10",
		"ssh",
		"-oPreferredAuthentications=gssapi-with-mic",
		"-oStrictHostKeyChecking=no",
		"-oConnectTimeout=8",
		"-oBatchMode=yes",
		"root@"+host, command)

	out, err := cmd.CombinedOutput()

	return string(out), err
}

func sshPass(host string, command string) (string, error) {
	password, err := roger.GetRootPassword(host)
	if err != nil {
		return "", errors.New("could not fetch root password for " + host + ": " + err.Error())
	}

	naughtyLogger.Debug("sshtest", "Root password for "+host+" is "+password)

	out, err := exec.Command(
		"timeout",
		"120",
		"sshpass",
		"-p"+password,
		"ssh",
		"-oStrictHostKeyChecking=no",
		"root@"+host,
		"-q",
		command).Output()
	if err != nil {
		// Usually getting timeouts here (status 124)
		return "", errors.New("while running command for host " + host + ": " + err.Error())
	}

	return string(out), nil
}
