package main

// Test for checking the ssh connection
type SshTest struct{}

func (ssh SshTest) GetName() string {
	return "ssh"
}

func (ssh SshTest) RunTest(target string) string {
	_, err := sshKrb(target, "exit")

	if err == nil {
		return "true"
	} else {
		return "false"
	}
}
