package main

import (
	"tester"
)

func main() {
	var ssh SshTest
	var puppetExec PuppetExecutableTest
	var puppetConf PuppetServerConfTest
	var uptime UptimeTest
	var varFull VarFullTest

	go tester.Run(ssh)
	go tester.Run(puppetExec)
	go tester.Run(puppetConf)
	go tester.Run(varFull)
	tester.Run(uptime)
}
