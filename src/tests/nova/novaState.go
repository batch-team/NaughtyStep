package main

import (
	"utils/novaUtils"

	"github.com/rackspace/gophercloud/openstack/compute/v2/servers"
)

type NovaStateTest struct{}

// The name of the current test
func (novaState NovaStateTest) GetName() string {
	return "novaState"
}

// Return the state of the server at Hostname
func (novaState NovaStateTest) RunTest(Hostname string) string {

	host, err := novaUtils.GetHostDetails(Hostname)
	if err != nil {
		return "NOT FOUND"
	}

	client, err := novaUtils.GetServiceClientForTenant(host.TenantId, host.TenantName)
	if err != nil {
		return "TEST ERROR (client service fetch)"
	}

	server, err := servers.Get(client, host.UUID).Extract()
	if err != nil {
		return "TEST ERROR (server extract)"
	}

	return server.Status
}
