package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"utils/novaUtils"

	"github.com/abiosoft/semaphore"
	"github.com/rackspace/gophercloud"
	"github.com/rackspace/gophercloud/openstack/compute/v2/servers"
)

type hostDetails struct {
	Name       string
	UUID       string
	TenantName string
	TenantId   string
}

type NovaConsoleOutput struct{}

// The name of the current test
func (novaConsole NovaConsoleOutput) GetName() string {
	return "novaConsoleOutput"
}

var sem *semaphore.Semaphore = semaphore.New(1)

// Return the console output of the server at Hostname
func (novaConsole NovaConsoleOutput) RunTest(Hostname string) string {
	// Update the semaphore to limit the number of concurrent requests
	sem.Acquire()
	defer sem.Release()

	// Get the tennant of the host
	host, err := novaUtils.GetHostDetails(Hostname)
	if err != nil {
		return "NOT FOUND"
	}

	// Check if the nova state allows a console request
	client, err := novaUtils.GetServiceClientForTenant(host.TenantId, host.TenantName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not fetch service client for tenant %s\n", host.TenantName)
		return "TEST ERROR"
	}

	server, err := servers.Get(client, host.UUID).Extract()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not fetch server for host %s: %s\n", host.Name, err)
		return "TEST ERROR"
	}
	novaState := server.Status

	if novaState != "ACTIVE" && novaState != "SHUTOFF" {
		return "NOT AVAILABLE"
	}

	fmt.Fprintf(os.Stderr, "Preparing to fetch nova console for %s, with state %s\n", Hostname, novaState)

	// Do the nova request for the console output
	url := client.ServiceURL("servers", host.UUID, "action")
	reqBody := struct {
		Field map[string]int `json:"os-getConsoleOutput"`
	}{
		map[string]int{"length": -1},
	}

	response, err := client.Post(url, reqBody, nil, &gophercloud.RequestOpts{
		OkCodes: []int{200},
	})

	if err != nil {
		fmt.Fprintf(os.Stderr, "Fetching console failed for %s, which was %s: %s\n", Hostname, novaState, err.Error())
		return "TEST FAILED"
	}
	defer response.Body.Close()

	body, _ := ioutil.ReadAll(response.Body)

	fmt.Fprintf(os.Stderr, "Fetch of nova console for %s completed\n", Hostname)

	// Replace \n in body with proper newlines
	return strings.Replace(string(body), "\\n", "\n", -1)
}
