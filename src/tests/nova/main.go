package main

import (
	"tester"
)

func main() {
	var novaState NovaStateTest
	var novaConsole NovaConsoleOutput

	go tester.Run(novaState)
	tester.Run(novaConsole)
}
