package main

import (
	"net"
)

type DnsTest struct{}

func (dns DnsTest) GetName() string {
	return "dns"
}

func (dns DnsTest) RunTest(target string) string {
	_, err := net.LookupIP(target)

	if err == nil {
		return "true"
	} else {
		return "false"
	}
}
