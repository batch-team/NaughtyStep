package main

import (
	"os/exec"
)

type PingTest struct{}

func (ping PingTest) GetName() string {
	return "ping"
}

func (ping PingTest) RunTest(target string) string {
	err := exec.Command("ping", "-c4", target).Run()

	if err == nil {
		return "true"
	} else {
		return "false"
	}
}
