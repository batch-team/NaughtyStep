package main

import (
	"tester"
)

func main() {
	var ping PingTest
	var dns DnsTest

	go tester.Run(dns)
	tester.Run(ping)
}
