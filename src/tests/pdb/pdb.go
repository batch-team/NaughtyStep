package main

import (
	"tester"

	"utils/naughtyLogger"
	"utils/pdb"
)

type CondorVersionTest struct{}

func (condorVersion CondorVersionTest) GetName() string {
	return "condor_version"
}

func (condorVersion CondorVersionTest) RunTest(target string) string {
	version, err := pdb.GetFact(target, "condor_version")
	if err != nil {
		naughtyLogger.Error("tests.condor_version", "Can't get condor version for "+target+": "+err.Error())
		return "ERROR"
	}
	return version
}

func main() {
	var condorVersion CondorVersionTest

	tester.Run(condorVersion)
}
