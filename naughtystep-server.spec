%define name naughtystep-server
%define version 1.7
%define release 2.el7
%define debug_package %{nil}

Summary: Automatic fault detection and recovery tool
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{version}.tar.gz
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-root

License: GPL+

%{?systemd_requires}
BuildRequires: systemd
BuildRequires: python-devel

Requires: ai-tools
Requires: batchfactory
Requires: condor-python
Requires: golang
Requires: python-dateutil
Requires: python2-eventlet
Requires: python-flask
Requires: python-flask-sqlalchemy
Requires: python-gssapi <= 0.6.4
Requires: python-gunicorn
Requires: python2-keystoneauth1
Requires: python-keystoneclient
Requires: python-keystoneclient-kerberos
Requires: python-ldap
Requires: python-novaclient
Requires: python-paramiko
Requires: python-platform-lsf
Requires: python-redis
Requires: python-requests
Requires: python-requests-kerberos
Requires: python-tabulate
Requires: python2-tqdm
Requires: python-twisted-core
Requires: sshpass
Requires: systemd

%description
Automatic fault detection and recovery tool


%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install

%{__rm} -rf %{buildroot}
install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_unitdir}
install -d %{buildroot}%{_libexecdir}
install -d %{buildroot}%{_libexecdir}/naughtystep/
install -d %{buildroot}%{_libexecdir}/naughtystep/actions

install -p -m 0755 %{_builddir}/%{name}-%{version}/bin/naughtystep-skynet %{buildroot}%{_bindir}/naughtystep-skynet
install -p -m 0755 %{_builddir}/%{name}-%{version}/bin/naughtystep-test-ping %{buildroot}%{_bindir}/naughtystep-test-ping
install -p -m 0755 %{_builddir}/%{name}-%{version}/bin/naughtystep-test-ssh %{buildroot}%{_bindir}/naughtystep-test-ssh
install -p -m 0755 %{_builddir}/%{name}-%{version}/bin/naughtystep-test-pdb %{buildroot}%{_bindir}/naughtystep-test-pdb
install -p -m 0755 %{_builddir}/%{name}-%{version}/bin/naughtystep-test-nova %{buildroot}%{_bindir}/naughtystep-test-nova

cp -Tr %{_builddir}/%{name}-%{version}/src/naughtystep/actions/scripts/ %{buildroot}/%{_libexecdir}/naughtystep/actions/

install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-action-drain.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-action-kill.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-action-manual.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-action-reboot.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-action-ssh.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-approver.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-blackboard.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-consumer.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-draining-engine.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-host-locator.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-ledger.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-logger.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-skynet.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-tester-nova.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-tester-pdb.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-tester-ping.service %{buildroot}%{_unitdir}
install -p -m 644 %{_builddir}/%{name}-%{version}/packaging/systemd/naughtystep-tester-ssh.service %{buildroot}%{_unitdir}

%{__python} setup.py install --skip-build --root %{buildroot}


%clean
rm -rf $RPM_BUILD_ROOT


%post

# Preset on first install
%systemd_post naughtystep-action-drain.service
%systemd_post naughtystep-action-kill.service
%systemd_post naughtystep-action-manual.service
%systemd_post naughtystep-action-reboot.service
%systemd_post naughtystep-action-ssh.service
%systemd_post naughtystep-approver.service
%systemd_post naughtystep-blackboard.service
%systemd_post naughtystep-consumer.service
%systemd_post naughtystep-draining-engine.service
%systemd_post naughtystep-host-locator.service
%systemd_post naughtystep-ledger.service
%systemd_post naughtystep-logger.service
%systemd_post naughtystep-skynet.service
%systemd_post naughtystep-tester-nova.service
%systemd_post naughtystep-tester-pdb.service
%systemd_post naughtystep-tester-ping.service
%systemd_post naughtystep-tester-ssh.service


%preun

# Stops units before uninstalling
%systemd_preun naughtystep-action-drain.service
%systemd_preun naughtystep-action-kill.service
%systemd_preun naughtystep-action-manual.service
%systemd_preun naughtystep-action-reboot.service
%systemd_preun naughtystep-action-ssh.service
%systemd_preun naughtystep-approver.service
%systemd_preun naughtystep-blackboard.service
%systemd_preun naughtystep-consumer.service
%systemd_preun naughtystep-draining-engine.service
%systemd_preun naughtystep-host-locator.service
%systemd_preun naughtystep-ledger.service
%systemd_preun naughtystep-logger.service
%systemd_preun naughtystep-skynet.service
%systemd_preun naughtystep-tester-nova.service
%systemd_preun naughtystep-tester-pdb.service
%systemd_preun naughtystep-tester-ping.service
%systemd_preun naughtystep-tester-ssh.service

%postun

# Reload and restart units after installation
%systemd_postun_with_restart naughtystep-action-drain.service
%systemd_postun_with_restart naughtystep-action-kill.service
%systemd_postun_with_restart naughtystep-action-manual.service
%systemd_postun_with_restart naughtystep-action-reboot.service
%systemd_postun_with_restart naughtystep-action-ssh.service
%systemd_postun_with_restart naughtystep-approver.service
%systemd_postun_with_restart naughtystep-blackboard.service
%systemd_postun_with_restart naughtystep-consumer.service
%systemd_postun_with_restart naughtystep-draining-engine.service
%systemd_postun_with_restart naughtystep-host-locator.service
%systemd_postun_with_restart naughtystep-ledger.service
%systemd_postun_with_restart naughtystep-logger.service
%systemd_postun_with_restart naughtystep-skynet.service
%systemd_postun_with_restart naughtystep-tester-nova.service
%systemd_postun_with_restart naughtystep-tester-pdb.service
%systemd_postun_with_restart naughtystep-tester-ping.service
%systemd_postun_with_restart naughtystep-tester-ssh.service


%files
%defattr(-,root,root)
%{_bindir}/*
%{python_sitelib}/*
%{_unitdir}/naughtystep-*
%{_libexecdir}/naughtystep/actions/*


%changelog
* Thu Dec 14 2017 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 1.7-2
- Fix packaging requirements.
- Add systemd unit files for all the services.
- Ship action scripts
